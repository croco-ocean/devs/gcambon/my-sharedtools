#! /bin/bash

yd=2006
md_yd=1
yf=2011
mf_yf=12

for run in blkperio ;
do echo 'Compute '$run
filename=roms_avg
#var=temp
filename2=windstress_blkperio
suffix=''

year=${yd}
while [ ${year} -le ${yf} ]
do
  if [ ${yd} == ${yf} ]; then
    md=${md_yd}
    mf=${mf_yf}
  elif [ ${year} == ${yd} ]; then
    md=${md_yd}
    mf=12
  elif [ ${year} == ${yf} ]; then
    md=1
    mf=${mf_yf}
  else
    md=1
    mf=12
  fi

  month=${md}
  while [ ${month} -le ${mf} ]
  do

   if  [ ${month} -le 9 ]; then
       month2=0${month}
   else
       month2=${month}
   fi

echo ${month}
echo ${year}

#   ncks -v scrum_time,lat_rho,lon_rho,${var} ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
#   ncks -v scrum_time,lat_rho,lon_rho,zeta,temp,salt,hbl ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
#   ncrcat -F -d s_rho,32,32 -v ${var} ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
    
###    ncks -v scrum_time,zeta ${run}/${filename}_Y${year}M${month}.nc${suffix} LAB/${filename2}_Y${year}M${month}.nc${suffix}
   ncks -v scrum_time,sustr,svstr,wstr ${run}/${filename}_Y${year}M${month}.nc${suffix} LAB/${filename2}_Y${year}M${month}.nc${suffix}
   month=`expr $month + 1`
   
  done
  ncrcat -h LAB/${filename2}_Y${year}M[123456789].nc${suffix} LAB/${filename2}_Y${year}M1[012].nc${suffix} LAB/${filename2}_Y${year}.nc${suffix}
  year=`expr $year + 1`
  
done
echo "Compute final"
ncrcat -h LAB/${filename2}_Y2006.nc${suffix} LAB/${filename2}_Y2007.nc${suffix} LAB/${filename2}_Y2008.nc${suffix} LAB/${filename2}_Y2009.nc${suffix} LAB/${filename2}_Y2010.nc${suffix} LAB/${filename2}_Y2011.nc${suffix} ${run}/${filename2}_${yd}-${yf}.nc${suffix}
rm -Rf LAB/*
done
