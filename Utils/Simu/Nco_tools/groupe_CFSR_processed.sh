#!/bin/bash

yeardeb=1979
yearend=2010
dirin='/local/tmp/1/gcambon/DATA_CFSR/'
dirout=Final

for vv in hf uwind vwind precip specific temp_2m ; do 
#for vv in uwind vwind precip tempsurf temp2m ; do
#for vv in specific ; do
    echo ${vv}
    myvar=${vv}
    
    if [ $myvar == hf ] ; then 
	#HEAT FLUX (4 variables)
	varnameRT=(Downward_Short-Wave_Rad_Flux_surface Downward_Long-Wave_Rad_Flux Upward_Short-Wave_Rad_Flux_surface Upward_Long-Wave_Rad_Flux_surface)
	dirname=${dirin}'heatflux/'
	
    elif [ $myvar == uwind ]  ; then 
	#Uwind
	varnameRT=(U-component_of_wind)
	dirname=${dirin}'U-wind'

    elif [ $myvar == vwind ]  ; then 
	#Vwind
	varnameRT=(V-component_of_wind)
	dirname=${dirin}'V-wind'
	
    elif [ $myvar == precip ]  ; then 
	#precip_rate
	varnameRT=(Precipitation_rate)
	dirname=${dirin}'precip_rate'
	
    elif [ $myvar == temp_2m ]  ; then 
	#temp_2m
	dirname=${dirin}'temp_2m'
	varnameRT=(Temperature_height_above_ground)
	
    elif [ $myvar == specific ]  ; then 
	#specific
	dirname=${dirin}'specific'
	varnameRT=(Specific_humidity)
l
    elif [ $myvar == temp_surf ]  ; then 
	#temp_surf
	dirname=${dirin}'temp_surf'
	varnameRT=(Temperature_surface)
    fi
    
    listvarRT=`echo ${varnameRT[*]}`

    
    echo '===== PROCESS '$listvarRT'========'
    cd ${dirname}
    echo $PWD
    
    for yy in `seq $yeardeb $yearend` ; do
	echo 'Process year '${yy}
	cd y${yy}
	echo $PWD
	lengthvar=${#varnameRT[@]}
	echo ${lengthvar}
	for k in `seq 0 $(( ${lengthvar} - 1))` ; do	 
	    vnameout0=${varnameRT[$k]}
	    vnameout=${vnameout0}'_Y'${yy}'_tmp.nc'
	    echo ${vnameout} '...'
	    mv ${vnameout} ${dirin}'/'${dirout}
	done
	cd ..
    done
    cd ..
done
