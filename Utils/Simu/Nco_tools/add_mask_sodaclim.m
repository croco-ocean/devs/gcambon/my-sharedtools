clear all
close all

%climfile='/net/centaure/local/tmp/1/gcambon/BUIC/OUTPUT/BENGSAFE_R15/blkperio_R15_CFSR_t2_EXTRACTED_forsoda/roms_clm_Soda5_M1-12.nc';
%N=100; 

%climfile='/net/centaure/local/tmp/1/gcambon/BUIC/OUTPUT/R3KM/BENGSAFE_R3KM_V2c_EXTRACTED_forsoda/roms_clm_Soda2_M1-12.nc';
%N=75

climfile='/net/centaure/local/tmp/1/gcambon/BUIC/OUTPUT/BENGSAFE_R15/blkperio_R15_CFSR_t2_EXTRACTED_forwoa/roms_clm_WOA_R7KM.nc';
nc=netcdf(climfile,'r');
N=100; 

mask_rho=nc{'mask_rho'}(:);
mask_u=rho2u_2d(mask_rho);
mask_v=rho2v_2d(mask_rho);

mask_rho2d=permute(repmat(mask_rho,[1 1 12]),[3 1 2]);
mask_rho3d=permute(repmat(mask_rho,[1 1 N 12]),[4 3 1 2]);
%
mask_u2d=permute(repmat(mask_u,[1 1 12]),[3 1 2]);
mask_u3d=permute(repmat(mask_u,[1 1 N 12]),[4 3 1 2]);
%
mask_v2d=permute(repmat(mask_v,[1 1 12]),[3 1 2]);
mask_v3d=permute(repmat(mask_v,[1 1 N 12]),[4 3 1 2]);


salt=nc{'salt'}(:);
temp=nc{'temp'}(:);
%ubar=nc{'ubar'}(:);
%vbar=nc{'vbar'}(:);
%u=nc{'u'}(:);
%v=nc{'v'}(:);
zeta=nc{'zeta'}(:);
%ssh=nc{'ssh'}(:);
close(nc)

nw=netcdf(climfile,'w');
nw{'salt'}(:)=salt.*mask_rho3d;
nw{'temp'}(:)=temp.*mask_rho3d;
%nw{'ubar'}(:)=ubar.*mask_u2d;
%nw{'vbar'}(:)=vbar.*mask_v2d;
%nw{'u'}(:)=u.*mask_u3d;
%nw{'v'}(:)=v.*mask_v3d;
nw{'zeta'}(:)=zeta.*mask_rho2d;
%nw{'ssh'}(:)=ssh.*mask_rho2d;
close(nw)
