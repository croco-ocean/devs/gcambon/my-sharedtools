#!/bin/bash

echo ' Process the extraction ...'
./extr_cfsr.sh > extr_cfsr.out 2>&1 
echo ' Extraction done ...'

echo '  '
echo ' Processing the variables ...'
./process_cfsr_hf.sh > process_cfsr.out 2>&1
echo ' Processing done ...'
