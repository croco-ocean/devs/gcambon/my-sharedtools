#! /bin/bash

md=1
mf=12

#for freq in 3d 10d 30d ; do 
for freq in 30d ; do 
filenameprefix='ext_roms_frc_6h_periodic_2002_2003_filt'${freq}'_Y2002'
echo $filenameprefix

for suffix in '' '_1' ; do
echo 'Suffix is:' $suffix 

month=${md}
while [ ${month} -le ${mf} ] ; 
do
    echo '======================='
    echo 'Process month '${month}
    echo '----> Process cycle_length changes'
    echo '      in file : '${filenameprefix}'M'${month}'.nc'$suffix
    ncatted -O -a cycle_length,sms_time,o,d,365. ${filenameprefix}'M'${month}'.nc'$suffix
    ncatted -O -a cycle_length,shf_time,o,d,365. ${filenameprefix}'M'${month}'.nc'$suffix
    ncatted -O -a cycle_length,srf_time,o,d,365. ${filenameprefix}'M'${month}'.nc'$suffix
    ncatted -O -a cycle_length,swf_time,o,d,365. ${filenameprefix}'M'${month}'.nc'$suffix
    echo '----done'
    month=`expr $month + 1`
done #boucle sur les mois
done # suffix
done # freq

