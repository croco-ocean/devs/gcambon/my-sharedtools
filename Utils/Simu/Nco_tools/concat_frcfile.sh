#!/bin/bash

# - Extract the various variable for each month
# - make the concatenation Y2001M12 -> Y2004M1 for eache variable and variable-dimension associated

# From all the variable_forcing file do the concateantion into 1 file
#================= 

#==========================
extract=0
allconcat=0
concat_allvar=1
upload=1

for endf in '_1' ; do 
#rm -Rf Lab/*

#==========================

if [ ${extract} == 1 ]; then 
    for var in sustr svstr shflux swflux swrad ; do
	
	if [ $var == sustr ]; then
	    vartime=sms_time
	elif [ $var == svstr ]; then
	    vartime=sms_time
	elif [ $var == shflux ]; then
	    vartime=shf_time
	elif [ $var == swflux ]; then
	    vartime=swf_time
	elif [ $var == swrad ]; then
	    vartime=srf_time
	fi
	
	echo '  '
	echo 'Extract var '$var
    echo '================'
    
    for YY in `seq 2001 2004` ; do 
	for MM in `seq 1 12` ; do 
	    
	    if [[ ( $YY == 2001  && $MM != 12 ) || ( $YY == 2004  && $MM != 1 ) ]]; then
		echo 'NO FILE TO PROCESS'
	    else
		file=roms_frc_6h_Y${YY}M${MM}.nc${endf}
		echo ' : ... in file '$file
		
		echo '       => extraction'
		ncks -v $var $file -o Lab/${var}_${file}
		
		echo '       => mk rec dim'
                ncks -O --mk_rec_dmn ${vartime} Lab/${var}_${file} Lab/${var}_${file}
	    fi
	done  # month
    done # year
    
    done
fi 

#=======================

if [ ${allconcat} == 1 ]; then
    for var in sustr svstr shflux swflux swrad ; do
	echo 'Process the var '$var
	echo '       => ncrcat the extracted file ...'
	cd  Lab/
	
	ncrcat ${var}_roms_frc_6h_Y2001M12.nc${endf} ${var}_roms_frc_6h_Y2002M[123456789].nc${endf} ${var}_roms_frc_6h_Y2002M1[012].nc${endf} ${var}_roms_frc_6h_Y2003M[123456789].nc${endf} ${var}_roms_frc_6h_Y2003M1[012].nc${endf} ${var}_roms_frc_6h_Y2004M1.nc${endf}  -o ${var}_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}   
	
	# Remove the unlimited time dimensions
	ncecat -O ${var}_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  ${var}_roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
	
	#Remove the record degenerated dimension + do it in 64 bit large file format
	ncwa -O -6 -a record ${var}_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  ${var}_roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
	
	cd -    
    done # var
fi

#=======================

if [ ${concat_allvar} == 1 ]; then
    echo 'Process concat_allvar ...'
    cd Lab/ 

    cp sustr_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  build_roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
    #
    ncks -A svstr_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  build_roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
    ncks -A shflux_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  build_roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
    ncks -A swflux_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  build_roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
    ncks -A swrad_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  build_roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
    #
    
    mv build_roms_frc_6h_Y2001M12_Y2004M1.nc${endf}  ../roms_frc_6h_Y2001M12_Y2004M1.nc${endf} 
    cd -
fi

if [ $upload == 1 ]; then 
    rsync -avz --progress roms_frc_6h_Y2001M12_Y2004M1.nc${endf} rkhe002@ada.idris.fr:/workgpfs/rech/khe/rkhe002/INPUT/BENGSAFE_R4/Y2000/atmo/6h_2002-2004_frc/
    
fi

done
