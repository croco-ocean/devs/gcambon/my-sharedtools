#!/bin/bash


for i in `seq 0 255`; do 
    #echo $i ; 	
    if [ $i -lt 10 ]; then 
	i2='00'$i 	
    elif [ $i -lt 100 ]; then 
	i2='0'$i	
    else 
	i2=$i 
    fi
    echo $i2
    cp roms_avg_Y2002M6.${i2}.nc_1 grid1/roms_avg_Y2002M6.${i2}.ncf
done 
