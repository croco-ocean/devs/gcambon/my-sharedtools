#!/bin/bash

# Script to rsync cfsr global 
# data on caparmor
# G. Cambon March 2016
#---------------------------------

yeardeb=1979
yearend=2010

for i in specific temp_2m U-wind V-wind precip_rate heatflux ; do 
    cd $i 
    echo 'PROCESS THE VAR:__ '$i
    listf=`ls -1 *.nc.gz`
        echo '--> rsync on 6h CFSFR flx on ada ....'
	rsync -avz --progress ${listf} rkhe002@ada.idris.fr:/workgpfs/rech/khe/rkhe002/BCK_CFSR_DATA_1979_2010/${i}/
    cd ../
done


