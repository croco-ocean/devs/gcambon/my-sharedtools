#! /bin/bash

# 0 - Compilation:
#############################
ls -1 *.nc > file_to_execute

# Pour creer le record 
for i in `cat file_to_execute` ;
do
echo $i
ncdump $i > tmp1.cdl
echo "shf_time"
sed 's/shf_time = 1/shf_time = 12/' tmp1.cdl > tmp2.cdl
echo "swf_time"
sed 's/swf_time = 1/swf_time = 12/' tmp2.cdl > tmp3.cdl
echo "sst_time"
sed 's/sst_time = 1/sst_time = 12/' tmp3.cdl > tmp4.cdl
echo "srf_time"
sed 's/srf_time = 1/srf_time = 12/' tmp4.cdl > tmp5.cdl
echo "sss_time"
sed 's/sss_time = 1/sss_time = 12/' tmp5.cdl > tmp6.cdl

ncgen tmp6.cdl -o $i
rm -f *tmp*
done
