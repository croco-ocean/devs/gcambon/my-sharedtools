#! /bin/bash

# 0 - Compilation:
#############################
ls -1 *frc_QSCAT* > file_to_execute

# Pour creer le record 
for i in `cat file_to_execute` ;
do
echo $i
ncecat -O $i $i  # Add degenerate record dimension named "record"
done

# Pour transformer la dimension fixe Time vers une dim record unlimited
for j in `cat file_to_execute` ;
do
echo $j
ncpdq -O -a sms_time,record $j  $j # Switch "record" and "time"
# Time= nom de ta dimension temporelle
done

# Pour mettre les variables qui dependait du Time fixe vers Time record !!
#set path=/media/disk/LINUX/Data_ERS/1991_test
for j in `cat file_to_execute` ;
do
echo $j
##ncwa -O -a record  $path/$file_out  $path/$file_out # Average out
ncwa -O -a record  $j  $j # Average out
#degenerate "record"
done



