#! /bin/bash

yd=1992
md_yd=1
yf=2000
mf_yf=12

filename=roms_avg
filename1=roms_avg_tau_concat
echo 'Compute '$filename

year=${yd}
while [ ${year} -le ${yf} ]
do
    if [ ${yd} == ${yf} ]; then
	md=${md_yd}
	mf=${mf_yf}
    elif [ ${year} == ${yd} ]; then
	md=${md_yd}
	mf=12
    elif [ ${year} == ${yf} ]; then
	md=1
	mf=${mf_yf}
    else
	md=1
	mf=12
    fi

    month=${md}
    while [ ${month} -le ${mf} ]
    do
	if  [ ${month} -le 9 ]; then
	    month2=0${month}
	else
	    month2=${month}
	fi
        echo "Compute Y,${year}, M,${month}"
#        echo "Compute ${filename}_Y${year}M${month}.nc"
	 ncks -F  -v sustr,svstr ${filename}_Y${year}M${month}.nc LAB/${filename}_tau_Y${year}M${month}.nc
        month=`expr $month + 1`
    done
        echo "Compute NCRCAT"
	ncrcat -h LAB/${filename}_tau_Y${year}M[123456789].nc LAB/${filename}_tau_Y${year}M1[012].nc LAB/${filename1}_Y${year}.nc
	year=`expr $year + 1`
done

ncrcat -h LAB/${filename1}_Y1992.nc LAB/${filename1}_Y1993.nc LAB/${filename1}_Y1994.nc LAB/${filename1}_Y1995.nc LAB/${filename1}_Y1996.nc LAB/${filename1}_Y1997.nc  LAB/${filename1}_Y1998.nc  LAB/${filename1}_Y1999.nc LAB/${filename1}_Y2000.nc ${filename1}_${yd}-${yf}.nc

\rm -Rf LAB/*
