#! /bin/bash
#for run in ScratchG6_FluxClim ScratchG6_7D_FluxClim ScratchG6_StERS_FluxClim ScratchG6_StNCEP_FluxClim ScratchG6_StQSCATANCEP_FluxClim ScratchG6_StQSCATCLM_FluxClim ; 
for run in  ScratchG6_StERS_FluxClim; 
do 
    dirextract='ExtractSaltSerena'
    echo 'dirextract: '$dirextract

    echo '##############################'
    echo 'Clean '${run}/$dirextract
    rm -Rf ${run}/$dirextract/*
    
    echo 'Compute '$run

    cpt=1
    yd=1992
    yf=2000
    md_yd=1
    mf_yf=12
#
    year=${yd}
    while [ ${year} -le ${yf} ]
    do
	if [ ${yd} == ${yf} ]; then
	    md=${md_yd}
	    mf=${mf_yf}
	elif [ ${year} == ${yd} ]; then
	    md=${md_yd}
	    mf=12
	elif [ ${year} == ${yf} ]; then
	    md=1
	    mf=${mf_yf}
	else
	    md=1
	    mf=12
	fi
	month=${md}
	while [ ${month} -le ${mf} ]
	do
	    file=${run}/roms_avg_Y${year}M${month}.nc
	    cpt2=${cpt}
	    if  [ ${cpt} -le 999 ]; then
		cpt2=0${cpt}
	    fi

	    if  [ ${cpt} -le 99 ]; then
		cpt2=00${cpt}
	    fi

	    if  [ ${cpt} -le 9 ]; then
		cpt2=000${cpt}
	    fi

	    ncks -v scrum_time,salt $file ${run}/$dirextract/temp_$cpt2.nc

	    cpt=`expr $cpt + 1 `
	    month=`expr $month + 1`
	done
	year=`expr $year + 1`
    done
done