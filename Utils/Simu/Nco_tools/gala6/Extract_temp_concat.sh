#! /bin/bash
#for run in ScratchG6_FluxClim ScratchG6_7D_FluxClim ScratchG6_StERS_FluxClim ScratchG6_StNCEP_FluxClim ScratchG6_StQSCATANCEP_FluxClim ScratchG6_StQSCATCLM_FluxClim ; 
for run in ScratchG6_StERS_FluxClim; 
#for run in ScratchG6_FluxClim ScratchG6_7D_FluxClim ScratchG6_StNCEP_FluxClim ScratchG6_StQSCATANCEP_FluxClim ScratchG6_StQSCATCLM_FluxClim ;
do 
    echo '##############################'
    echo 'Clean '${run}/LAB
    rm -Rf ${run}/LAB/*
#
    echo 'Compute '$run
    echo '==> Entering '$run
    cd $run
#
#    lat1=27
#    lat2=235
#
    yd=1992
    yf=2000
    md_yd=1
    mf_yf=12
#
    year=${yd}
    while [ ${year} -le ${yf} ]
    do
	if [ ${yd} == ${yf} ]; then
	    md=${md_yd}
	    mf=${mf_yf}
	elif [ ${year} == ${yd} ]; then
	    md=${md_yd}
	    mf=12
	elif [ ${year} == ${yf} ]; then
	    md=1
	    mf=${mf_yf}
	else
	    md=1
	    mf=12
	fi
	month=${md}
	while [ ${month} -le ${mf} ]
	do
	    file=roms_avg_Y${year}M${month}.nc
	    file2=LAB/roms_avg_temp_Y${year}M${month}.nc
#	    ncks -F -d xi_rho,55, -d eta_rho,${lat1},${lat2} -v scrum_time,lon_rho,lat_rho,temp,salt $file $file2
	    ncks -F -v scrum_time,lon_rho,lat_rho,temp $file $file2
#
	    echo 'Compute month '${month}
	    month=`expr $month + 1`
	done
	echo '==> Mean for Year: '${year}
#	ncra -h LAB/roms_avg_temp_Y${year}M[123456789].nc LAB/roms_avg_temp_Y${year}M1[012].nc LAB/roms_avg_temp_Y${year}.nc
	ncrcat -h LAB/roms_avg_temp_Y${year}M[123456789].nc LAB/roms_avg_temp_Y${year}M1[012].nc LAB/roms_avg_temp_Y${year}.nc
#
	year=`expr $year + 1`
    done
	echo '---------------------------'
	echo '==> Concat over all the years'
    ncrcat -h LAB/roms_avg_temp_Y1992.nc LAB/roms_avg_temp_Y1993.nc LAB/roms_avg_temp_Y1994.nc LAB/roms_avg_temp_Y1995.nc LAB/roms_avg_temp_Y1996.nc LAB/roms_avg_temp_Y1997.nc LAB/roms_avg_temp_Y1998.nc LAB/roms_avg_temp_Y1999.nc LAB/roms_avg_temp_Y2000.nc LAB/roms_avg_temp_${yd}-${yf}.nc

    mv LAB/roms_avg_temp_${yd}-${yf}.nc ${run}-allyear-concat_roms_avg_temp_${yd}-${yf}.nc

    rm -f LAB/*
    cd ../
    echo '<== Sorting '$run
done