#! /bin/bash


    echo '##############################'
    echo 'Clean LAB'
    rm -Rf LAB/*
 
    cpt=1
    yd=1992
    yf=2000
    md_yd=1
    mf_yf=12
    dim_smstime=0;
    dim_smstime_ok=0;
#
    year=${yd}
    while [ ${year} -le ${yf} ]
    do
	if [ ${yd} == ${yf} ]; then
	    md=${md_yd}
	    mf=${mf_yf}
	elif [ ${year} == ${yd} ]; then
	    md=${md_yd}
	    mf=12
	elif [ ${year} == ${yf} ]; then
	    md=1
	    mf=${mf_yf}
	else
	    md=1
	    mf=12
	fi
	month=${md}
	while [ ${month} -le ${mf} ]
	do
	    file=roms_frc_QSCAT_Y${year}M${month}.nc
	    echo 'process file : ' ${file}

 	    filedim=dim_smstime_file/Dimsmstime_Y${year}M${month}.txt
	    echo 'filedim : ' ${filedim}

	    dim_smstime=`cat ${filedim}`
	    echo $dim_smstime
	    
	    dim_smstime_ok=`expr $dim_smstime - 6 `
	    echo $dim_smstime_ok
	    

	    cpt2=${cpt}
	    if  [ ${cpt} -le 999 ]; then
		cpt2=0${cpt}
	    fi

	    if  [ ${cpt} -le 99 ]; then
		cpt2=00${cpt}
	    fi


	    if  [ ${cpt} -le 9 ]; then
		cpt2=000${cpt}
	    fi
	    
	    ncks -F -d sms_time,5,${dim_smstime_ok} ${file} LAB/roms_frc_QSCAT_Y${year}M${month}_nol.nc
	    #ncks -F -v sustr,svstr ${file} LAB/roms_frc_QSCAT_Y${year}M${month}_nol.nc
	    cpt=`expr $cpt + 1 `
	    month=`expr $month + 1`
	done
	year=`expr $year + 1`
    done
