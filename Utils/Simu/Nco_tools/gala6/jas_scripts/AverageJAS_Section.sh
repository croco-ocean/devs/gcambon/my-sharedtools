#! /bin/bash

yd=1992
md_yd=1
yf=2000
mf_yf=12

#for run in ScratchG6_FluxClim ScratchG6_7D_FluxClim ScratchG6_StERS_FluxClim ScratchG6_StNCEP_FluxClim ScratchG6_StQSCATANCEP_FluxClim;
for run in V_NCEP V_ERS V_QSCATANCEP V_NCEP_DS_7D;
do echo 'Compute '$run
ncra -F -d MONTH_REG,7,9 ${run}/clim_peru_200km.nc ${run}/clim_peru_200km_JAS.nc
ncra -F -d MONTH_REG,7,9 ${run}/clim_chile_100km.nc ${run}/clim_chile_100km_JAS.nc
done
