#! /bin/bash

yd=1992
md_yd=1
yf=2000
mf_yf=12

#run=ScratchG6_StQSCATCLM_FluxClim
#run=ScratchG6_7D_FluxClim
#!
run=ScratchG6_FluxClim
#run=ScratchG6_StERS_FluxClim
#run=ScratchG6_StNCEP_FluxClim
echo 'Compute '$run
filename=roms_avg
var=v

#filename2=vG6_peru
#echo 'Compute Perou Box'
## Perou Box => ncks -F -d eta_v,178,225 comprends le range 7-13°S
#lat1=178
#lat2=225


filename2=vG6_ERS_chile
echo 'Compute Chili Box'
# Chile Box => ncks -F -d eta_v,27,99 comprends le range 27-35°S
lat1=27
lat2=99
#!
year=${yd}
while [ ${year} -le ${yf} ]
do
  if [ ${yd} == ${yf} ]; then
    md=${md_yd}
    mf=${mf_yf}
  elif [ ${year} == ${yd} ]; then
    md=${md_yd}
    mf=12
  elif [ ${year} == ${yf} ]; then
    md=1
    mf=${mf_yf}
  else
    md=1
    mf=12
  fi

  month=${md}
  while [ ${month} -le ${mf} ]
  do

   if  [ ${month} -le 9 ]; then
       month2=0${month}
   else
       month2=${month}
   fi
   ncks -F -d eta_v,${lat1},${lat2} -d xi_rho,55, -d eta_rho,${lat1},${lat2} -v ${var},lat_rho,lon_rho ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc  
   #ncecat -O LAB/${filename2}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
    month=`expr $month + 1`
  done

  ncrcat -h LAB/${filename2}_Y${year}M[123456789].nc LAB/${filename2}_Y${year}M1[012].nc LAB/${filename2}_Y${year}.nc
  year=`expr $year + 1`
done

ncrcat -h LAB/${filename2}_Y1992.nc LAB/${filename2}_Y1993.nc LAB/${filename2}_Y1994.nc LAB/${filename2}_Y1995.nc LAB/${filename2}_Y1996.nc LAB/${filename2}_Y1997.nc  LAB/${filename2}_Y1998.nc  LAB/${filename2}_Y1999.nc LAB/${filename2}_Y2000.nc ${run}/${filename2}_${yd}-${yf}.nc

\rm LAB/${filename2}_Y*.nc

