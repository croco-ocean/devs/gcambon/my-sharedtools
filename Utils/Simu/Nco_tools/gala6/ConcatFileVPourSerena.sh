#! /bin/bash
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#filename2=v_peru
#echo 'Compute Perou Box'
## Perou Box => ncks -F -d eta_v,178,225 comprends le range 7-13°S
#lat1=178
#lat2=225
#
#    filename2=v_chile
#    echo 'Compute Chili Box'
# Chile Box => ncks -F -d eta_v,27,99 comprends le range 27-35°S
#    lat1=27
#    lat2=99
#

#for run in ScratchG6_FluxClim ScratchG6_StERS_FluxClim ScratchG6_StNCEP_FluxClim ScratchG6_StQSCATANCEP_FluxClim  ScratchG6_StQSCATCLM_FluxClim;
#for run in ScratchG6_StERS_FluxClim ScratchG6_StNCEP_FluxClim ;
#for run in ScratchG6_7D_FluxClim;
#for run in ScratchG6_StQSCATANCEP_FluxClim;
#for run in ScratchG6_FluxClim ScratchG6_StERS_FluxClim ScratchG6_StNCEP_FluxClim ScratchG6_StQSCATANCEP_FluxClim  ScratchG6_StQSCATCLM_FluxClim;
#for run in ScratchG6_StQSCATCLM_FluxClim;
for run in  ScratchG6_StQSCATANCEP_FluxClim;
do 
    echo '##############################'
    echo 'Clean '${run}/LAB
    echo '=============================='
    rm -Rf ${run}/LAB/*

    cpt=1
    yd=1992
    yf=2000
    md_yd=1
    mf_yf=12

    echo 'Compute '$run
    filename=roms_avg 
    var=v
    for filename2 in v_peru v_chile ;
    do 
	echo 'Compute ncks '$filename2
	if   [ $filename2 == 'v_peru' ]  ; then 
	    lat1=178
	    lat2=225
	elif [ $filename2 == 'v_chile' ] ; then 
	    lat1=27
	    lat2=99
	fi

	year=${yd}
	while [ ${year} -le ${yf} ]
	do
	    if [ ${yd} == ${yf} ]; then
		md=${md_yd}
		mf=${mf_yf}
	    elif [ ${year} == ${yd} ]; then
		md=${md_yd}
		mf=12
	    elif [ ${year} == ${yf} ]; then
		md=1
		mf=${mf_yf}
	    else
		md=1
		mf=12
	    fi
	    month=${md}
	    while [ ${month} -le ${mf} ]
	    do    
		if  [ ${month} -le 9 ]; then
		    month2=0${month}
		else
		    month2=${month}
		fi
		ncks -F -d eta_v,${lat1},${lat2} -d xi_rho,55, -d eta_rho,${lat1},${lat2} -v scrum_time,${var},lat_rho,lon_rho ${run}/${filename}_Y${year}M${month}.nc ${run}/LAB/${filename2}_Y${year}M${month}.nc  
		echo 'M= '${month}
		month=`expr $month + 1`
	    done
	    echo 'Y= '${year}
	    year=`expr $year + 1`
	done   
    

	echo 'Compute renumbering '$filename2
	cpt=1
       	year=${yd} 
	while [ ${year} -le ${yf} ] 
	do 
	    if [ ${yd} == ${yf} ]; then
		md=${md_yd} mf=${mf_yf} 
	    elif [ ${year} == ${yd} ]; then 
		md=${md_yd}
		mf=12 
	    elif [ ${year} == ${yf} ]; then 
		md=1 mf=${mf_yf} 
	    else 
		md=1 
		mf=12
	    fi
	    month=${md}
	    while [ ${month} -le ${mf} ]
	    do
		file=${run}/LAB/${filename2}_Y${year}M${month}.nc
		cpt2=${cpt}
		
		if  [ ${cpt} -le 999 ]; then
		    cpt2=0${cpt}
		fi
		
		if  [ ${cpt} -le 99 ]; then
		    cpt2=00${cpt}
		fi
		
		if  [ ${cpt} -le 9 ]; then
		    cpt2=000${cpt}
		fi
		
		mv $file ${run}/Processed_Serena/${filename2}_Y$cpt2.nc
                #ncks -v scrum_time,v,lon_rho,lat_rho $file ${run}/Processed_Serena/vG6_chile_Y$cpt2.nc
		cpt=`expr $cpt + 1 `
		month=`expr $month + 1`
	    done
	    year=`expr $year + 1`
	done
    done
rm -Rf ${run}/LAB/*
done

