#! /bin/sh
ncra roms_avg_Y1997M12.nc roms_avg_Y1998M1.nc roms_avg_Y1998M2.nc roms_avg_deb98.nc
ncra roms_avg_Y1998M12.nc roms_avg_Y1999M1.nc roms_avg_Y1999M2.nc roms_avg_deb99.nc

ncwa -O -a time roms_avg_deb98.nc roms_avg_deb98.nc
ncwa -O -a time roms_avg_deb99.nc roms_avg_deb99.nc

ncdiff roms_avg_deb98.nc roms_avg_deb99.nc anm_98-99.nc
