#!/bin/bash


processmonth_flag=0
processtimeinterp_flag=1

listvar='v10 u10 t2 q2 radlw radsw precip'
listyear=`seq 1999 2000`
listmonth=`seq 1 12`
iimonth=-1

#============================================================================================================
if [ $process_month == 1 ] ; then 
    
    for var in $listvar ; do
	
	if [ $var == 'q2' ] || [ $var == 't2' ] || [ $var ==  'u10' ] || [ $var == 'v10' ] ; then  
	    
	    indmonth_noleap=(1 249 473 721 961 1209 1449 1697 1945 2185 2433 2673 2921)
	    indmonth_leap=(1 249 481 729 969 1217 1457 1705 1953 2193 2441 2681 2929)
	else
	    
	    indmonth_noleap=(1 32 59 90 120 151 181 212 243 273 304 334 365)
	    indmonth_leap=(1 32 60 91 121 152 182 213 244 274 305 335 366)
	fi
	
	echo 'Process DSF in "romstools monthly format ...'
	echo '===> Process var. '$var
	echo ' '
	
	for yy in $listyear ; do
	    echo '   ===> Process year. '$yy
	    for mm0 in $listmonth ; do 
		mm=$((mm0-1))
		echo '   ===> Process month. '$mm0
		
		if [ $yy == '1996' ] || [ $yy == '2000' ] || [ $yy == '2004' ] || [ $yy == '2008' ] || [ $yy == '2012' ] ; then
		    echo 'LEAP YEAR'
		    indx=${indmonth_leap[$mm]}
		    indxp=$((${indmonth_leap[$(( mm + 1))]} - 1 ))
		else
		    echo 'LEAP YEAR'
		    indx=${indmonth_noleap[$mm]}
		    indxp=$(( ${indmonth_noleap[$(( mm + 1 ))]} -1 ))
		    
		fi   
		echo '   ===> . indx=  '$indx
		echo '   ===> . indxp= '$indxp
		ncks -F -d time,$indx,$indxp drowned_${var}_DFS5.2_y${yy}.nc -o Lab/${var}_DFS5.2_RTformat_Y${yy}M${mm0}.nc
		ncrename -O -d lat0,lat -d lon0,lon -v lat0,lat -v lon0,lon Lab/${var}_DFS5.2_RTformat_Y${yy}M${mm0}.nc Lab/${var}_DFS5.2_RTformat_Y${yy}M${mm0}.nc
		
		
	    done
	done
    done
    
fi
#==============================================================================
if [ $processtimeinterp_flag == 1 ] ; then 



fi
#==============================================================================
if [ $processextraction_flag == 1 ] ; then 



fi
