#!/bin/bash

createdir_fflag=1
gunzip_fflag=1

yeardeb=1979
yearend=2010

# Gildas
#indlatmin=260
#indlatmax=490

# Gaelle (+ gildas)
#indlatmin=240
#indlatmax=490

# Steven + Gaelle + Gildas
indlatmin=240
indlatmax=497

#for dirin in V-wind ; do 
for dirin in heatflux U-wind V-wind precip_rate specific temp_2m ; do 
    echo 'PROCESS VAR in '$dirin
    cd $dirin 

#Create the year dir
    if [ ${createdir_fflag} == 1 ] ; then 
	for yy in `seq ${yeardeb} ${yearend}` ; do
	    dir=y${yy} ; echo 'Process '${dir}
	    rm -Rf Lab/ ; rm -Rf ${dir}

	    if [ ! -d "${dir}" ] ; then
		echo 'Create dir. 'y$yy
		mkdir ${dir}
	    fi
	done 
    fi
    
    if [ ${gunzip_fflag} == 1 ] ; then 	
	for yy in `seq ${yeardeb} ${yearend}` ; do 
            echo 'Process year '$yy
            echo ''
	    cp *.${yy}*.nc.gz y${yy}/ 
	    cd y${yy}/
	    listf=`ls -1 *.${yy}*.nc.gz`
	    for ffile in $listf ; do 
		echo 'Process file '$ffile
		ffile2=`echo $ffile | cut -d. -f1-4`  
		gunzip ${ffile2}.nc.gz
		ncks -F -d lat,${indlatmin},${indlatmax} ${ffile2}.nc -o extr_${ffile2}.nc  
		rm ${ffile2}.nc  
	    done
	    echo '===> Extraction done ....'
	    cd ../
	done
    fi
    
    cd ../
done
