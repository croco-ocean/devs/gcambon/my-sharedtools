#!/bin/bash

#Finistere
lonmin=660
lonmax=730
latn=530
lats=490

extract_flag=1
processedRT_flag=1

if [[ $extract_flag == 1  ]] ; then 
    rm MANGA_Extracted/*.nc
    for filein in `ls *.nc`; do
	fileout=extracted_${filein}
	ncks -d latitude,${lats},${latn} -d longitude,${lonmin},${lonmax} ${filein} -o MANGA_Extracted/${fileout}
	echo ${filein}
	#echo ${fileout}.nc
	bb=`echo $fileout | cut -c 1-20`
	nfilout='ascat_'${bb}'.nc'
	cd MANGA_Extracted
	mv ${fileout} ${nfilout}
	cd -
    done
fi


rm -Rf MANGA_Extracted/Processed_for_RT/*.nc

if [[ $processedRT_flag == 1 ]] ; then 
    for year in 2009 2010 2011 2012 2013 ; do	
	for month in 01 02 03 04 05 06 07 08 09 10 11 12 ; do 
	    
	    echo ' => Process Year ' $year ' - Month '$month
	    echo ' ======================================  '
	    cd MANGA_Extracted/
	    for i in `ls -1 ascat_extracted_${year}${month}*` ; do
		echo $i	
		#ncrename -O -v time,my_time $i $i
		ncks -O --mk_rec_dmn time $i $i
		#ncrename -O -v my_time,time $i $i
		ncpdq -O -U $i $i             # unpack
		#ncecat -O $i $i               # add record unlimited dimension
		ncpdq -O -a time,record $i $i # switch record en time dimension
		ncwa -O -a depth $i $i       # remove the singleton record
	    done
	    
	    listfile2=`ls -1 ascat_extracted_${year}${month}*`           
	    ncrcat $listfile2 -o manga_ascat_Y${year}M${month}.nc  # record clm
	    mv manga_ascat_Y${year}M${month}.nc Processed_for_RT/ 
	    echo 'DONE ...'
	    cd -
	done
    done
fi
