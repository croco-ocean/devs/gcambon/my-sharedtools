#!/bin/bash

#Clean Lab/ dir 
rm -Rf Lab/*

#South africa 
#------------
dir_extract='SudAf_extr'
latmin=75
latmax=375
lonmin=685
lonmax=1010

#Perou
#------------
#dir_extract='Peru_extr'
#latmin=110 
#latmax=355
#lonmin=210
#lonmax=450

# extraction variable
for year in `seq 2000 2008`; do
    echo '=>Process year '$year
    for month in 01 02 03 04 05 06 07 08 09 10 11 12; do
	#for month in 01; do
	echo '   '
	echo '==> Process month '$month
	listfiles=`ls -1 qscat_${year}${month}*`
	for i in ${listfiles} ; do 
	    echo ${i}
	    ncks -d latitude,$latmin,$latmax -d longitude,$lonmin,$lonmax -v time,latitude,longitude,wind_speed,eastward_wind,northward_wind,wind_stress,surface_downward_eastward_stress,surface_downward_northward_stress $i -o Lab/extr_$i
	done
	
	echo ' Second step...'
	echo '---------------'
	echo '   '
	cd Lab/
	rm file_to_execute
	ls -1 extr_qscat_${year}${month}*> file_to_execute
	for i in `cat file_to_execute` ;
	do
	    echo $i	
	    #ncrename -O -v time,my_time $i $i
	    ncks -O --mk_rec_dmn time $i $i
	    #ncrename -O -v my_time,time $i $i
	    ncpdq -O -U $i $i             # unpack
	    #ncecat -O $i $i               # add record unlimited dimension
	    ncpdq -O -a time,record $i $i # switch record en time dimension
	    ncwa -O -a depth $i $i       # remove the singleton record
	done
	
	listfile2=`ls -1 extr_qscat_${year}${month}*`           
	ncrcat $listfile2 -o peru_extr_qscat_Y${year}M${month}.nc  # record clm
	mv peru_extr_qscat_Y${year}M${month}.nc ../$dir_extract 
	echo 'DONE ...'
	cd .. 
    done
done
