#!/bin/bash

dirin='/net/centaure/local/tmp/1/ragoasha/BUIC/Outputs_PhD/blkperio_clim/'
# Parse the anoma squere file and extract u and v
for i in `seq 1 72` ; do 
    if [ $i -le 9 ] ; then 
	ii2='0'$i
#	echo $ii2
    else
	ii2=$i
    fi    
    file=$dirin'roms_blkperio_clim_2000_anomasquare_M_00'$ii2.nc
    echo 'Process file '$file
    ncks -v u,v $file Lab/uv_anomsquare_${ii2}.nc
done 

# concatenat in a saeme file
cd Lab/
listfile=`ls -1 *.nc`
ncrcat $listfile -o all_uv_anomsquare_2006-2011.nc

# demi somme pour u
ncks -F -d eta_rho,2,410 -v u all_uv_anomsquare_2006-2011.nc -o tmp_u_rho1.nc
ncks -F -d eta_rho,1,409 -v u all_uv_anomsquare_2006-2011.nc -o tmp_u_rho2.nc
ncbo -O --op_typ='+' tmp_u_rho1.nc tmp_u_rho2.nc sum_tmp_u_rho3.nc
ncap2 -s 'u=0.5*u' sum_tmp_u_rho3.nc -o demisum_tmp_u_rho4.nc 

# demi somme pour v
ncks -F -d xi_rho,2,362 -v v all_uv_anomsquare_2006-2011.nc -o tmp_v_rho1.nc
ncks -F -d xi_rho,1,361 -v v all_uv_anomsquare_2006-2011.nc -o tmp_v_rho2.nc
ncbo -O --op_typ='+' tmp_v_rho1.nc tmp_v_rho2.nc sum_tmp_v_rho3.nc
ncap2 -s 'v=0.5*v' sum_tmp_v_rho3.nc -o demisum_tmp_v_rho4.nc

# Rename
ncrename -v u,u_rho -d xi_u,xi_rho demisum_tmp_u_rho4.nc 
ncrename -v v,v_rho -d eta_v,eta_rho demisum_tmp_v_rho4.nc 


# Append oall in a same file
ncks -A demisum_tmp_u_rho4.nc demisum_tmp_v_rho4.nc 
ncks -A demisum_tmp_v_rho4.nc all_uv_anomsquare_2006-2011.nc

#ncks -H -A demisum_tmp_v_rho4.nc all_uv_anomsquare_2006-2011.nc

# Compute the EKE
# Put the u and v at rho point

ncap2 -F -s "EKE=0.5*(sqrt(u_rho * u_rho + v_rho * v_rho))" all_uv_anomsquare_2006-2011.nc -o all_eke_uv_anomsquare_2006-2011.nc
ncra all_eke_uv_anomsquare_2006-2011.nc -o mean_eke_anomsquare_2006-2011.nc

mv mean_eke_anomsquare_2006-2011.nc all_eke_uv_anomsquare_2006-2011.nc ../
cd ../
