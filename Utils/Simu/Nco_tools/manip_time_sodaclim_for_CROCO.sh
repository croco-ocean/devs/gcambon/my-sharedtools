#/bin/bash

 for i in `ls roms_clm_Soda2_*.nc` ; do
     echo ' '
     echo $i
   
     for var in temp salt u v ubar vbar SSH zeta; do
 	echo "ncks -v $var $i LAB/${var}_$i"
 	ncks -v $var $i LAB/${var}_$i
     done
   
     #ncks -d zeta_time,time -d ssh_time,time -d uclm_time,time -d vclm_time,time -d sclm_time,time -d tclm_time,time -d v2d_time,time -d v3d_time,time $i LAB/$i  ;
   
 done
 
 cd LAB
 for var in temp salt u v ubar vbar zeta; do
 #for var in temp; do
     echo $var
     for file in `ls -1 ${var}_roms_clm_Soda2*.nc`; do 
 	echo "Process file ...."
 	echo '..........   '$file

 	## extract time dimension name
 	echo ""ncdump -h ${file} | grep "${var}(" > extract_time_dim_tmp""
 	ncdump -h ${file} | grep "${var}(" > extract_time_dim_tmp
 	cat extract_time_dim_tmp
         time_dim=`cat extract_time_dim_tmp | cut -d "("  -f2 | cut -d , -f1`
 	rm extract_time_dim_tmp

 	## make record dimension unlimited
 	echo "ncks --mk_rec_dmn $time_dim ${file} nn_${file}"
 	ncks -O --mk_rec_dmn $time_dim ${file} nn_${file}
 	mv nn_${file} ${file}

 	## change name of the reocred dimension
 	ncrename -O -v $time_dim,time -d $time_dim,time ${file}

 	## extract the second value
 	ncks -O -F -d time,2,2  ${file} ${file}
     done
 done
 cd -

cd LAB
for i0 in {1..12}; do
    echo $i0
    ii=$(printf "%02d" ${i0})
    echo $ii
    for file in `ls -1 *M${i0}.nc`;  do
        file2=`echo $file | cut -d 'M' -f1`
	filef=${file2}'M'${ii}'.nc'
	echo $filef
	mv $file  $filef
    done
done
cd -

cd LAB
for var in temp salt u v ubar vbar zeta; do
    #for var in temp; do
    echo $var
    filelist=`ls -1 ${var}_roms_clm_Soda2_Y2000M*.nc`
    echo "Process filelist ...."
    echo '..........   '$filelist
    ncrcat $filelist ${var}_roms_clm_Soda2_M1-12.nc
done
cd -

mv LAB/*clm_Soda2_M1-12.nc .
cp temp_roms_clm_Soda2_M1-12.nc roms_clm_Soda2_M1-12.nc
for i in salt_*M1-12.nc u_*M1-12.nc v_*M1-12.nc ubar_*M1-12.nc vbar_*M1-12.nc zeta_*M1-12.nc; do
    echo $i 
    ncks -A $i roms_clm_Soda2_M1-12.nc
done
