#! /bin/bash

yd=2005
md_yd=1
yf=2008
mf_yf=12

for run in PCC12_bxd ; 
do echo 'Compute '$run
filename=longhcs_avg
var=temp
filename2=zeta

year=${yd}
while [ ${year} -le ${yf} ]
do
  if [ ${yd} == ${yf} ]; then
    md=${md_yd}
    mf=${mf_yf}
  elif [ ${year} == ${yd} ]; then
    md=${md_yd}
    mf=12
  elif [ ${year} == ${yf} ]; then
    md=1
    mf=${mf_yf}
  else
    md=1
    mf=12
  fi

  month=${md}
  while [ ${month} -le ${mf} ]
  do

   if  [ ${month} -le 9 ]; then
       month2=0${month}
   else
       month2=${month}
   fi

#   ncks -v scrum_time,lat_rho,lon_rho,${var} ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
#   ncks -v scrum_time,lat_rho,lon_rho,zeta,temp,salt,hbl ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
#   ncrcat -F -d s_rho,32,32 -v ${var} ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
    ncks -v scrum_time,zeta ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
   
   month=`expr $month + 1`
   echo ${month}
  done
  ncrcat -h LAB/${filename2}_Y${year}M[123456789].nc LAB/${filename2}_Y${year}M1[012].nc LAB/${filename2}_Y${year}.nc
  year=`expr $year + 1`
  echo ${year}
done
echo "Compute final"
ncrcat -h LAB/${filename2}_Y2005.nc LAB/${filename2}_Y2006.nc LAB/${filename2}_Y2007.nc LAB/${filename2}_Y2008.nc ${run}/${filename2}_${yd}-${yf}.nc
#mv LAB/${filename2}_Y${yf}.nc ./${run} 
#rm -Rf LAB/${filename2}_Y*.nc
done
