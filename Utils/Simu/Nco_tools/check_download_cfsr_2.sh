#!/bin/bash

indvar=-1
rm -f all_*

#var in     heatflux U-wind V-wind precip_rate specific temp_2m temp_surf
#minimum size Ok for non besiectile februabry month

#          heatflux U-wind V-wind precip_rate specific temp_2m temp_surf
sizevarlim=(46       15     15     5        18       18      18)

#V-wind
#sizevarlim=(15)

for var in heatflux U-wind V-wind precip_rate specific temp_2m temp_surf ; do 
#for var in V-wind ; do 
echo 'Check var '$var
indvar=$(( indvar + 1 ))
echo 'indvar='${indvar}
rm -f ${var}_out*

cd ${var}
du -ms *.gz >> ../${var}_out

#Parse the out file ...
echo 'Check var '$var >> ../all_out2
echo '===========' >> ../all_out2
while read line  
do
#detect line number with 3 first digit < 50
   bb=`echo -e $line`
   #echo ${bb}
   aa0=`echo $line | cut -d' ' -f1`
   aa=$(echo $aa0 | tr -d ' ')
   #echo ${aa0}
   if [ ${aa0} -lt ${sizevarlim[${indvar}]} ]; then
   #if [ ${aa0} -lt 10 ]; then
   echo ${bb} >> ../all_out2
   fi 
   
done < ../${var}_out

cd -
done
