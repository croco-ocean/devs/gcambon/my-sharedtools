#!/bin/bash

# - Extract the various variable for each month
#
#================= 

#==========================
extract=1
concat=1
concat_allvar=1
upload=0

YSTART=2002
YEND=2002

#endf=''
#motif='_6h_periodic_2002_2003_filt10d_'

#for motif in _6h_periodic_2002_2003_filt3d_ _6h_periodic_2002_2003_filt5d_ _6h_periodic_2002_2003_filt10d_ _6h_periodic_2002_2003_filt30d_ ; do

for endf in '' '_1' ; do
#for endf in '_1' ; do
    echo 'Process the grid '$endf
    #for motif in _6h_periodic_2002_2003_filt3d_ _6h_periodic_2002_2003_filt5d_ _6h_periodic_2002_2003_filt10d_ _6h_periodic_2002_2003_filt30d_ ; do
	    for motif in '_6h_periodic_2002_2003_' ; do 
	    #for motif in '_6h_periodic_2002_2003_filt30d_' ; do 
	    echo $motif

	    #==========================
	    
	    if [ ${extract} == 1 ]; then 
		for var in sustr svstr shflux swflux swrad ; do
		# for var in swrad ; do
		    if [ $var == sustr ]; then
			vartime=sms_time
		    elif [ $var == svstr ]; then
			vartime=sms_time
		    elif [ $var == shflux ]; then
			vartime=shf_time
		    elif [ $var == swflux ]; then
			vartime=swf_time
		    elif [ $var == swrad ]; then
			vartime=srf_time
		    fi
		    
		    echo '  '
		    echo 'Extract var '$var
		    echo '================'
		    
		    for YY in `seq $(( $YSTART )) $(( $YEND ))` ; do 
			for MM in `seq 1 12` ; do 	
			    file=roms_frc${motif}Y${YY}M${MM}.nc${endf}
			    echo ' : ... in file '$file
			    
			    echo '       => extraction'
			    ncks -v $var $file -o Lab/${var}_${file}
			    
			    echo '       => mk rec dim'
			    ncks -O --mk_rec_dmn ${vartime} Lab/${var}_${file} Lab/${var}_${file}	
			done  # month
		    done # year
		    
		    echo 'Process extra file'
		    fileEND=${var}_roms_frc${motif}Y2002M12.nc${endf}
		    fileEND2=${var}_roms_frc${motif}Y2001M12.nc${endf}
		    
		    fileDEB=${var}_roms_frc${motif}Y2002M1.nc${endf}
		    fileDEB2=${var}_roms_frc${motif}Y2003M1.nc${endf}
		    
		    cd Lab/
		    cp ${fileEND} ${fileEND2}
		    ncap2 -O -s " ${vartime} = ${vartime} - 365 " ${fileEND2} ${fileEND2}
		    
		    cp ${fileDEB} ${fileDEB2}
		    ncap2 -O -s " ${vartime} = ${vartime} + 365 " ${fileDEB2} ${fileDEB2}
		    cd -
		    
		done # var
	    fi
	    
	    #=======================
	    
	    if [ ${concat} == 1 ]; then
		
		cd Lab/ 
		
		for var in sustr svstr shflux swflux swrad ; do
		#for var in swrad ; do
		    echo '=================================================='
		    echo 'Process the var '$var
		    echo '=================================================='
		    for YY in `seq $YSTART $YEND` ; do 
			for MM in `seq 1 12` ; do 
			    
			    #	

			    if [ ${MM} == 1  ]; then
				YP=$(( $YY - 1 ))
				MP=12
				YN=$YY
				MN=2
				
			    elif [ ${MM} == 12  ]; then
				YP=$YY
				MP=11
				YN=$(( $YY + 1 ))
				MN=1    

			    #elif [ ${MM} == 2 || ${MM} == 3 ]; then
			    #	
			    #
 	
			    else
				YP=$YY
				MP=$(( $MM - 1 ))
				YN=$YY
				MN=$(( $MM + 1 ))
			    fi
			    #
			    prevfile=${var}_roms_frc${motif}Y${YP}M${MP}.nc${endf}
			    nextfile=${var}_roms_frc${motif}Y${YN}M${MN}.nc${endf}
			    echo 'Currrent file = ' ${var}_roms_frc${motif}Y${YY}M${MM}.nc${endf}
			    echo 'Prev. file = ' $prevfile
			    echo 'Next. file = ' $nextfile
			    echo '  '
			    ncrcat -O  ${prevfile} ${var}_roms_frc${motif}Y${YY}M${MM}.nc${endf} ${nextfile} ext_${var}_roms_frc${motif}Y${YY}M${MM}.nc${endf}
			    
			    # Remove the unlimited time dimensions
			    ffile=ext_${var}_roms_frc${motif}Y${YY}M${MM}.nc${endf}
			    ncecat -O  ${ffile} ${ffile} 
			    #Remove the record degenerated dimension + do it in 64 bit large file format
			    ncwa -O -6 -a record ${ffile} ${ffile} 
			done  # month
		    done # year   
		done # var
		cd -
	    fi
	    
	    #=======================
	    
	    if [ ${concat_allvar} == 1 ]; then
		cd Lab/
		for YY in `seq $YSTART $YEND` ; do 
		    for MM in `seq 1 12` ; do 
			echo 'Process the ncks ...'
			echo '  '
			cp ext_sustr_roms_frc${motif}Y${YY}M${MM}.nc${endf} build_roms_frc${motif}Y${YY}M${MM}.nc${endf} 
			
			ncks -A ext_svstr_roms_frc${motif}Y${YY}M${MM}.nc${endf}  build_roms_frc${motif}Y${YY}M${MM}.nc${endf} 
			ncks -A ext_shflux_roms_frc${motif}Y${YY}M${MM}.nc${endf}  build_roms_frc${motif}Y${YY}M${MM}.nc${endf} 
			ncks -A ext_swflux_roms_frc${motif}Y${YY}M${MM}.nc${endf}  build_roms_frc${motif}Y${YY}M${MM}.nc${endf} 
			ncks -A ext_swrad_roms_frc${motif}Y${YY}M${MM}.nc${endf}  build_roms_frc${motif}Y${YY}M${MM}.nc${endf} 
			
			mv build_roms_frc${motif}Y${YY}M${MM}.nc${endf} ../Extended/ext_roms_frc${motif}Y${YY}M${MM}.nc${endf} 
		    done
		done
		cd -
	    fi
	    
	    #=======================
	    
	    if [ $upload == 1 ]; then 
		rsync -avz --progress ext_roms_frc${motif}Y\*M\*.nc${endf} rkhe002@ada.idris.fr:/workgpfs/rech/khe/rkhe002/INPUT/BENGSAFE_R4/Y2000/atmo/6h_2002-2004_frc/
		
	    fi
	done
    done
