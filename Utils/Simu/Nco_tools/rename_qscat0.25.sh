#!/bin/bash

ls -1 *.bz2 > listfiles
for ii in `cat listfiles` ; do 
    echo $ii
    newname=`echo ${ii} | cut -c1-21`
    echo $newname
    mv ${ii} qscat_${newname}_qscat_0.25_daily.nc.bz2
done
