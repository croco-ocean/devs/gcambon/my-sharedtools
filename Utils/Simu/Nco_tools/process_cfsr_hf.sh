#!/bin/bash

process_var=1
if [ ${process_var} == 1 ]; then 
    flag_extractvar=1
    flag_concatfile=1
    flag_renamefile=1
    flag_invertlat=1

    flag_cleaning_extVAR=0
    flag_cleaning_RTVAR=0
    flag_realcleaning_unzip=0
else 
    flag_extractvar=0
    flag_concatfile=0
    flag_renamefile=0
    flag_invertlat=0  
    
    flag_cleaning_extVAR=1
    flag_cleaning_RTVAR=1
    flag_realcleaning_unzip=1
fi

yeardeb=1979
yearend=2010

#centaure
#dirin='/local/tmp/1/gcambon/DATA_CFSR'
# adai
dirin=$WORKDIR'/BCK_CFSR_DATA_1979_2010'



for vv in hf uwind vwind precip specific temp_2m ; do 
#for vv in vwind ; do 
    echo ${vv}
    myvar=${vv}
    
    if [ $myvar == hf ] ; then 
	#HEAT FLUX (4 variables)
	varname0=(DSWRF_L1_Avg_1 DLWRF_L1_Avg_1 ULWRF_L1_Avg_1 USWRF_L1_Avg_1)
	varnameRT=(Downward_Short-Wave_Rad_Flux_surface Downward_Long-Wave_Rad_Flux Upward_Long-Wave_Rad_Flux_surface Upward_Short-Wave_Rad_Flux_surface)
	dirname=${dirin}'/heatflux/'
	
    elif [ $myvar == uwind ]  ; then 
	#Uwind
	varname0=(U_GRD_L103)
	varnameRT=(U-component_of_wind)
	dirname=${dirin}'/U-wind'
	
    elif [ $myvar == vwind ]  ; then 
	#Vwind
	varname0=(V_GRD_L103)
	varnameRT=(V-component_of_wind)
	dirname=${dirin}'/V-wind'
	
    elif [ $myvar == precip ]  ; then 
	#precip_rate]
	varname0=(PRATE_L1_Avg_1)
	varnameRT=(Precipitation_rate)
	dirname=${dirin}'/precip_rate'
	
    elif [ $myvar == temp_2m ]  ; then 
	#temp_2m
	varname0=(TMP_L103)
	dirname=${dirin}'/temp_2m'
	varnameRT=(Temperature_height_above_ground)

    elif [ $myvar == specific ]  ; then 
	#specific
	varname0=(SPF_H_L103)
	dirname=${dirin}'/specific'
	varnameRT=(Specific_humidity)
	
    elif [ $myvar == temp_surf ]  ; then 
	#temp_surf
	varname0=(TMP_L1)
	dirname=${dirin}'/temp_surf'
	varnameRT=(Temperature_surface)
    fi
    
    listvar0=`echo ${varname0[*]}`
    listvarRT=`echo ${varnameRT[*]}`
    
    echo '===== PROCESS '$listvar0'========'
    echo ${dirname}
    cd ${dirname}
    echo $PWD
    
    for yy in `seq $yeardeb $yearend` ; do
	cd y${yy}
	echo $PWD
	listf=`ls -1 extr*.nc` 

	for var in $listvar0 ; do
	    for ffile in ${listf} ; do 
		if [ ${flag_extractvar} == 1 ] ; then
		    echo ' '
		    echo '============================'
		    echo '==>001 ncks the variable '$var
		    echo ' '
		    echo '... in file '$ffile
		    #echo 'ncks -O -v ' ${var}' '${ffile}
		    ncks -O -v ${var} ${ffile} ${var}_${ffile}
		fi
	    done
	done

	#===============================================
	
	if [ ${flag_concatfile} == 1 ] ; then	 
	    for var in $listvar0 ; do
		echo ' '
		echo '==>002 ncrcat the variable '$var		    
		listf2=`ls -1 ${var}_extr_*`
		ncrcat -O ${listf2} -o ${var}_y${yy}.nc
		rm -Rf ${listf2}
	    done
	fi

	#===============================================
		
	if [ ${flag_renamefile} == 1 ] ; then
	    lengthvar=${#varname0[@]}
	    echo '${lengthvar}='${lengthvar}
	    for k in `seq 0 $(( ${lengthvar} - 1))` ; do
		vnamein=${varname0[$k]}
		vnameout=${varnameRT[$k]}
	       	echo ' '
		echo '==>003 ncrename the variable ' ${vnamein} ' in ' ${vnameout}
		ncrename -O -v ${varname0[$k]},${varnameRT[$k]}  ${vnamein}_y${yy}.nc ${vnameout}_Y${yy}_tmp.nc
	        rm -Rf ${vnamein}_y${yy}.nc 
	    done
	fi
	
	#===============================================
	
	if [ ${flag_invertlat} == 1 ] ; then
	    lengthvar=${#varnameRT[@]}
	    echo ' '
	    echo '${lengthvar}='${lengthvar}
	    for k in `seq 0 $(( ${lengthvar} - 1))` ; do	   
		vnameout=${varnameRT[$k]}
 
		echo '==>004 Invert lat for '${vnameout}
		# Invert lat
		ncpdq -O -a -lat ${vnameout}_Y${yy}_tmp.nc ${vnameout}_Y${yy}_tmp.nc
                rm -Rf *.ncpdq.tmp
	
	    done	
	fi
	
	#===============================================
	
	if [ ${flag_cleaning_extVAR} == 1 ] ; then	 
	    for var in $listvar0 ; do
		echo ' '
		echo '==>005.1 cleaning the variable '$var		    
		listf2=`ls -1 ${var}_*.nc`
		rm -Rf ${listf2}
		done
	fi
	
	#===================================	
	
	if [ ${flag_cleaning_RTVAR} == 1 ] ; then
		for var in $listvarRT ; do
	  	    echo ' '
	    	    echo '==>005.2 cleaning the variable '$var		    
	    	    listf2=`ls -1 ${var}_*.nc`
	    	    rm -Rf ${listf2}
		done
	fi
	
	#==========================================
	
	if [ ${flag_realcleaning_unzip} == 1 ] ; then	 
	    echo ' '
	    echo '==>006 remove the unzip file '
	    echo $PWD
	    listall=`ls -1 extr_*nc`
	    rm -Rf ${listall}
	fi
	#------
	cd ..
	#=======================================================================
    done   
    cd .. 
done
