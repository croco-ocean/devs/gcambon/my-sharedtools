#!/bin/bash

for yy in `seq 1997 2006` ; do
    #echo $yy
    for mm in {1..9} ; do
        #echo $mm
	for var in U10M V10M ; do
	    echo "========================"
	    filein=${var}_Y${yy}M0${mm}.nc
	    echo $filein
	    bb=`echo $filein | cut -dM -f3-`
	    echo $bb
	    cc=`echo $filein | cut -dM -f1-2`
	    echo $cc
	    fileout=$cc'M'$mm'.nc'
	    echo $fileout
	    CMD="ln -sf $filein $fileout"
	    echo $CMD
	    #$CMD
	done
    done
done
