#!/bin/bash

for yy in `seq 2000 2005` ; do
    echo $yy
    for mm in `seq 1 9` ; do
	echo $mm
	filein=croco_avg_Y${yy}M${mm}.nc
	echo $filein
	bb=`echo $filein | cut -dM -f2-`
	echo $bb
	cc=`echo $filein | cut -dM -f1-1`
	echo $cc
	fileout=$cc'M0'$bb
	echo $fileout
	CMD="mv $filein $fileout"
	echo $CMD
	$CMD
    done
done
