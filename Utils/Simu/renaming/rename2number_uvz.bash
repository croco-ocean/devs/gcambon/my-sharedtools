#!/bin/bash

ROMSDIR=$PWD
SAVEDIR=$PWD

YEARMIN=2005
YEARMAX=2011
endfile=''

echo "from $YEARMIN to $YEARMAX"
cd $ROMSDIR
YEAR=$YEARMIN
im=0
while [ $YEAR -le $YEARMAX ]
do
    MONTH=0
    while [ $MONTH -lt 12 ]
    do
	MONTH=$((MONTH+1))
	im=$((im+1))
	echo '========================================='
        echo "MONTHLY AVG : roms_avg_Y${YEAR}M${MONTH}.nc${endfile}"
	echo '-->' $YEAR $MONTH $im
	echo '  '
	#--
	MONTH2=$MONTH
        romsfile=${ROMSDIR}/roms_avg_Y${YEAR}M${MONTH2}.nc${endfile}
	
        if [ "${im}" -lt  "10" ]
        then
               nfile="000${im}"
        elif [ "${im}" -lt  "100" -a "${im}" -ge  "10" ]
             then
		 nfile="00${im}"
        elif [ "${im}" -lt  "1000" -a "${im}" -ge  "100" ]
        then
		 nfile="0${im}"
        elif [ "${im}" -lt  "10000" -a "${im}" -ge  "1000" ]
        then
		 nfile="${im}"
        fi
        outfile=${SAVEDIR}/roms_avg_M_${nfile}.nc${endfile}
	
        CMD="ln -sf $romsfile $outfile"
        echo $CMD
	$CMD
    done
    YEAR=$(( YEAR+1 ))
done
