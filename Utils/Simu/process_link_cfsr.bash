#!/bin/bash
#
# Programme pour linker les fichier croco_blk_Yxx.nc et croco_frc_Yxx.nc vers
# croco_blk_YxxMxx.nc et croco_frc_YxxMxx.nc
# 

##listvar='lap_stress_croco_frc lap_croco_blk'
#listvar='lap_stress_croco_frc_CFSR'
listvar='lap_croco_blk_CFSR'

#Create monthly DFS files vy linking them their respective yearly files

years=`seq 2005 2008`
months=`seq 1 12`

for yy in $years ; do
  echo '   ===> Process year. '$yy
  for mm in $months ; do
    echo '   ===> Process month. '$m
     for var in $listvar ; do
      echo $var
      outputfile="croco_blk_CFSR_Y${yy}M${mm}.nc"
      echo ${outputfile}
      ln -sf ${var}_Y${yy}.nc $outputfile
     done
   done
done
