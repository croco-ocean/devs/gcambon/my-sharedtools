#!/bin/bash

#=======================================================================================================
# Programme pour creer des fichiers par mois netcdf roms
# frc avec CFSR a 6 heures pour sustr et svstr  par mois + les 12 mois des flux COADS
# =======================================================================================================
rm -Rf Lab/*

YMIN=2007
YMAX=2009
suffix=''

fileflux="roms_frc_climato_coads.nc$suffix"
filefluxextract="fluxes_roms_frc_climato_coads.nc$suffix"

#==================
ncks -v shflux,swflux,SST,SSS,dQdSST,swrad $fileflux -o $filefluxextract

for yy in `seq $YMIN $YMAX` ; do 
    for mm in `seq 1 12`  ; do
	
	fileinCFSR="roms_frc_QSCAT_Y${yy}M${mm}.nc$suffix"
	echo $yy '- ' $mm
	echo $fileinCFSR
	
	if [ -f ${fileinCFSR} ];  then
	    echo "File exist"
	    fileinCFSRextract="stress_roms_frc_QSCAT_Y${yy}M${mm}.nc$suffix"
	    fileflux="fluxes_roms_frc_climato_coads.nc$suffix"
	    fileout="roms_frc_QSCAT_FLXCOADS_Y${yy}M${mm}.nc$suffix"
	    
	    ncks -v sustr,svstr $fileinCFSR $fileinCFSRextract
	    mv $fileinCFSRextract $fileout 
	    ncks -A $fileflux $fileout	    
	else
	    echo "File $fileinCFSR do not exist"
	fi
	echo '==========='
	
	#rm -f $fileinCFSRextract $filefluxextract
    done
done


