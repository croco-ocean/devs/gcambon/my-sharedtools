#!/bin/bash

var='uwnd,vwnd'
bulkgen='roms_blk_CFSR_periodic_midyear_'
suffix='.nc.1 '


ncks -F -O -d bulk_time,9,132 -v $var $bulkgen'Y2000M1'$suffix -o LAB/wind_$bulkgen'Y2000M1'$suffix
ncks -F -O -d bulk_time,9,124 -v $var $bulkgen'Y2000M2'$suffix -o LAB/wind_$bulkgen'Y2000M2'$suffix
ncks -F -O -d bulk_time,9,132 -v $var $bulkgen'Y2000M3'$suffix -o LAB/wind_$bulkgen'Y2000M3'$suffix
ncks -F -O -d bulk_time,9,128 -v $var $bulkgen'Y2000M4'$suffix -o LAB/wind_$bulkgen'Y2000M4'$suffix
ncks -F -O -d bulk_time,9,132 -v $var $bulkgen'Y2000M5'$suffix -o LAB/wind_$bulkgen'Y2000M5'$suffix
ncks -F -O -d bulk_time,9,128 -v $var $bulkgen'Y2000M6'$suffix -o LAB/wind_$bulkgen'Y2000M6'$suffix
ncks -F -O -d bulk_time,9,132 -v $var $bulkgen'Y2000M7'$suffix -o LAB/wind_$bulkgen'Y2000M7'$suffix
ncks -F -O -d bulk_time,9,132 -v $var $bulkgen'Y2000M8'$suffix -o LAB/wind_$bulkgen'Y2000M8'$suffix
ncks -F -O -d bulk_time,9,128 -v $var $bulkgen'Y2000M9'$suffix -o LAB/wind_$bulkgen'Y2000M9'$suffix
ncks -F -O -d bulk_time,9,132 -v $var $bulkgen'Y2000M10'$suffix -o LAB/wind_$bulkgen'Y2000M10'$suffix
ncks -F -O -d bulk_time,9,128 -v $var $bulkgen'Y2000M11'$suffix -o LAB/wind_$bulkgen'Y2000M11'$suffix
ncks -F -O -d bulk_time,9,132 -v $var $bulkgen'Y2000M12'$suffix -o LAB/wind_$bulkgen'Y2000M12'$suffix

cd LAB
pwd
ncrcat wind_$bulkgen'Y2000M'[123456789]$suffix wind_$bulkgen'Y2000M1'[012]$suffix -o wind_$bulkgen'Y2000M1-12'$suffix

mv wind_$bulkgen'Y2000M1-12'$suffix ../
cd ..

