#!/bin/bash

#set -e
#set -x

for yy in `seq 2000 2006` ; do
    for mm in `seq 1 12` ; do

	if [[ $yy == 2000 && $mm == 1 ]]; then
	    trange='1,2'
	elif [[ $yy == 2006 && $mm == 12 ]]; then
	    trange='2,3'
	else
	    trange='2,2'
	fi
	bryfile=croco_bry_SODA_Y${yy}M${mm}.nc
	
	if [ -f $bryfile ]; then
	    echo $bryfile
	    
	    varlist="Cs_r,Cs_w,Tcline,Vstretching,Vtransform,hc,sc_r,sc_w,spherical,tstart,tend,theta_s,theta_b"
	    ncks -F -O -v $varlist $bryfile static_croco_bry_SODA.nc

	    varlist_zeta="-d zeta_time,$trange -v zeta_time,zeta_south,zeta_west,zeta_north,zeta_east"
	    ncks -F -O $varlist_zeta $bryfile -o zeta_$bryfile
	    ncrename -O -d zeta_time,bry_time zeta_$bryfile
	    #ncks -O --mk_rec_dmn bry_time zeta_$bryfile


	    varlist_salt="-d salt_time,$trange -v salt_time,salt_south,salt_west,salt_north,salt_east"
	    ncks -F -O $varlist_salt $bryfile salt_$bryfile
	    ncrename -O -d salt_time,bry_time salt_$bryfile
	    #ncks -O --mk_rec_dmn bry_time salt_$bryfile salt_$bryfile

	    varlist_temp="-d temp_time,$trange -v temp_time,temp_south,temp_west,temp_north,temp_east"
	    ncks -F -O $varlist_temp $bryfile temp_$bryfile
	    ncrename -O -d temp_time,bry_time temp_$bryfile
	    #ncks -O --mk_rec_dmn bry_time temp_$bryfile temp_$bryfile

	    varlist_u2d="-d v2d_time,$trange -v v2d_time,ubar_south,ubar_west,ubar_north,ubar_east"
	    ncks -F -O $varlist_u2d $bryfile u2d_$bryfile
	    ncrename -O -d v2d_time,bry_time u2d_$bryfile
	    #ncks -O --mk_rec_dmn bry_time u2d_$bryfile u2d_$bryfile

	    varlist_u3d="-d v3d_time,$trange -v v3d_time,u_south,u_west,u_north,u_east"
	    ncks -F -O $varlist_u3d $bryfile u3d_$bryfile
	    ncrename -O -d v3d_time,bry_time u3d_$bryfile
	    #ncks -O --mk_rec_dmn bry_time u3d_$bryfile u3d_$bryfile

	    varlist_v2d="-d v2d_time,$trange -v v2d_time,vbar_south,vbar_west,vbar_north,vbar_east"
	    ncks -F -O $varlist_v2d $bryfile v2d_$bryfile
	    ncrename -O -d v2d_time,bry_time v2d_$bryfile
	    #ncks -O --mk_rec_dmn bry_time v2d_$bryfile v2d_$bryfile

	    varlist_v3d="-d v3d_time,$trange -v v3d_time,v_south,v_west,v_north,v_east"
	    ncks -F -O $varlist_v3d $bryfile v3d_$bryfile
	    ncrename -O -d v3d_time,bry_time v3d_$bryfile
	    
	    ncrename -O -v v3d_time,bry_time v3d_$bryfile
	    #ncks -O --mk_rec_dmn bry_time v3d_$bryfile v3d_$bryfile

	    cp zeta_$bryfile ${bryfile}_repro
	    ncks -A salt_$bryfile ${bryfile}_repro
	    ncks -A temp_$bryfile ${bryfile}_repro
	    ncks -A v2d_$bryfile ${bryfile}_repro
	    ncks -A u2d_$bryfile ${bryfile}_repro
	    ncks -A u3d_$bryfile ${bryfile}_repro
	    ncks -A v3d_$bryfile ${bryfile}_repro

	    ncks -A static_croco_bry_SODA.nc ${bryfile}_repro
	    
	    ncks -O --mk_rec_dmn bry_time ${bryfile}_repro ${bryfile}_repro
	    
	    rm -Rf temp* salt* zeta* zeta* u2d* v2d* u3d* v3d* static*
	fi
    done
done
exit
#####
for yy in `seq 2000 2006` ; do
    for mm0 in `seq 1 12` ; do
	bryfile=croco_bry_SODA_Y${yy}M${mm0}.nc_repro
	if [ -f $bryfile ]; then
	    echo $bryfile
	    # put with 2 digit
	    mm=`printf "%02d" $mm0` ;  echo $mm
	    bryfile2=croco_bry_SODA_Y${yy}M${mm}.nc_repro
	    mv $bryfile $bryfile2
	fi
    done
done

listf=`ls -1 croco_bry_SODA_Y*.nc_repro`
