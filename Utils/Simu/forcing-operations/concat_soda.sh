#! /bin/bash

for var in temp salt u v zeta ; do
    echo '----------------'
    echo 'Process var '$var
    echo '----------------'
    ncrcat  ${var}_roms_clm_Soda2_Y2000M[123456789].nc ${var}_roms_clm_Soda2_Y2000M1[012].nc ${var}_roms_clm_Soda2_AllY2000.nc
    ncra   ${var}_roms_clm_Soda2_AllY2000.nc ${var}_roms_clm_Soda2_MeanY2000.nc
  
    ncrcat  ${var}_roms_clm_Soda2_Y2000M[123456789].nc.1 ${var}_roms_clm_Soda2_Y2000M1[012].nc.1 ${var}_roms_clm_Soda2_AllY2000.nc.1
    ncra   ${var}_roms_clm_Soda2_AllY2000.nc.1 ${var}_roms_clm_Soda2_MeanY2000.nc.1

    rm -f ${var}_roms_clm*
done

ncks -A --fix_rec_dmn zeta_time zeta_roms_clm_Soda2_MeanY2000.nc u_roms_clm_Soda2_MeanY2000.nc
ncks -A --fix_rec_dmn tclm_time temp_roms_clm_Soda2_MeanY2000.nc u_roms_clm_Soda2_MeanY2000.nc
ncks -A --fix_rec_dmn sclm_time salt_roms_clm_Soda2_MeanY2000.nc u_roms_clm_Soda2_MeanY2000.nc
ncks -A --fix_rec_dmn vclm_time v_roms_clm_Soda2_MeanY2000.nc u_roms_clm_Soda2_MeanY2000.nc
cp u_roms_clm_Soda2_MeanY2000.nc concat_roms_clm_Soda2_MeanY2000.nc

ncks -A --fix_rec_dmn zeta_time zeta_roms_clm_Soda2_MeanY2000.nc.1 u_roms_clm_Soda2_MeanY2000.nc.1
ncks -A --fix_rec_dmn tclm_time temp_roms_clm_Soda2_MeanY2000.nc.1 u_roms_clm_Soda2_MeanY2000.nc.1
ncks -A --fix_rec_dmn sclm_time salt_roms_clm_Soda2_MeanY2000.nc.1 u_roms_clm_Soda2_MeanY2000.nc.1
ncks -A --fix_rec_dmn vclm_time v_roms_clm_Soda2_MeanY2000.nc.1 u_roms_clm_Soda2_MeanY2000.nc.1
cp u_roms_clm_Soda2_MeanY2000.nc.1 concat_roms_clm_Soda2_MeanY2000.nc.1


ncks -v h roms_grd.nc -o h_roms_grd.nc ;  ncks -A h_roms_grd.nc concat_roms_clm_Soda2_MeanY2000.nc
ncks -v h roms_grd.nc.1 -o h_roms_grd.nc.1 ; ncks -A h_roms_grd.nc.1 concat_roms_clm_Soda2_MeanY2000.nc.1
