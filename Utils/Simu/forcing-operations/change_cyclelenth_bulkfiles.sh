#! /bin/bash

md=1
mf=12
typefile='blk_CFSR'

month=${md}
while [ ${month} -le ${mf} ]
do

    if  [ ${month} -le 9 ]; then
	month2=0${month}
    else
	month2=${month}
    fi

    echo '======================='
    echo 'Process month '${month}
    echo '----> Process cycle_length changes'
    echo '      in file :  roms_'${typefile}'_Y2001M'${month}'.nc'
    ncatted -O -a cycle_length,bulk_time,o,d,365. roms_${typefile}_Y2001M${month}.nc
    echo '----done'
    month=`expr $month + 1`
done #boucle sur les mois

