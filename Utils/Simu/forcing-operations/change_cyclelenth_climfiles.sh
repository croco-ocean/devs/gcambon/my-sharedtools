#! /bin/bash

md=1
mf=1
typefile='clm_Soda2'
INPUTDIR='.'
month=${md}
while [ ${month} -le ${mf} ]
do
    if  [ ${month} -le 9 ]; then
	month2=0${month}
    else
	month2=${month}
    fi
    
    echo '======================='
    echo 'Process month '${month}
    echo '----> Process cycle_length changes'
    echo '      in file :  roms_'${typefile}'_Y2001M'${month}'.nc'
    ncatted -O -a cycle_length,tclm_time,o,d,365. ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,sclm_time,o,d,365. ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,temp_time,o,d,365. ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,salt_time,o,d,365. ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,uclm_time,o,d,365. ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,vclm_time,o,d,365. ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,v2d_time,o,d,365.  ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,v3d_time,o,d,365.  ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,zeta_time,o,d,365. ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    ncatted -O -a cycle_length,ssh_time,o,d,365.  ${INPUTDIR}/roms_${typefile}_Y2001M${month}.nc roms_${typefile}_Y2001M${month}.nc
    echo '      -- done. '
    month=`expr $month + 1`
done #boucle sur les mois

