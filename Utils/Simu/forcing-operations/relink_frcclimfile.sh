#! /bin/bash

yd=2000
md_yd=1
yf=2011
mf_yf=12

typefile='frc_CFSR_climato'
#endfile=' '
endfile='.1'


year=${yd}
while [ ${year} -le ${yf} ]
do
  if [ ${yd} == ${yf} ]; then
    md=${md_yd}
    mf=${mf_yf}
  elif [ ${year} == ${yd} ]; then
    md=${md_yd}
    mf=12
  elif [ ${year} == ${yf} ]; then
    md=1
    mf=${mf_yf}
  else
    md=1
    mf=12
  fi
  month=${md}
  while [ ${month} -le ${mf} ]
  do

   if  [ ${month} -le 9 ]; then
       month2=0${month}
   else
       month2=${month}
   fi
   echo '======================='
   echo 'Process month '${month}
   echo '----> Process linking'
    ln -sf roms_frc_climfromBengR4_test.nc${endfile} roms_${typefile}_Y${year}M${month}.nc${endfile}
   # ln -sf roms_frc.nc${endfile} roms_${typefile}_Y${year}M${month}.nc${endfile}
    month=`expr $month + 1`
 
   done #boucle sur les mois
   echo '-------'
   echo 'Process year '${year}

   year=`expr $year + 1`
done  # boucle sur les annees

