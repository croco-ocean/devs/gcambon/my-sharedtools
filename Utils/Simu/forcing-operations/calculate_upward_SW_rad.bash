#!/bin/bash
#==============================================================
# July 2016 
#  ---
# In this scripts we do create the outcoming short wave file.
# We compute drowned_upradsw_DFS5.2_yxxxx from the original 
# drowned_radsw_DFS5.2_yxxx.
# We do that because we need in ROMS the incoming (drawned_radsw)
# AND the outcoming one drowned_upradsw. 
# We get it multiplying by a constant albedo
#==============================================================


dirpath='/net/leo/local/tmp/1/gcambon/COMMON_DATA/DFS5.2_RD_processed'
cd ${dirpath}

#calculate upward short wave radiation
years=`seq 1999 2000`
#years=`seq 1999 2000`


for yy in $years ; do
 echo '   ===> Process year. '$yy

outputfile="drowned_upradsw_DFS5.2_y${yy}.nc"
echo ${outputfile}
  ncap2 -s 'radsw=radsw*0.06f'  drowned_radsw_DFS5.2_y${yy}.nc  -O drowned_upradsw_DFS5.2_y${yy}.nc   
  ncrename -v radsw,upradsw  $outputfile -O  $outputfile
done



