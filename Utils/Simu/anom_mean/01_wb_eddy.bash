#/bin/bash

mkdir LAB
g=9.81
rho0=1025

# Extraction des variables
for month in {1..12} ; do
    #    for i in `seq -w 1 12 12` ; do
    for i in `seq -w $month 12 84 ` ; do
    ##for i in `seq -w 2 1 2 ` ; do
	echo $i
	#file="roms_avg_M_00${i}.nc"
	file="roms_avg_M_000${i}.nc"
	echo $file
	ncks -v lat_rho,lon_rho,mask_rho,w,rho $file -o LAB/wb_${file}
	
	cd LAB
	# Calcul de b  b=-g/rho0*rho
	#ncap2 -O -s "b = - ${g} / ${rho0} * ( rho )" wb_${file} wb_${file}
	
	# Add the variables  b and w*b to rho
	ncap2 -O -s "rho = - ${g} / ${rho0} * ( rho )" wb_${file} wb_${file}
	echo "First ncap2 done"
	
	#ncrename -O -v rho,b  wb_${file} wb_${file}
	#echo "ncreanme rho en b"
	ncap2 -O -s "wb =  w * b" wb_${file} wb_${file}
	echo "second ncap2"

 	ncrename -v rho,b wb_${file} wb_${file}	
	cd -
    done
done # sur les months de 1 a 12

#cd LAB
# Compute the mean associated to each month in order to have a seasonal cycle
#for i in {1..12} ; do
#    ncra -n 7,4,12 wb_roms_avg_M_00${i}.nc wb_roms_avg_mean_M${i}_2005_2011.nc
#    # example web ncra -n 5,2,1 85.nc 8589.nc
#done
#cd -
