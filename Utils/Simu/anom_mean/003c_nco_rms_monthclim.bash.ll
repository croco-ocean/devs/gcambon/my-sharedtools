#!/bin/bash
#
# Nom du travail LoadLeveler
# @ job_name= 003c_nco_diag
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ wall_clock_limit = 20:00:00
# @ requirements = (Feature == "prepost")
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
#
cd $LOADL_STEP_INITDIR

#===================
source configure_anom
#===================

#-----------------------------------------------------------------------
cd $AVGDIR

# => relink uvz anomsquare roms files from Year Month to number
#
cp ../rename2number_anomsquare.bash .
./rename2number_anomsquare.bash
#==

month=0
im=1
while [ ${month} -lt 12 ] ; do 
    month=$((month+1))
    echo '======================================'
    echo 'Process the clim for the month: '$month
    
    if [ ${im} -le 9 ] ; then
        mm='000'${im}
    elif [ ${im} -le 99 ] ; then
        mm='00'${im}
    elif [ ${im} -le 999 ] ; then
	mm='0'${im}
    fi
    

    echo 'mm='$mm
    echo 'month='$month
    
    infile=uvz_roms_avg_monthavg_anomsquare_M_${mm}.nc${endfile}
    echo 'infile='$infile    
    outfile=uvz_roms_avg_monthavg_anomsquare_clim_M_${month}.nc${endfile}
    echo 'clim outfile='$outfile

    ncra -n $nyear,$ndigit,12 $infile -o $outfile
    im=$((im+1))
done

ncrcat -O uvz_roms_avg_monthavg_anomsquare_clim_M_[123456789].nc${endfile} uvz_roms_avg_monthavg_anomsquare_clim_M_1[012].nc${endfile} -o uvz_roms_avg_monthavg_anomsquare_seasclim.nc${endfile}
mv uvz_roms_avg_monthavg_anomsquare_seasclim.nc${endfile} uvz_roms_avg_monthavg_anomsquare_seasclim_${YEARMIN}'_'${YEARMAX}.nc${endfile}
cp uvz_roms_avg_monthavg_anomsquare_seasclim_${YEARMIN}'_'${YEARMAX}.nc${endfile} ../
#
#Compute the SSH RMS 
#
filein=uvz_roms_avg_monthavg_anomsquare_seasclim_${YEARMIN}'_'${YEARMAX}'.nc'${endfile}
ncks -O -v lat_rho,lon_rho,zeta $filein ZETA_$filein
ncap2 -O -s 'zeta_rms=sqrt(zeta)' ZETA_${filein} ZETA_${filein} 
mv  ZETA_${filein} ../

echo ' '
echo '===================='
echo '=> PROCESSING DONE !'
echo '===================='

cd - 


