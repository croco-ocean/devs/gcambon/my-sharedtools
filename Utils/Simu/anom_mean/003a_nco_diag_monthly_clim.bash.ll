#!/bin/bash
#
# Nom du travail LoadLeveler
# @ job_name= 003a_nco
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ wall_clock_limit = 20:00:00
# @ requirements = (Feature == "prepost")
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
#

cd $LOADL_STEP_INITDIR

#=============================
source configure_anom
#=============================
set -x

#
./rename2number.bash ; cd -

# => Compute the climatology
#
month=0
im=1
while [ ${month} -lt 12 ] ; do 
    month=$((month+1))
    echo '======================================'
    echo 'Process the clim for the month: '$month
    
    if [ ${im} -le 9 ] ; then
        mm='000'${im}
    elif [ ${im} -le 99 ] ; then
        mm='00'${im}
    elif [ ${im} -le 999 ] ; then
	mm='0'${im}
    fi
    echo 'im='$im
    echo 'mm='$mm
    
    #=====================================================================
    infile=$indir/${prefix}roms_avg_monthavg_M_${mm}.nc${endfile}
    outfile=$indir/${prefix}roms_avg_monthavg_clim_M_${month}.nc${endfile}
    #=====================================================================
    
    ncra -n $nyear,$ndigit,12 $infile -o $outfile
    
    im=$((im+1))
done


# => ncrcat to create a single seasonal file.
#
cd $indir

ncrcat ${prefix}roms_avg_monthavg_clim_M_[123456789].nc${endfile} ${prefix}roms_avg_monthavg_clim_M_1[012].nc${endfile} -o ${prefix}roms_avg_monthavg_seasclim.nc${endfile}

cp -Rf ${prefix}roms_avg_monthavg_seasclim.nc${endfile} ../${prefix}roms_avg_monthavg_seasclim_${YEARMIN}'_'${YEARMAX}.nc${endfile}

cd -

# => Remove the singleton dimension
#
# This is needed here to remove time=1 singleton dimension
# for after do compute the anomalie

for month in `seq 1 12` ;  do
    outfile=$indir/${prefix}roms_avg_monthavg_clim_M_${month}.nc${endfile}
    ncwa -O -a time $outfile $outfile
done

