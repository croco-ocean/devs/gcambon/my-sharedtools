#!/bin/bash
#
# Nom du travail LoadLeveler
# @ job_name= 000_nco_diag
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ wall_clock_limit = 20:00:00
# @ requirements = (Feature == "prepost")
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
#
######################################################################
cd $LOADL_STEP_INITDIR
#=================================
source configure_anom
#=================================


echo "from $YEARMIN to $YEARMAX"
cd $ROMSDIR
YEAR=$YEARMIN
im=0
while [ $YEAR -le $YEARMAX ]
do
    MONTH=0
    while [ $MONTH -lt 12 ]
    do
	MONTH=$((MONTH+1))
	im=$((im+1))
	echo '========================================='
        echo "MONTHLY AVG : roms_avg_Y${YEAR}M${MONTH}.nc${endfile}"
	echo '-->' $YEAR $MONTH $im
	echo '  '
	#--
	MONTH2=$MONTH
        romsfile=${ROMSDIR}/roms_avg_Y${YEAR}M${MONTH2}.nc${endfile}
        outfile=${indir}/${prefix}roms_avg_Y${YEAR}M${MONTH2}.nc${endfile}
        echo $romsfile
        echo $outfile
	
	if [ ${extract_flag} == 1 ]; then
	    echo $VARTOEXTRACT
	    echo  " ==> ncks -v $VARTOEXTRACT $romsfile $outfile "
            ncks -v $VARTOEXTRACT $romsfile -o $outfile
	else
	    cp -Rf $romsfile $outfile
	fi
	
    done
    YEAR=$(( YEAR+1 ))
done
