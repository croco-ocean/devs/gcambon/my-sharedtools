#!/bin/bash
# Nom du travail LoadLeveler
# @ job_name= ANOM_nco
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 20:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
######################################################################
cd $LOADL_STEP_INITDIR

#-------------
#==>Needed only if not computed seasonal mean before using Launch_Lean
./000_nco_diag_extract.bash

./001_nco_diag_monthlyavg.bash
./003a_nco_diag_monthly_clim.bash
#--
./003b_nco_anomsquare.bash
./003c_nco_rms_monthclim.bash
#==============
