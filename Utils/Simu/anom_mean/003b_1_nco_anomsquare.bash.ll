#!/bin/bash
#
# Nom du travail LoadLeveler
# @ job_name= 003b_nco_diag
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ wall_clock_limit = 20:00:00
# @ requirements = (Feature == "prepost")
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
#

# Split into 2 steps the anomalies computation
# HERE FIRST PART

cd $LOADL_STEP_INITDIR

#===================
source configure_anom
#===================

YEARMAX=$(($YEARMIN + $nyear / 2))

echo "from $YEARMIN to $YEARMAX"
YEAR=$YEARMIN
im=0
while [ $YEAR -le $YEARMAX ]
do
    MONTH=0
    while [ $MONTH -lt 12 ]
    do
	MONTH=$((MONTH+1))
	im=$((im+1))
	
	echo ' '
	echo '============================================='
	echo $YEAR $MONTH $im

	
        if [ "${im}" -lt  "10" ]
      then
            nfile="000${im}"
        elif [ "${im}" -lt  "100" -a "${im}" -ge  "10" ]
        then
            nfile="00${im}"
        elif [ "${im}" -lt  "1000" -a "${im}" -ge  "100" ]
        then
            nfile="0${im}"
        elif [ "${im}" -lt  "10000" -a "${im}" -ge  "1000" ]
        then
            nfile="${im}"
        fi
	
	# INPUT FILES
	romsfile=$indir/${prefix}roms_avg_Y${YEAR}M${MONTH}.nc
	allmonthmean_romsfile=$indir/${prefix}roms_avg_monthavg_clim_M_${MONTH}.nc
	
        echo 'romsfile='$romsfile
	echo 'allmonthmean_romsfile='$allmonthmean_romsfile
	
	# OUTPUT FILES
        allmonth_anomfiletmp=$indir/${prefix}roms_avg_anom_tmp.nc
        allmonth_anomsquarefile=$indir/${prefix}roms_avg_anomsquare_Y${YEAR}M${MONTH}.nc
        meanmonth_allmonth_anomsquarefile=$indir/${prefix}roms_avg_monthavg_anomsquare_Y${YEAR}M${MONTH}.nc
	
        echo 'allmonthmean_romsfile='$allmonthmean_romsfile
	echo 'allmonth_anomfiletmp='$allmonth_anomfiletmp
	echo 'allmonth_anomsquarefile='$allmonth_anomsquarefile
	echo 'meanmonth_allmonth_anomsquarefile='$meanmonth_allmonth_anomsquarefile
	
        ncbo -O --op_typ='-' $romsfile $allmonthmean_romsfile $allmonth_anomfiletmp
        ncbo -O --op_typ='*' $allmonth_anomfiletmp $allmonth_anomfiletmp $allmonth_anomsquarefile
        ncra -O -F  $allmonth_anomsquarefile -o  $meanmonth_allmonth_anomsquarefile	    

    done
    YEAR=$((YEAR+1))
done
