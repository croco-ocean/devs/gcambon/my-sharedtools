#!/bin/bash
# Nom du travail LoadLeveler
# @ job_name= 1MEAN_2CLIM_fullvar
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(step_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(step_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 20:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
######################################################################
#=========== Step 1 directives ===========
#            MONTHAVG
#=========================================
# Type de travail
# @ step_name = one_nco_fullvar
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

#=========== Step 3a directives ===========
#            MONTHAVG CLIM
#=========================================
# Type de travail
# @ step_name = threea_nco_fullvar
# @ dependency = (one_nco_fullvar == 0)
#   (executed only if previous step completed without error)
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

#####################################################################
case ${LOADL_STEP_NAME} in
#============ Step 1 commands ============
one_nco_fullvar )
cd $LOADL_STEP_INITDIR
./001_nco_diag_monthlyavg_fullvar.bash.ll
;;

#============ Step 2 commands ============
threea_nco_fullvar )
cd $LOADL_STEP_INITDIR
./003a_nco_diag_monthly_clim_fullvar.bash.ll
;;

esac
