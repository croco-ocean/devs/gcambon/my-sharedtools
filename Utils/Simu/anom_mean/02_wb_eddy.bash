#/bin/bash
#ncra -n 2,4,12 wb_roms_avg_M_0001.nc wb_roms_avg_mean_month_1.nc

# Compute the mean associated to each month in order to have a seasonal cycle
for i in `seq -w 1 12` ; do
    echo $i
    filein="wb_roms_avg_M_00${i}.nc"
    fileout="wb_roms_avg_mean_month_${i}_2005_2011.nc"
    echo "=> filein is" $filein
    echo "=> fileout is" $fileout

    echo "ncra ..."
    ncra -n 7,4,12 $filein $fileout
    # ==> example web ncra -n 5,2,1 85.nc 8589.nc
	
     echo "ncap2 for wpbp ..."
     ncap2 -O -s 'wpbp = wb - w * b' $fileout $fileout
done
