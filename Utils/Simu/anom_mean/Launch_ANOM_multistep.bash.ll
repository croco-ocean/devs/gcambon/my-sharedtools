#!/bin/bash
#=========== Global directives ===========
# Nom du travail LoadLeveler
# @ job_name= ANOM_RUN
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(step_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(step_name).$(jobid)
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always


#=========== Step 1 directives ===========
#            EXTRACTION
#=========================================
# Type de travail
# @ step_name = zero_nco
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

#=========== Step 1 directives ===========
#            MONTHAVG
#=========================================
# Type de travail
# @ step_name = one_nco
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

#=========== Step 3a directives ===========
#            MONTHAVG CLIM
#=========================================
# Type de travail
# @ step_name = threea_nco
# @ dependency = (one_nco == 0)
#   (executed only if previous step completed without error)
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

#=========== Step 3b_1 directives ===========
#            MONTHAVG ANOMSQUARE
#=========================================
# Type de travail
# @ step_name = threeb_1_nco
# @ dependency = (threea_nco == 0)
#   (executed only if previous step completed without error)
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

#=========== Step 3b_2 directives ===========
#            MONTHAVG ANOMSQUARE
#=========================================
# Type de travail
# @ step_name = threeb_2_nco
# @ dependency = (threeb_1_nco == 0)
#   (executed only if previous step completed without error)
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

#=========== Step 3c directives ===========
#            MONTHAVG ANOMSQUARE CLIM
#=========================================
# Type de travail
# @ step_name = threec_nco
# @ dependency = (threeb_2_nco == 0)
#   (executed only if previous step completed without error)
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# @ wall_clock_limit = 20:00:00
# @ as_limit = 20.0gb
# @ queue

case ${LOADL_STEP_NAME} in
#============ Step 1 commands ============
zero_nco )
cd $LOADL_STEP_INITDIR
./
;;

case ${LOADL_STEP_NAME} in
#============ Step 1 commands ============
one_nco )
cd $LOADL_STEP_INITDIR
./001_nco_diag_monthlyavg.bash.ll
;;
    
#============ Step 2 commands ============
threea_nco )
cd $LOADL_STEP_INITDIR
./003a_nco_diag_monthly_clim.bash.ll
;;
    
#============ Step 3 commands ============
threeb_1_nco )
cd $LOADL_STEP_INITDIR
./003b_1_nco_anomsquare.bash.ll
;;

#============ Step 3 commands ============
threeb_2_nco )
cd $LOADL_STEP_INITDIR
./003b_2_nco_anomsquare.bash.ll
;;

#============ Step 4 commands ============
threec_nco )
cd $LOADL_STEP_INITDIR
./003c_nco_rms_monthclim.bash.ll
;;

esac
