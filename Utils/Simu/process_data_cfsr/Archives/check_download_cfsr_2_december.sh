#!/bin/bash

indvar=-1
#var in     heatflux U-wind V-wind precip_rate specific temp_2m
#minimum size Ok for december month in MkO !!
sizevarlim=(90000  28000  28000  10000  36000 26000 )

rm -f dec_all_*



for var in heatflux U-wind V-wind precip_rate specific temp_2m ; do 
#for var in V-wind ; do 
echo 'Check var '$var
indvar=$(( indvar + 1 ))
echo 'indvar='${indvar}
rm -f dec_${var}_out*

cd ${var}
du -ks flxf06.gdas.????1226-????1231.grb2.nc.gz >> ../dec_${var}_out

#Parse the out file ...
echo 'Check var '$var >> ../dec_all_out2
echo '===========' >> ../dec_all_out2
while read line  
do
#detect line number with 3 first digit < 50
   bb=`echo -e $line`
   #echo ${bb}
   aa0=`echo $line | cut -d' ' -f1`
   aa=$(echo $aa0 | tr -d ' ')
   #echo ${aa0}
   if [ ${aa0} -lt ${sizevarlim[${indvar}]} ]; then
   #if [ ${aa0} -lt 10 ]; then
   echo ${bb} >> ../dec_all_out2
   fi 
   
done < ../dec_${var}_out

cd -
done
