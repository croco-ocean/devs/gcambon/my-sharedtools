#!/bin/bash

createdir_fflag=1
gunzip_fflag=1

#yeardeb=2011
#yearend=2012

yeardeb=1999
yearend=2010

# Gildas
#indlatmin=260
#indlatmax=490

# Gaelle (+ gildas)
#indlatmin=240
#indlatmax=490

# Steven + Gaelle + Gildas BENGSAFE R15 (Final V2)
# All longitude and -65.09 <->15.1431 in latitude
indlatmin=240
indlatmax=497

# Gildas BENGSAFE R15 (CFSRv2 Final V2 resolution plus grande)
# All longitude and -65.09 <->15.1431 in latitude => 2011 et 2012
indlatmin=365
indlatmax=760

# Formation 2016  30 N - 55 N
#indlatmin=55
#indlatmax=205

# For Jonathan North Atlantic -5°S 75°N
#indlatmin=30
#indlatmax=315

# For Jonathan Megatl -65°S 75°N 1998-2011
indlatmin=30
indlatmax=497


#for dirin in V-wind ; do 
for dirin in heatflux U-wind V-wind precip_rate specific temp_2m ; do 
    echo 'PROCESS VAR in '$dirin
    cd $dirin 

#Create the year dir
    if [ ${createdir_fflag} == 1 ] ; then 
	for yy in `seq ${yeardeb} ${yearend}` ; do
	    dir=y${yy} ; echo 'Process '${dir}
	    rm -Rf Lab/ ; rm -Rf ${dir}

	    if [ ! -d "${dir}" ] ; then
		echo 'Create dir. 'y$yy
		mkdir ${dir}
	    fi
	done 
    fi
    
    if [ ${gunzip_fflag} == 1 ] ; then 	
	for yy in `seq ${yeardeb} ${yearend}` ; do 
            echo 'Process year '$yy
            echo ''
	    cp *.${yy}*.nc.gz y${yy}/ 
	    cd y${yy}/
	    listf=`ls -1 *.${yy}*.nc.gz`
	    for ffile in $listf ; do 
		echo 'Process file '$ffile
		ffile2=`echo $ffile | cut -d. -f1-4`  
		gunzip ${ffile2}.nc.gz
		ncks -F -d lat,${indlatmin},${indlatmax} ${ffile2}.nc -o extr_${ffile2}.nc  
		rm ${ffile2}.nc  
	    done
	    echo '===> Extraction done ....'
	    cd ../
	done
    fi
    
    cd ../
done
