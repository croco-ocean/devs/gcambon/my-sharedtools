#!/bin/bash

for dir in heatflux U-wind V-wind precip_rate specific temp_2m ; do 
    echo 'Process dir: '$dir
    cd $dir
    rm -Rf cdas1.????????-????????.aa.bb.grb2.nc.gz ; 
    
    for i in `ls -1 FLX_FILES_2011_V1-V2/*`; do 
	echo $i 
	aa=`echo $i | cut -d. -f3-3`; 
	echo $aa 
	bb='cdas1.'$aa'.aa.grb2.nc.gz' 
	echo $bb    
	ln -sf $i $bb 
    done
    
    cd -
done 
