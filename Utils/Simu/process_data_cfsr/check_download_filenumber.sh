#!/bin/bash

rm -f recap* linenumber*

for var in heatflux U-wind V-wind precip_rate specific temp_2m temp_surf ; do 
#for var in V-wind ; do 
    echo 'Check var '$var
    rm -f ${var}_out*
    
    cd ${var}
    du -ms *nc.gz >> ../linenumber_${var}_out
    linenumber=`wc -l ../linenumber_${var}_out | cut -c 1-4`
    echo $linenumber
    echo '===============' >> ../recap_linenumber_out
    echo 'CHECK VAR ' $var >> ../recap_linenumber_out
    echo $linenumber >> ../recap_linenumber_out

    ls -1 *nc.gz >> ../02_linenumber_${var}_out

    cd -
done


