#!/bin/bash

# mois de 31
#set -x
#indxp = index start
year=1990
ndaysfilt=3

for i in `seq 1 12` ; do
    echo 'Process Month '$i
    filein=roms_avg_Y${year}M${i}.nc
    if (( $i == 1 || $i == 3 ||  $i == 5 ||   $i == 7 ||  $i == 8 ||  $i == 10 ||  $i == 12 )) ; then
	indxb=1
	echo "Process file " ${filein}
    elif (( $i == 2 || $i == 4 ||  $i == 6 ||   $i == 9  ||  $i == 11 )) ; then
	indxb=0
	echo "Process file " ${filein}
    fi

    cdo timselmean,${ndaysfilt},${indxb} ${filein} "${ndaysfilt}d_${filein}"
    
    ncrename -d x,xi_rho -d y,eta_rho "${ndaysfilt}d_${filein}"
    
    ncks -A grid_roms_avg_r3km.nc "${ndaysfilt}d_${filein}"

    ncks -F -d xi_rho,5,400 -d eta_rho,115,475 "${ndaysfilt}d_${filein}" "${ndaysfilt}d_ext_${filein}" 
done
