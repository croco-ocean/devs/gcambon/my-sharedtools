#!/bin/bash

# Process the 5 days average on the R3KM daily outputs

# process the averaging
varlist="pm,pn,zeta,scrum_time,lat_rho,lon_rho,mask_rho,f"
year=1990

# mois de 31
for i in 1 3 5 7 8 10 12 ; do
    echo 'Process Month '$i
    filein=roms_avg_Y${year}M${i}.nc
    #ncks -F -v $varlist $filein ssh_${filein}
    cdo timselmean,5,1 ${filein} 5d_${filein}
    ncrename -d x,xi_rho -d y,eta_rho 5d_${filein}
done

# mois de 30
for i in 4 6 9 11 ; do
    echo 'Process Month '$i
    filein=roms_avg_Y${year}M${i}.nc
#    ncks -F -v $varlist $filein ssh_${filein}
    cdo timselmean,1 ${filein} 5d_${filein}
    ncrename  -d x,xi_rho -d y,eta_rho 5d_${filein}
    
done

# mois de fevrier
for i in 2 ; do
    echo 'Process Month '$i
    filein=roms_avg_Y${year}M${i}.nc
#    ncks -F -v $varlist $filein ssh_${filein}
    cdo timselmean,1,3 ${filein} 5d_${filein}
    ncrename -d x,xi_rho -d y,eta_rho 5d_${filein}
done

##rm -Rf ssh_roms_avg_*.nc
