#!/bin/bash

set -x

listvar='Downward_Short-Wave_Rad_Flux_surface Precipitation_rate Specific_humidity Temperature_height_above_ground U-component_of_wind Downward_Long-Wave_Rad_Flux Upward_Long-Wave_Rad_Flux_surface  Upward_Short-Wave_Rad_Flux_surface  V-component_of_wind'

#years=`seq 2004 `
#months=`seq 1 12`

timeorig2005M1=1827
mm=12
copy_flag=0
changedate_flag=0
rename_flag=1

if [[ $copy_flag -eq 1 ]]   ; then
    for var in $listvar ; do
	echo $var
	inputfile="${var}_Y2005M${mm}.nc"
	outputfile="${var}_Y2004M${mm}.nc"
	echo "=> Process " ${outputfile}
	mv $inputfile $outputfile
	#ncap2 -O -F -s "time=time - time(1)" $outputfile $outputfile
    done
fi

if [[ $changedate_flag -eq 1 ]]   ; then
    for file in `ls -1 *2005*.nc` ; do
	echo "Process file "$file
	ncap2 -O -F -s "time=time - ${timeorig2005M1}" $file $file
	#ncdump -h ${var}_${ffile} | grep "${var}(time" > extract_time_dim_tmp
    done
    for file in `ls -1 *Y2004M12.nc` ; do
	echo "Process file "$file
	ncap2 -O -F -s "time=time - time(1) - 31 ;  " $file $file
	#ncdump -h ${var}_${ffile} | grep "${var}(time" > extract_time_dim_tmp
    done
    
fi


for file in `ls -1 *2005*.nc` ; do
    echo "Rename file "$file
    datein=`echo $file | cut -dY -f1,1` 
    echo $datein
done

if [[ $rename_flag -eq 1 ]]   ; then
    for var in $listvar ; do
	for mm in `seq 1 2` ; do
	    inputfile="${var}_Y2005M${mm}.nc"
	    outputfile="${var}_Y2000M${mm}.nc"
	    echo "=> Process " ${outputfile}
	    mv $inputfile $outputfile
	done
	
	inputfile="${var}_Y2004M12.nc"
	outputfile="${var}_Y1999M12.nc"
	echo "=> Process " ${outputfile}
	mv $inputfile $outputfile
	
    done
fi
