#!/bin/sh

#Juillet 
#------
echo "==============="
echo "  "
echo "Process july"
ncks -v RAIN,ACSWUPB,ACSWDNB,ACLWUPB,ACLWDNB,XLAT,XLONG,LANDMASK,Time wrfout2_d01_Y2008M7.nc -o wrfout2_d01_Y2008M7_extr.nc

cdo seltimestep,1/256 wrfout2_d01_Y2008M7_extr.nc wrfout2_d01_Y2008M7_extr_OK.nc
rm -f wrfout2_d01_Y2008M7_extr.nc ; mv wrfout2_d01_Y2008M7_extr_OK.nc wrfout2_d01_Y2008M7_extr.nc

cdo sub -seltimestep,3/249 wrfout2_d01_Y2008M7_extr.nc -seltimestep,1/247 wrfout2_d01_Y2008M7_extr.nc wrfout2_d01_Y2008M7_extr_deacc_missfirst.nc

# case first month #
# add the first time step non deaccumule, 0h00 ie it= 9 - 24 h  #
# 2(3h00) 3(06h00)  4(09h) 5(12h) 6(15h) 7(18h) 8(21h) 9(0h00) 10(3h) 
ncks -F -d time,8 wrfout2_d01_Y2008M7_extr_deacc_missfirst.nc -o first_00h.nc
ncap2 -O -s "time=time - 1440. " first_00h.nc first_00h.nc
ncrcat first_00h.nc wrfout2_d01_Y2008M7_extr_deacc_missfirst.nc -o wrfout2_d01_Y2008M7_extr_deacc.nc
# end  #
ncap2 -s "time=time-180" wrfout2_d01_Y2008M7_extr_deacc.nc -o wrfout2_d01_Y2008M7_extr_deacc_2.nc

rm -f wrfout2_d01_Y2008M7_extr.nc
rm -f wrfout2_d01_Y2008M7_extr_deacc.nc
mv wrfout2_d01_Y2008M7_extr_deacc_2.nc wrfout2_d01_deacc_extr_Y2008M7.nc
#-----
cdo seltimestep,1/248 wrfout2_d01_Y2008M7.nc wrfout2_d01_extr_Y2008M7.nc



#Aout
#----
echo "==============="
echo "  "
echo "Process august"
ncks -v RAIN,ACSWUPB,ACSWDNB,ACLWUPB,ACLWDNB,XLAT,XLONG,LANDMASK,Time wrfout2_d01_Y2008M8.nc -o wrfout2_d01_Y2008M8_extr.nc
cdo sub -seltimestep,10/257 wrfout2_d01_Y2008M8_extr.nc -seltimestep,8/255 wrfout2_d01_Y2008M8_extr.nc wrfout2_d01_Y2008M8_extr_deacc.nc
ncap2 -s "time=time-180" wrfout2_d01_Y2008M8_extr_deacc.nc -o wrfout2_d01_Y2008M8_extr_deacc_2.nc

rm -f wrfout2_d01_Y2008M8_extr.nc
rm -f wrfout2_d01_Y2008M8_extr_deacc.nc
mv wrfout2_d01_Y2008M8_extr_deacc_2.nc wrfout2_d01_deacc_extr_Y2008M8.nc
#-----
cdo seltimestep,9/256 wrfout2_d01_Y2008M8.nc wrfout2_d01_extr_Y2008M8.nc

#Sept
#---
echo "==============="
echo "  "
echo "Process sept"
ncks -v RAIN,ACSWUPB,ACSWDNB,ACLWUPB,ACLWDNB,XLAT,XLONG,LANDMASK,Time wrfout2_d01_Y2008M9.nc -o wrfout2_d01_Y2008M9_extr.nc
cdo sub -seltimestep,10/249 wrfout2_d01_Y2008M9_extr.nc -seltimestep,8/247 wrfout2_d01_Y2008M9_extr.nc wrfout2_d01_Y2008M9_extr_deacc.nc
ncap2 -s "time=time-180" wrfout2_d01_Y2008M9_extr_deacc.nc -o wrfout2_d01_Y2008M9_extr_deacc_2.nc

rm -f wrfout2_d01_Y2008M9_extr.nc
rm -f wrfout2_d01_Y2008M9_extr_deacc.nc
mv wrfout2_d01_Y2008M9_extr_deacc_2.nc wrfout2_d01_deacc_extr_Y2008M9.nc
#-----
cdo seltimestep,9/248 wrfout2_d01_Y2008M9.nc wrfout2_d01_extr_Y2008M9.nc

#Oct
#----
echo "==============="
echo "  "
echo "Process oct"
ncks -v RAIN,ACSWUPB,ACSWDNB,ACLWUPB,ACLWDNB,XLAT,XLONG,LANDMASK,Time wrfout2_d01_Y2008M10.nc -o wrfout2_d01_Y2008M10_extr.nc
cdo sub -seltimestep,10/257 wrfout2_d01_Y2008M10_extr.nc -seltimestep,8/255 wrfout2_d01_Y2008M10_extr.nc wrfout2_d01_Y2008M10_extr_deacc.nc
ncap2 -s "time=time-180" wrfout2_d01_Y2008M10_extr_deacc.nc -o wrfout2_d01_Y2008M10_extr_deacc_2.nc

rm -f wrfout2_d01_Y2008M10_extr.nc
rm -f wrfout2_d01_Y2008M10_extr_deacc.nc
mv wrfout2_d01_Y2008M10_extr_deacc_2.nc wrfout2_d01_deacc_extr_Y2008M10.nc
#-----
cdo seltimestep,9/256 wrfout2_d01_Y2008M10.nc wrfout2_d01_extr_Y2008M10.nc

#Nov
#---
echo "==============="
echo "  "
echo "Process nov"
ncks -v RAIN,ACSWUPB,ACSWDNB,ACLWUPB,ACLWDNB,XLAT,XLONG,LANDMASK,Time wrfout2_d01_Y2008M11.nc -o wrfout2_d01_Y2008M11_extr.nc
cdo sub -seltimestep,10/249 wrfout2_d01_Y2008M11_extr.nc -seltimestep,8/247 wrfout2_d01_Y2008M11_extr.nc wrfout2_d01_Y2008M11_extr_deacc.nc
ncap2 -s "time=time-180" wrfout2_d01_Y2008M11_extr_deacc.nc -o wrfout2_d01_Y2008M11_extr_deacc_2.nc

rm -f wrfout2_d01_Y2008M11_extr.nc
rm -f wrfout2_d01_Y2008M11_extr_deacc.nc
mv wrfout2_d01_Y2008M11_extr_deacc_2.nc wrfout2_d01_deacc_extr_Y2008M11.nc
#-----
cdo seltimestep,9/248 wrfout2_d01_Y2008M11.nc wrfout2_d01_extr_Y2008M11.nc


#Dec
#----
echo "==============="
echo "  "
echo "Process dec"
ncks -v RAIN,ACSWUPB,ACSWDNB,ACLWUPB,ACLWDNB,XLAT,XLONG,LANDMASK,Time wrfout2_d01_Y2008M12.nc -o wrfout2_d01_Y2008M12_extr.nc
cdo sub -seltimestep,10/256 wrfout2_d01_Y2008M12_extr.nc -seltimestep,8/254 wrfout2_d01_Y2008M12_extr.nc wrfout2_d01_Y2008M12_extr_deacc_misslast.nc

cdo seltimestep,1/256 wrfout2_d01_Y2008M12_extr.nc wrfout2_d01_Y2008M12_extr_OK.nc
rm -f wrfout2_d01_Y2008M12_extr.nc ; mv wrfout2_d01_Y2008M12_extr_OK.nc wrfout2_d01_Y2008M12_extr.nc

# case first month #
# add last time step deaccumule, 21 h00 ie it 248 + 24h 
# it =   248(21h00) 249(0h00)  250(03h) 251(6h)  252(09h) 253(12h) 254(15h) 255(18) 256(21h) 
ncks -F -d time,247 wrfout2_d01_Y2008M12_extr_deacc_misslast.nc -o last_00h.nc
ncap2 -O -s "time=time + 1440. " last_00h.nc last_00h.nc
ncrcat wrfout2_d01_Y2008M12_extr_deacc_misslast.nc last_00h.nc -o wrfout2_d01_Y2008M12_extr_deacc.nc
# end
ncap2 -s "time=time-180" wrfout2_d01_Y2008M12_extr_deacc.nc -o wrfout2_d01_Y2008M12_extr_deacc_2.nc

rm -f wrfout2_d01_Y2008M12_extr.nc
rm -f wrfout2_d01_Y2008M12_extr_deacc.nc
mv wrfout2_d01_Y2008M12_extr_deacc_2.nc wrfout2_d01_extr_deacc_Y2008M12.nc
#-----
cdo seltimestep,9/256 wrfout2_d01_Y2008M12.nc wrfout2_d01_extr_Y2008M12.nc
#=====
rm -f first_00h.nc last_00h.nc wrfout2_d01_Y2008M12_extr_deacc_misslast.nc wrfout2_d01_Y2008M7_extr_deacc_missfirst.nc