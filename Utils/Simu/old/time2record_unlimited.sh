#! /bin/bash

# 0 - Compilation:
#############################
ls -1 *.nc > file_to_execute

# Pour creer le record 
for i in `cat file_to_execute` ;
do
echo $i

#set A = sed 's/UVonRHO/UVonRHOrecord/g' $i
#        echo "$i to $A"
ncecat -O $i $i  # Add degenerate record dimension named "record"

done

# Pour transformer la dimension fixe Time vers une dim record unlimited
for i in `cat file_to_execute` ;
do
echo $i
ncpdq -O -a sms_time,record $i  $i # Switch "record" and "time"
# Time= nom de ta dimension temporelle



done


# Pour mettre les variables qui dependait du Time fixe vers Time record !!
for i in `cat file_to_execute` ;
do
echo $i
#ncwa -O -a record  $path/$file_out  $path/$file_out # Average out
ncwa -O -a record  $i  $i # Average out
#degenerate "record"
done
