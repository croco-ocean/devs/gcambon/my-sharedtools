#! /bin/bash

yd=1992
md_yd=1
yf=2000
mf_yf=12

#for run in ScratchG6_FluxClim ScratchG6_7D_FluxClim ScratchG6_StERS_FluxClim ScratchG6_StNCEP_FluxClim ScratchG6_StQSCATANCEP_FluxClim ScratchG6_StQSCATCLM_FluxClim ; 
for run in ScratchG6_StQSCATANCEP_FluxClim; 
do echo 'Compute '$run
filename=roms_avg

var=zeta
filename2=zeta

year=${yd}
while [ ${year} -le ${yf} ]
do
  if [ ${yd} == ${yf} ]; then
    md=${md_yd}
    mf=${mf_yf}
  elif [ ${year} == ${yd} ]; then
    md=${md_yd}
    mf=12
  elif [ ${year} == ${yf} ]; then
    md=1
    mf=${mf_yf}
  else
    md=1
    mf=12
  fi

  month=${md}
  while [ ${month} -le ${mf} ]
  do

   if  [ ${month} -le 9 ]; then
       month2=0${month}
   else
       month2=${month}
   fi

ncrcat -v scrum_time,lat_rho,lon_rho,${var} ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc
#ncrcat -F -d s_rho,32,32 -v ${var} ${run}/${filename}_Y${year}M${month}.nc LAB/${filename2}_Y${year}M${month}.nc

    month=`expr $month + 1`
  done
  ncrcat -h LAB/${filename2}_Y${year}M[123456789].nc LAB/${filename2}_Y${year}M1[012].nc LAB/${filename2}_Y${year}.nc
  year=`expr $year + 1`
done

ncrcat -h LAB/${filename2}_Y1992.nc LAB/${filename2}_Y1993.nc LAB/${filename2}_Y1994.nc LAB/${filename2}_Y1995.nc LAB/${filename2}_Y1996.nc LAB/${filename2}_Y1997.nc  LAB/${filename2}_Y1998.nc  LAB/${filename2}_Y1999.nc LAB/${filename2}_Y2000.nc ${run}/${filename2}_${yd}-${yf}.nc

rm -Rf LAB/${filename2}_Y*.nc
done

