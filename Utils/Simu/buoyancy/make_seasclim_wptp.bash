#!/bin/bash

filetype='mean_wpXtempp_wt_roms_avg'
endfile='nc'
yeardeb=2005
yearend=2011
nyear=$(( yearend - yeardeb +1 ))
ndigit=4

#-----------------------------------------------------------------------
month=0
im=1
while [ ${month} -lt 12 ] ; do 
    month=$((month+1))
    echo '======================================'
    echo 'Process the clim for the month: '$month
    
    if [ ${im} -le 9 ] ; then
        mm='000'${im}
    elif [ ${im} -le 99 ] ; then
        mm='00'${im}
    elif [ ${im} -le 999 ] ; then
	mm='0'${im}
    fi
    echo 'im='$im
    echo 'mm='$mm
    
    infile=${filetype}_M_${mm}.${endfile}
    echo 'infile='$infile
    
    outfile=${filetype}_monthlymean_${month}.${endfile}
    echo 'clim outfile='$outfile
    
    echo ' ncra -n '$nyear','$ndigit',12 '$infile' -o '$outfile

    ncra -O -n $nyear,$ndigit,12 $infile -o $outfile

    im=$((im+1))
done
echo '============================================'
echo ' '
ncrcat -O ${filetype}_monthlymean_[123456789].${endfile} ${filetype}_monthlymean_1[012].${endfile} -o ${filetype}_seasclimmonthly.${endfile}
mv ${filetype}_seasclimmonthly.${endfile} ${filetype}_seasclimmonthly_${yeardeb}'_'${yearend}.${endfile}

#Cleaning
##rm -Rf ${filetype}_M_*.${endfile}
rm -Rf ${filetype}__monthlymean*.${endfile}
rm -Rf ${filetype}_seasclimmonthly.${endfile}


##Remove time =1 singlleton dimension
#for month in `seq 1 12` ;  do 
#    outfile=${filetype}_monthlymean_${month}.${endfile}	
#    ncwa -O -a time $outfile $outfile
#done	
