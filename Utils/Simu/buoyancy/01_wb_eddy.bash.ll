#/bin/bash

# @ job_name=01_WB_EDDY
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 20:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
######################################################################
cd $LOADL_STEP_INITDIR

set -x

#==================================================
source ./configure_wb
#==================================================

###########################

if [[ $flag_addvar == 1 ]]; then
    # 01 : Add the variables  b and w*rho to rho
    # do not forget the scaling after by - g/rho0
    
    if [[ $flag_addvaru == 1 ]]; then
	for year in `seq $ymin $ymax`; do
	    for file in `ls -1 roms_avg_Y${year}M*.nc` ; do
	    #for file in `ls -1 roms_avg_Y${year}M9.nc` ; do
		echo $file
		# put u on the rho grid
		ncap2 -O -F -s "ur=0*rho" ${file} ${file}
		##ncap2 -O -F -s "ur(:,:,:,2:$(($ind_xi - 2)) )=0.5*( u(:,:,:,2:$(( $ind_xi - 2)) ) + u(:,:,:,3:$(( $ind_xi - 1))) )" ${file} ${file}
		##ncap2 -O -F -s "ur(:,:,:,1)=ur(:,:,:,2)" -s "ur(:,:,:,$(( $ind_xi - 1)) ) = ur(:,:,:,$(( $ind_xi - 2)) )" ${file} ${file}

                ncap2 -O -F -s "ur(:,:,:,2:$(($ind_xi - 1)) )=0.5*( u(:,:,:,1:$(( $ind_xi - 2)) ) + u(:,:,:,2:$(( $ind_xi - 1))) )" ${file} ${file}
		ncap2 -O -F -s "ur(:,:,:,1)=ur(:,:,:,2)" -s "ur(:,:,:,$(( $ind_xi)) ) = ur(:,:,:,$(( $ind_xi - 1)) )" ${file} ${file}
		
		# Compute urho
		ncap2 -O -s "urho =  ur * rho" ${file} ${file}
		#echo "ncap2 -O -s urho =  ur * rho ${file} ${file}"
		echo "... ncap2 for urho"
	    done
	done #annees
    fi
    
    if [[ $flag_addvarv == 1 ]]; then
	for year in `seq $ymin $ymax`; do
	    for file in `ls -1 roms_avg_Y${year}M*.nc` ; do
	    #for file in `ls -1 roms_avg_Y${year}M9.nc` ; do
		echo $file
		# put v on the rho grid
		ncap2 -O -F -s "vr=0*rho" ${file} ${file}
		#ncap2 -O -F -s "vr(:,:,:,2:$(($ind_eta - 2)))=0.5*( v(:,:,2:$(( $ind_eta - 2)),:) + v(:,:,3:$(( $ind_eta - 1))),:)" ${file} ${file}
		#ncap2 -O -F -s "vr(:,:,1,:)=ur(:,:,2,:)" -s "vr(:,:,$(( $ind_eta - 1)),:) = vr(:,:,$(( $ind_eta - 2)),:)" ${file} ${file}
		
		ncap2 -O -F -s "vr(:,:,2:$(($ind_eta - 1)),:)=0.5*( v(:,:,1:$(( $ind_eta - 2)),:) + v(:,:,2:$(( $ind_eta -1)),:) )" ${file} ${file}
		ncap2 -O -F -s "vr(:,:,1,:)=vr(:,:,2,:)" -s "vr(:,:,$(( $ind_eta)),:) = vr(:,:,$(( $ind_eta - 1)),:)" ${file} ${file}
		
		#put v on rho grid
		ncap2 -O -s "vrho =  vr * rho" ${file} ${file}
		#echo "ncap2 -O -s vrho =  vr * rho ${file} ${file}"
		echo "... ncap2 for vrho"
	    done
	done #annees
    fi
    
    if [[ $flag_addvarw == 1 ]]; then
	for year in `seq $ymin $ymax`; do
	    for file in `ls -1 roms_avg_Y${year}M*.nc` ; do
	    #for file in `ls -1 roms_avg_Y${year}M9.nc` ; do
		echo $file
		ncap2 -O -s "wrho =  w * rho" ${file} ${file}
		#echo "ncap2 -O -s wrho =  w * rho ${file} ${file}"
		echo "... ncap2 for wrho"
	    done
	done #annees
    fi
    
fi
########################

if [[ $flag_compmean == 1 ]]; then
    # 02 STEP : Compute the mean
    #02.1 : renaming 
    ./rename2number.bash

    # 02.2 compute the mean
    listf=`ls -1 roms_avg_M_*.nc`
    clim -t=scrum_time $listf
    mv monthly.nc $filemonthly
    mv seasonal.nc $fileseasonal
    # =============================
    echo '  '
    echo ' Great job done '
    echo ' ============= '
    
    
    # 02.3: remove the number
    rm -Rf roms_avg_M_*.nc
fi

############################

if [[ $flag_compeddyu == 1 ]]; then
    # 04 STEP : add the variables U_pRHO_p as <U * RHO > - <U> * <RHO>
    ncap2 -O -s "uprhop=urho - ur*rho" $filemonthly $filemonthly
    ncap2 -O -s "uprhop=urho - ur*rho" $fileseasonal $fileseasonal
fi

if [[ $flag_compeddyv == 1 ]]; then
    # 04 STEP : add the variables W_pRHO_p as <V * RHO > - <V> * <RHO>
    ncap2 -O -s "vprhop=vrho - vr*rho" $filemonthly $filemonthly
    ncap2 -O -s "vprhop=vrho - vr*rho" $fileseasonal $fileseasonal
fi

if [[ $flag_compeddyw == 1 ]]; then
    # 04 STEP : add the variables W_pRHO_p as <W * RHO > - <W> * <RHO>
    ncap2 -O -s "wprhop=wrho - w*rho" $filemonthly $filemonthly
    ncap2 -O -s "wprhop=wrho - w*rho" $fileseasonal $fileseasonal
fi
