#!/bin/bash

for file in `ls -1 roms_avg_Y201[01]*` ; do

[ ! -d LAB ] && mkdir LAB
echo $file
#filein=roms_avg_Y2011M9.nc
filein=$file
wtfilein=wt_$filein

echo 'Process '$filein
echo ' '
ncks -v temp,w $filein LAB/$wtfilein

cd LAB

#Moyenne des produits
echo '==> Moyenne des produits'
ncap2 -A -s 'wXtemp=w*temp' ${wtfilein}
ncra ${wtfilein}  mean_wXtemp_${wtfilein}
ncrename -v wXtemp,VAR1 mean_wXtemp_${wtfilein}
echo '... done'
echo ' '

# Produits des moyennes
echo '==> Produit des moyennes'
ncra ${wtfilein} meanwXmeantemp_${wtfilein}
ncap2 -A -s 'VAR1=w*temp' meanwXmeantemp_${wtfilein}
echo 'Done'
echo ' '

#Difference
echo '==> Difference'
ncdiff mean_wXtemp_${wtfilein} meanwXmeantemp_${wtfilein} -o mean_wpXtempp_${wtfilein}
ncks -O -v VAR1 mean_wpXtempp_${wtfilein} mean_wpXtempp_${wtfilein}
ncrename -v VAR1,wpXtempp mean_wpXtempp_${wtfilein}
echo 'Done'
echo ' '

mv mean_wpXtempp_${wtfilein} ../
rm -Rf *
cd -

echo 'Process done on file '$filein

done
