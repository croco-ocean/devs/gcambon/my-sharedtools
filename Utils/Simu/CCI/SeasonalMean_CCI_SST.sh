#!/bin/bash

# Compute the various file Number of year for CCI-SST 1991-2010
# 1 Compute the monthly mean from the All date files
 
# 2 From the monthly mean one-file SST then compute: 
#   2.1 the multi-file sequence with monthly mean
#   2.2 compte the monthly seasonal file

#1 
cd SUDAF_XTR_OK/ 
listf=`ls -1 *_All*`
cdo monmean $listf 1992-2010-monmean_SST-CCI-daily.nc

#2.1 
for mm in `seq 1 12` ; do
    echo ' '
    echo 'Process month #'$mm
    echo '==================='
    mm2=$mm
    if [ $mm -lt 10 ] ; then 
	mm2='0'$mm
    fi
    for yy in `seq 1 19` ; do 
	yy2=$yy
	if [ $yy -lt 10 ] ; then 
	    yy2='0'$yy
	fi
	it=$(( ($yy - 1) * 12 + $mm ))
	echo 'Process  the month : '$it
	it2=$it
	if [ $it -lt 10 ] ; then
	    it2='00'$it
	elif [ $it -lt 100 ] ; then
	    it2='0'$it
	fi
	ncks -O -F -d time,$it,$it 1992-2010-monmean_SST-CCI-daily.nc -o 1992-2010-monmean_SST-CCI-daily_M_${it2}.nc
    done
done

#2.2
ncra -n 12,3,12 1992-2010-monmean_SST-CCI-daily_M_001.nc -o test.nc

cd -
