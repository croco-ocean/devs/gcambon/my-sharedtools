#!/bin/bash

#Flag definitions :
storing_flag=0
copying_flag=0
rename_flag=0
unpack_flag=0
extract_flag=0
concat_year_flag=0
process_seasclim=1

extractdir='SUDAF_XTR_OK_V2'
#Options
concat_flag=0

# 001    =============================================================================
if [ $storing_flag == 1 ] ; then
    echo '==========================================================================='
    echo 'Process_storing...'
    DIR='./v01.1_1991-2010-daily/'
    DIR2='./Process/'
    for yy in `seq 1991 2010`;  do 
	#for yy in 1992;  do 
	for mm in 01 02 03 04 05 06 07 08 09 10 11 12 ; do 
	    for jj in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31; do
		echo 'Process Year: '$yy' Month: '$mm' Jour: '$jj
		mv $DIR/$yy/$mm/$jj/* $DIR2	
	    done
	done
    done
fi

#  002 ===============================================================================
if [ $copying_flag == 1 ] ; then 
    echo '==========================================================================='
    echo 'Process copying...'
    cp -Rf Process/* Process_dev/
fi

#  003  =============================================================================
if [ $unpack_flag == 1 ] ; then 
    echo '==========================================================================='
    echo 'Process unpack ...'
    cd Process_dev/
    listfile=`ls *.nc`
    for i in $listfile; do
	echo $i
	#unpack
	ncpdq -O -U $i $i
    done
fi

#  004 ===============================================================================
if [ $rename_flag == 1 ] ; then 
    echo '==========================================================================='
    echo 'Process renaming...'
    listfile=`ls -1 Process_dev/*`
    for i in $listfile; do
	echo $i
    aa=`echo $i | cut -c1-33`
    newname=${aa}'.nc'
    mv ${i} ${newname}
    done
fi

#  005 ===============================================================================
if [ $extract_flag == 1 ] ; then 
    echo '==========================================================================='
    #Extraction domaine 1/4 : SUDAF :
    indS=800
    indN=2000
    indW=3060
    indE=5001
    echo 'Process extracting ...'
    cd Process_dev/
    listfile=`ls -1 *.nc`
    for i in $listfile ; do
	echo $i
	ncks -v time_bnds,time,lat,lon,analysed_sst -d lat,$indS,$indN -d lon,$indW,$indE $i $extractdir/$i
	ncrename -v analysed_sst,sst $extractdir/$i 
	#ncap2 -O -s 'sst=sst-273.15' SUDAF_XTR/$i
    done
    cd -
fi

#  006 ==============================================================================
if [ $concat_year_flag == 1 ] ; then 
    cd Process_dev/$extractdir
#    for i in `seq 1992 2010` ; do
    for i in `seq 1992 2010` ; do 
	echo '================'
	echo 'Process year '$i
	listfile0=`ls -1 $i*.nc`
	ncrcat $listfile0 -o ${i}_All_SST-CCI.nc
	cdo monmean ${i}_All_SST-CCI.nc ${i}_monthmean_SST-CCI.nc
    done
    listf2=`ls -1  *_monthmean_SST-CCI.nc`
    ncrcat $listf2 1992-2010-monmean_SST-CCI-daily.nc
    cd -
fi

#  007  ==============================================================================
#  007.1 Extract the various monthly mean file 
if [ $process_seasclim == 1 ] ; then 
    cd Process_dev/$extractdir 
    for mm in `seq 1 12` ; do
	echo ' '
	echo 'Process month #'$mm
	echo '==================='
	mm2=$mm
	if [ $mm -lt 10 ] ; then 
	    mm2='00'$mm
	else
	    mm2='0'$mm
	fi
	for yy in `seq 1 19` ; do 
	    yy2=$yy
	    if [ $yy -lt 10 ] ; then
		yy2='0'$yy
	    fi
	    it=$(( ($yy - 1) * 12 + $mm ))
	    it2=$it
	    if [ $it -lt 10 ] ; then
		it2='00'$it
	    elif [ $it -lt 100 ] ; then
		it2='0'$it
	    fi
	    echo 'Process  the month it : '$it2
	    ncks -O -F -d time,$it,$it 1992-2010-monmean_SST-CCI-daily.nc -o 1992-2010-monmean_SST-CCI-daily_M_${it2}.nc
	done
	echo ' '
	echo '  -> Process the monthly mean for this month'
	ncra -n 19,3,12 1992-2010-monmean_SST-CCI-daily_M_${mm2}.nc -o 1992-2010-seasmean_SST-CCI-daily_MM_${mm2}.nc
    done
    #   007.2
    listf=`ls -1 1992-2010-seasmean_SST-CCI-daily_MM_*.nc`
    ncrcat $listf -o 1992-2010-seasmean_SST-CCI-daily_M1-12.nc
    rm -f 1992-2010-monmean_SST-CCI-daily_M_*.nc
    rm -f 1992-2010-seasmean_SST-CCI-daily_MM_*.nc
    cd -
fi

#  007 ==============================================================================
if [ $concat_flag == 1 ] ; then
    echo '==========================================================================='
    echo 'Process concat ....'
    ncrcat $listfile -o All-SST-CCI-1991-2010.nc
    ncks -O -v time,lat,lon,analysed_sst All-SST-CCI-1991-2010.nc
    ncrename -O -v analysed_sst,sst All-SST-CCI-1991-2010.nc
fi
#=============================================================================
