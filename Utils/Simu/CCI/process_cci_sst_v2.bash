#!/bin/bash

#Flag definitions :
copying_flag=1
extract_flag=1
concat_year_flag=1
process_seasclim=1
concat_flag=0

#extractdir='/local/tmp/2/gcambon/SST_CCI/SUDAF_XTR_V2'
extractdir='/local/tmp/2/gcambon/SST_CCI/ATLTROP'

#  001 ==============================================================================
if [ $extract_flag == 1 ] ; then 
    echo '==========================================================================='
    #Extraction domaine 1/4 : SUDAF :
    #indS=500
    #indN=2000
    #indW=3060
    #indE=5001
    
    #ATL trop Gaelle
    indS=1300
    indN=2000
    indW=1020
    indE=4000

    echo 'Process extracting ...'
    cd Global/
    listfile=`ls -1 *.nc`
    for i in $listfile ; do
	echo $i
	ncks -v time_bnds,time,lat,lon,analysed_sst -d lat,$indS,$indN -d lon,$indW,$indE $i $extractdir/$i
	ncrename -v analysed_sst,sst $extractdir/$i 
	#ncap2 -O -s 'sst=sst-273.15' SUDAF_XTR/$i
    done
    cd -
fi

#  002 ==============================================================================
if [ $concat_year_flag == 1 ] ; then 
    cd $extractdir
    for i in `seq 1992 2010` ; do 
	echo '================'
	echo 'Process year '$i
	listfile0=`ls -1 $i*.nc`
	ncrcat $listfile0 -o ${i}_All_SST-CCI.nc
	cdo monmean ${i}_All_SST-CCI.nc ${i}_monthmean_SST-CCI.nc
    done
    listf2=`ls -1  *_monthmean_SST-CCI.nc`
    ncrcat $listf2 1992-2010-monmean_SST-CCI-daily.nc
    cd -
fi
#  003  ==============================================================================
#  003.1 Extract the various monthly mean file 
if [ $process_seasclim == 1 ] ; then 
    cd $extractdir 
    for mm in `seq 1 12` ; do
	echo ' '
	echo 'Process month #'$mm
	echo '==================='
	mm2=$mm
	if [ $mm -lt 10 ] ; then 
	    mm2='00'$mm
	else
	    mm2='0'$mm
	fi
	for yy in `seq 1 19` ; do 
	    yy2=$yy
	    if [ $yy -lt 10 ] ; then
		yy2='0'$yy
	    fi
	    it=$(( ($yy - 1) * 12 + $mm ))
	    it2=$it
	    if [ $it -lt 10 ] ; then
		it2='00'$it
	    elif [ $it -lt 100 ] ; then
		it2='0'$it
	    fi
	    echo 'Process  the month it : '$it2
	    ncks -O -F -d time,$it,$it 1992-2010-monmean_SST-CCI-daily.nc -o 1992-2010-monmean_SST-CCI-daily_M_${it2}.nc
	done
	echo ' '
	echo '  -> Process the monthly mean for this month'
	ncra -n 19,3,12 1992-2010-monmean_SST-CCI-daily_M_${mm2}.nc -o 1992-2010-seasmean_SST-CCI-daily_MM_${mm2}.nc
    done
    #   0073.2
    listf=`ls -1 1992-2010-seasmean_SST-CCI-daily_MM_*.nc`
    ncrcat $listf -o 1992-2010-seasmean_SST-CCI-daily_M1-12.nc
    rm -f 1992-2010-monmean_SST-CCI-daily_M_*.nc
    rm -f 1992-2010-seasmean_SST-CCI-daily_MM_*.nc
    cd -
fi

#  004 ==============================================================================
if [ $concat_flag == 1 ] ; then
    echo '==========================================================================='
    echo 'Process concat ....'
    
    cd $extractdir
    ncrcat $listfile -o All-SST-CCI-1991-2010.nc
    ncks -O -v time,lat,lon,analysed_sst All-SST-CCI-1991-2010.nc
    ncrename -O -v analysed_sst,sst All-SST-CCI-1991-2010.nc
    
    cd -
fi
#=============================================================================
