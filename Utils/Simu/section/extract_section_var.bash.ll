#!/bin/bash

# Nom du travail LoadLeveler
# @ job_name=extract_section_var 
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ wall_clock_limit = 20:00:00
# @ requirements = (Feature == "prepost")
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
######################################################################
cd $LOADL_STEP_INITDIR

#####################################
# DEFINE THE SECTION

source configure_section
#=================================

for mysec in $MYSECTION ;  do
    echo 'process the section '$mysec
    case $mysec in
	#== R1KM
	GLOB1KM)
            # -> GLOBAL3KM section
            nlevel=50
            extx=300
            indXCOASTCC=490
            exty=250
            indYCENTERCC=400
            ymin=2005
            ymax=2011
            sectioname='_sectionGLOB1KM'
            ;;
	#== R3KM
	GLOB3KM)
            # -> GLOBAL3KM section
            nlevel=75
            extx=100
            indXCOASTCC=317
            exty=60
            indYCENTERCC=175
            ymin=1995
            ymax=2011
            sectioname='_sectionGLOB3KM'
            ;;
        GLOB3KM_EXTEAST)
            # -> GLOBAL3KM_EXTEAST section
            nlevel=75
            extx=100
            indXCOASTCC=417
            exty=100
            indYCENTERCC=50
            ymin=2005
            ymax=2005
            sectioname='_sectionGLOB3KM_EXTEAST'
            ;;
        CC3KM) 
	    # -> CColumbine section
	    nlevel=75
	    extx=100
	    indXCOASTCC=300
	    exty=20
	    indYCENTERCC=193
	    ymin=1995
	    ymax=2011
	    sectioname='_sectionCC'
	    ;;
	CP3KM)
	    # -> CPoint section
	    nlevel=75
	    extx=100
	    indXCOASTCC=320
	    exty=20
	    indYCENTERCC=142
	    ymin=1995
	    ymax=1995
	    sectioname='_sectionCP'
	    ;;
	GB3KM)
	    # ===
	    # -> GBat secction
	    nlevel=75
	    extx=100
	    indXCOASTCC=300
	    exty=20
	    indYCENTERCC=193
	    ymin=1995
	    ymax=1995
	    sectioname='_sectionGB'
	    ;;
	#== R7KM
	GLOB7KM)
            # -> GLOBAL7KM section
            nlevel=100
            extx=70
            indXCOASTCC=175
            exty=50
            indYCENTERCC=295
            #ymin=1989
            #ymax=2012
            ymin=1992
            ymax=2012
	    sectioname='_sectionGLOB7KM'
            ;;
    esac
    #=====================================
    #######################################
    
    indYMAXCC=$(($indYCENTERCC + $exty))
    indYMINCC=$(($indYCENTERCC - $exty))
    indXMAXCC=$(($indXCOASTCC))
    indXMINCC=$(($indXCOASTCC - $extx))
    LABDIR='LAB'$sectioname
    
    # Extract the grid only
    #========================
    [ ! -d $LABDIR ]  && mkdir $LABDIR
    
    ncks -O -v lat_rho,lon_rho,lat_u,lon_u,lat_v,lon_v,f,pm,pn,mask_rho -d xi_rho,$indXMINCC,$indXMAXCC -d eta_rho,$indYMINCC,$indYMAXCC -d xi_u,$indXMINCC,$(($indXMAXCC -1 )) -d eta_v,$indYMINCC,$(($indYMAXCC -1)) roms_avg_Y2000M12.nc -o grid_roms$sectioname.nc

    
    for yy in `seq ${ymin} ${ymax}` ; do
	echo "Process year $yy"
	
	for filein in `ls roms_avg_Y$yy*` ; do
	    
	    echo "Process file $filein"
	    fileout=`echo $filein | cut -d . -f 1,1`$sectioname.nc

	    #===	    
	    # CMD="ncks -F -v lat_rho, lon_rho,lat_u,lon_u,lat_v,lon_v sustr,svstr,temp,u,v -d s_rho,$nlevel,$nlevel -d xi_rho,$indXMINCC,$indXMAXCC -d eta_rho,$indYMINCC,$indYMAXCC -d xi_u,$indXMINCC,$(($indXMAXCC -1 )) -d eta_v,$indYMINCC,$(($indYMAXCC -1)) $filein -o $LABDIR/$fileout"
	    # echo $CMD
	    
	    # ncks -O -F -v lat_rho,lon_rho,lat_u,lon_u,lon_v,lat_v,sustr,svstr,temp,u,v -d s_rho,$nlevel,$nlevel -d xi_rho,$indXMINCC,$indXMAXCC -d eta_rho,$indYMINCC,$indYMAXCC -d xi_u,$indXMINCC,$(($indXMAXCC -1 )) -d eta_v,$indYMINCC,$(($indYMAXCC -1)) $filein -o $LABDIR/$fileout
	    
	    # ncrename -O -v temp,SST -v u,usurf -v v,vsurf $LABDIR/$fileout

	    #====

	    ncks -O -F -v lat_rho,lon_rho,lat_u,lon_u,lon_v,lat_v,sustr,svstr,temp,u,v,zeta -d xi_rho,$indXMINCC,$indXMAXCC -d eta_rho,$indYMINCC,$indYMAXCC -d xi_u,$indXMINCC,$(($indXMAXCC -1 )) -d eta_v,$indYMINCC,$(($indYMAXCC -1)) $filein -o $LABDIR/$fileout
	    
	done
    done
done
