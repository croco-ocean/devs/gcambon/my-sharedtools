#!/bin/bash

# Nom du travail LoadLeveler
# @ job_name= 3DVAR_ZINTERP
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ wall_clock_limit = 20:00:00
# @ requirements = (Feature == "prepost")
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
######################################################################
cd $LOADL_STEP_INITDIR


#zslice 0 -5 -10 -15 -20 -25 -30 -35 -40 -45 -50 -55 -60 -65 -70 -75 -80 -85 -90 -95 -100 -105 -110 -115 -120 -125 -130 -135 -140 -145 -150 -155 -160 -165 -170 -175 -180 -185 -190 -195 -200 -205 -210 -215 -220 -225 -230 -235 -240 -245 -250 -255 -260 -265 -270 -275 -280 -285 -290 -295 -300 -305 -310 -315 -320 -325 -330 -335 -340 -345 -350 -355 -360 -365 -370 -375 -380 -385 -390 -395 -400 --vars=temp,salt,u,v,rho roms_avg_Y1995M1.nc

#####################################
# DEFINE THE SECTION

sectioning_flag=1
zslicing_flag=1
appending_flag=1

source configure_section

for mysec in $MYSECTION ; do
    echo 'process the section '$mysec
    
    case $mysec in
	#== R1KM
	GLOB1KM)
            # -> GLOBAL3KM section
            nlevel=50
            extx=300
            indXCOASTCC=490
            exty=250
            indYCENTERCC=400
            ymin=2005
            ymax=2011
            sectioname='_sectionGLOB1KM'
            ;;
	GLOB3KM) 
	    # CColumbine section for R3KM
	    nlevel=75
	    extx=100
	    indXCOASTCC=317
	    exty=60
	    indYCENTERCC=175
	    ymin=1996
	    ymax=2011
	    sectioname='_sectionGLOB3KM'
	    ;;
	CC3KM) 
	    # CColumbine section for R3KM
	    nlevel=75
	    extx=100
	    indXCOASTCC=300
	    exty=20
	    indYCENTERCC=193
	    ymin=1995
	    ymax=2011
	    sectioname='_sectionCC'
	    ;;
	CP3KM)
	    # CPoint section for R3KM
	    nlevel=75
	    extx=100
	    indXCOASTCC=320
	    exty=20
	    indYCENTERCC=140
	    ymin=1995
	    ymax=2011
	    sectioname='_sectionCP'
	    ;;
	GB3KM)
	    # ===
	    # GBat secction for R3KM
	    nlevel=75
	    extx=100
	    indXCOASTCC=300
	    exty=20
	    indYCENTERCC=193
	    ymin=1995
	    ymax=2011
	    sectioname='_sectionGB'
	    ;;
	#== R7KM
	GLOB7KM)
            # -> GLOBAL7KM section
            nlevel=100
            extx=70
            indXCOASTCC=175
            exty=50
            indYCENTERCC=295
            ymin=1989
            ymax=2012
            sectioname='_sectionGLOB7KM'
    esac
    #===================================== 

    #######################################
    
    
    indYMAXCC=$(($indYCENTERCC + $exty))
    indYMINCC=$(($indYCENTERCC - $exty))
    indXMAXCC=$(($indXCOASTCC))
    indXMINCC=$(($indXCOASTCC - $extx))
    LABDIR='LAB'$sectioname
    LABZDIR='LAB_Z'$sectioname

    [ ! -d $LABZDIR ]  && mkdir $LABZDIR
    
    for yy in `seq ${ymin} ${ymax}` ; do
	echo "Process year $yy"
	for filein in `ls roms_avg_Y$yy*` ; do
#	    fileinz=z_`echo $filein`
	    fileoutz=z_`echo $filein | cut -d . -f 1,1`$sectioname.nc
	    fileout=`echo $filein | cut -d . -f 1,1`$sectioname.nc
#	    echo $fileinz
	    echo 'fileoutz='$fileoutz
	    echo '   '
	    echo '   '
	    echo "=> Process file $filein"
	    #	    
	    if [ $sectioning_flag == 1 ]; then
		CMD="ncks -F -d xi_rho,$indXMINCC,$indXMAXCC -d eta_rho,$indYMINCC,$indYMAXCC -d xi_u,$indXMINCC,$(($indXMAXCC -1 )) -d eta_v,$indYMINCC,$(($indYMAXCC -1)) $filein -o $LABZDIR/$fileout"
		echo $CMD
		
		ncks -F -d xi_rho,$indXMINCC,$indXMAXCC -d eta_rho,$indYMINCC,$indYMAXCC -d xi_u,$indXMINCC,$(($indXMAXCC -1 )) -d eta_v,$indYMINCC,$(($indYMAXCC -1)) $filein -o $LABZDIR/$fileout
		
#		# Add the grid in it
#		ncks -A grid_roms$sectioname.nc $LABZDIR/$fileout
	    fi
	    #==	    
	    if [ $zslicing_flag == 1 ]; then
		cd $LABZDIR
		
		# Zslicing
		zslice 0 -5 -10 -15 -20 -25 -30 -35 -40 -45 -50 -55 -60 -65 -70 -75 -80 -85 -90 -95 -100 -105 -110 -115 -120 -125 -130 -135 -140 -145 -150 -155 -160 -165 -170 -175 -180 -185 -190 -195 -200 -205 -210 -215 -220 -225 -230 -235 -240 -245 -250 -255 -260 -265 -270 -275 -280 -285 -290 -295 -300 --vars=temp,salt,u,v,rho,w $fileout	
		cd - 
	    fi
#==
	    if [ $appending_flag == 1 ]; then
		ncks -A $LABZDIR/$fileoutz $LABDIR/$fileout

		rm -Rf $LABZDIR/$fileoutz
		rm -Rf $LABZDIR/$fileout

	    fi
#==
	done
    done
    
done
