#!/bin/bash
###################################################################@
# Nom du travail LoadLeveler
# @ job_name= BENGSAFE_R15_1D_EXTRACTION
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 20:00:00
# @ requirements = (Feature == "prepost")
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ queue
# @ as_limit = 20.0gb
#-------------
## La variable LOADL_STEP_INITDIR est automatiquement positionnee par
## LoadLeveler au repertoire dans lequel on tape la commande llsubmit
## Execution d'un programme MPI.
#-------------
cd $LOADL_STEP_INITDIR

flag_extractNAT=0
flag_extractR2R=1

#Extract the run for natalie and alice
# => extract_GRID flag
extract_GRID=1

# => extract_avg flag
extract_AVG=1

if [ ${flag_extractNAT} == 1 ]; then 
######################################################################
#Extract the run for natalie and alice
# Extraction V1 for rapat in LPO/IUEM + Nat, Claude, Steven for R15 
indw=114
inde=563
inds=220
indn=640
ymin=1989
ymax=2012

DIRIN='blkperio_R15_CFSR_t2_1d'
DIROUT='blkperio_R15_CFSR_t2_1d_EXTRACTED'
filetype_list='roms_rst'
#filetype_list='roms_avg'

fi
#==

if [ ${flag_extractR2R} == 1 ]; then
#=======================================================
#
# Extraction V2 for R2R from R15 to R3KM
#
#=======================================================
indw=143
inde=533
inds=175
indn=673

ymin=2003
ymax=2012

DIRIN='blkperio_R15_CFSR_t2_1d'
DIROUT='blkperio_R15_CFSR_t2_1d_EXTRACTED_forR2R'
#filetype_list='roms_avg roms_rst roms_his'
filetype_list='roms_avg'

fi
#####################################################################

if [ $extract_AVG == 1 ]; then
    
    [ ! -d ${DIROUT} ]  && mkdir ${DIROUT}
    for filetype in $filetype_list ; do
	for Y in `seq ${ymin} ${ymax}` ;  do 
	    for M in `seq 12` ;  do 
		i=${filetype}_Y${Y}M${M}.nc 
		echo $i
		ncks -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) ${DIRIN}/$i ${DIROUT}/$i 
		
	    done
	done
    done
fi

#==========================================================
# GRID EXTRACTION
#==========================================================

if [ ${extract_GRID} == 1 ] ; then
    
    [ ! -d ${DIROUT} ]  && mkdir ${DIROUT}
    
    echo 'extract_GRID is ON'
    i=roms_grd.nc
     echo $i
     
     CMD="ncks -O -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_u,$inds,$indn -d xi_v,$indw,$inde -d eta_v,$inds,$((indn-1)) -d xi_psi,$indw,$((inde-1)) -d eta_psi,$inds,$((indn-1)) ${DIRIN}/$i ${DIROUT}/$i"
     
     echo $CMD
    $CMD
fi
