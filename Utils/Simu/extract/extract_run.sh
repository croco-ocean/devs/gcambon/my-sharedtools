# Nom du travail LoadLeveler
# @ job_name= EXTRACTION
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# requirement = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 40:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = error
# @ as_limit = 20.0gb
# @ queue
######################################################################
#Extract the run for natalie and alice

# Extraction V1 for rapat in LPO/IUEM + Nat, Claude, Steven for R15 
indw=114
inde=563
inds=220
indn=640

#ymin=1989
#ymax=2010

#ymin=1986
#ymax=1988

ymin=1986
ymax=1988
mmin=1
mmax=12

# Extraction V2 for R2R from R15 to R3KM and R3KM/R1KM

#indw=143
#inde=533
#inds=175
#indn=673

#ymin=1986
#ymax=1988
#####################################################################

M=$mmin
Y=$ymin
#echo $Y
#echo ${ymax}
#echo ' '  
#------------------

while [ $Y -le ${ymax} ]; do 
    echo $Y
    echo ${ymax}
    echo ' '  

    if [ $Y -eq ${ymin} ] ;  then
	M=$mmin
    else
	M=1
    fi

    while [ $M -le 12 ]; do 	
	echo 'M= '$M	
	if [ $Y -eq ${ymax} ] && [ $M -gt ${mmax} ]; then exit 0; fi
        # compute next month and year
	Mnxt=$(($M + 1))
	if [ ${Mnxt} -gt 13 ]; then
            M=1
        fi
	i=roms_avg_Y${Y}M${M}.nc 
	#echo $i
	#echo '------'	
	#echo 'ncks -F -d xi_rho,'$indw','$inde '-d eta_rho,'$inds','$indn '-d xi_u,'$indw','$((inde-1)) '-d eta_v,'$inds','$((indn -1)) $i 'LAB/'$i 
	ncks -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) $i EXTRACTED/$i 
	
	M=$(($M + 1))
    done   
    Y=$(($Y +1))
done
