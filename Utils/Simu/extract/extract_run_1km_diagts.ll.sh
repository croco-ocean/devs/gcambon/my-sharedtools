#!/bin/bash
###################################################################@
# Nom du travail LoadLeveler
# @ job_name= BENGSAFE_R15_DIAGTS_EXTRACTION
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 10:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ queue
#-------------
## La variable LOADL_STEP_INITDIR est automatiquement positionnee par
## LoadLeveler au repertoire dans lequel on tape la commande llsubmit
## Execution d'un programme MPI.
#-------------
cd $LOADL_STEP_INITDIR

flag_extractR2R=1
if [ ${flag_extractR2R} == 1 ]; then
#=======================================================
# Extraction V2 for R2R from R15 to R3KM and R3KM/R1KM
#=======================================================
indw=200
inde=500
inds=260
indn=560

ymin=2003
ymax=2004

DIROUT='../EXTRACTED/'
#filetype_list='roms_rst roms_avg'
filetype_list='roms_dia_avg'

fi
#####################################################################

for filetype in $filetype_list ; do
for Y in `seq ${ymin} ${ymax}` ;  do 
    for M in `seq 12` ;  do 
	i=${filetype}_Y${Y}M${M}.nc 
	echo $i
#	echo 'ncks -F -d xi_rho,'$indw','$inde '-d eta_rho,'$inds','$indn '-d xi_u,'$indw','$((inde-1)) '-d eta_v,'$inds','$((indn -1)) $i 'LAB/'$i 
	ncks -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) $i ${DIROUT}/$i 
	
    done
done
done
