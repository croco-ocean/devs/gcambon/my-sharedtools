#/bin/bash

#==
source CONFIG_extrfordiag
#==
[ ! -d $DIROUT ] && mkdir $DIROUT

for i in `ls roms_grd*.nc`; do
    echo $i
    
    CMD="ncks -F -d xi_rho,$indw,$inde,$skip -d eta_rho,$inds,$indn,$skip -d xi_u,$indw,$((inde-1)),$skip -d eta_u,$inds,$indn,$skip -d xi_v,$indw,$inde,$skip -d eta_v,$inds,$((indn-1)),$skip -d xi_psi,$indw,$((inde-1)),$skip -d eta_psi,$inds,$((indn-1)),$skip $i ${DIROUT}/${DIROUT}_$i"
    
    echo $CMD
    $CMD
    
done

