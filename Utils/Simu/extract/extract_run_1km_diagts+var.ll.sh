#!/bin/bash
###################################################################@
# Nom du travail LoadLeveler
# @ job_name= BENGSAFE_R15_DIAGTS_EXTRACTION
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 10:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ queue
#-------------
## La variable LOADL_STEP_INITDIR est automatiquement positionnee par
## LoadLeveler au repertoire dans lequel on tape la commande llsubmit
## Execution d'un programme MPI.
#-------------
cd $LOADL_STEP_INITDIR

flag_extractR2R=1
if [ ${flag_extractR2R} == 1 ]; then
#=======================================================
# Extraction V2 for R2R from R15 to R3KM and R3KM/R1KM
#=======================================================
indw=200
inde=500
inds=260
indn=560

ymin=2005
ymax=2005

temp_var='temp_entr_mld,temp_forc,temp_forc_mld,temp_hmix,temp_hmix_mld,temp_rate,temp_rate_mld,temp_vadv,temp_vadv_mld,temp_vmix,temp_vmix_mld,temp_xadv,temp_xadv_mld,temp_yadv,temp_yadv_mld'
salt_var='salt_entr_mld,salt_forc,salt_forc_mld,salt_hmix,salt_hmix_mld,salt_rate,salt_rate_mld,salt_vadv,salt_vadv_mld,salt_vmix,salt_vmix_mld,salt_xadv,salt_xadv_mld,salt_yadv,salt_yadv_mld'

DIROUT='EXTRACTED_GEO_VAR'
#filetype_list='roms_rst roms_avg'
filetype_list='roms_dia_avg'

fi
#####################################################################

for filetype in $filetype_list ; do
for Y in `seq ${ymin} ${ymax}` ;  do 
    for M in `seq 12` ;  do 
	i=${filetype}_Y${Y}M${M}.nc 
	echo $i
#	echo 'ncks -F -d xi_rho,'$indw','$inde '-d eta_rho,'$inds','$indn '-d xi_u,'$indw','$((inde-1)) '-d eta_v,'$inds','$((indn -1)) $i 'LAB/'$i 
	ncks -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) -x -v $salt_var  $i ${DIROUT}/$i 
	
    done
done
done
