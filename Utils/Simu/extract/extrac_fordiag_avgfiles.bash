#/bin/bash

#==
source CONFIG_extrfordiag
#==

[ ! -d $DIROUT ] && mkdir $DIROUT

#================================
for Y in `seq $ymin $ymax`; do 
echo '=> Year :'$Y
for i in `ls roms_avg_Y${Y}M*.nc`; do
echo $i

CMD="ncks -F -d xi_rho,$indw,$inde,$skip -d eta_rho,$inds,$indn,$skip -d xi_u,$indw,$((inde-1)),$skip -d eta_v,$inds,$((indn -1)),$skip $i ${DIROUT}/${DIROUT}_$i"

echo $CMD
$CMD
done
done
