#!/bin/bash

DIRIN='./'
DIROUT='LAB/'

indw=575
inde=1057
inds=466
indn=1027

process_avg=1
process_grid=0

# ##average files
# ncks -O -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) ${DIRIN}/$i ${DIROUT}/$i

# ## grid files
# ncks -O -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_u,$inds,$indn -d xi_v,$indw,$inde -d eta_v,$inds,$((indn-1)) -d xi_psi,$indw,$((inde-1)) -d eta_psi,$inds,$((indn-1)) ${DIRIN}/$i ${DIROUT}/$i
# echo $CMD
# $CMD

if [[ $process_avg == 1 ]]; then
    for mm in `seq 1 5`; do
	file=croco_avg_Y2006M${mm}.nc
	echo "==> file is : "$file
	##average files
	CMD="ncks -O -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) ${DIRIN}${file} ${DIROUT}${file}"
	echo $CMD
	$CMD
	echo "  "
    done
fi


# GRID
if [[ $process_grid == 1 ]]; then
    gridfile=croco_grd.nc
    CMD="ncks -O -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_u,$inds,$indn -d xi_v,$indw,$inde -d eta_v,$inds,$((indn-1)) -d xi_psi,$indw,$((inde-1)) -d eta_psi,$inds,$((indn-1)) ${DIRIN}/$gridfile ${DIROUT}/$gridfile"
    echo $CMD
    $CMD
fi

