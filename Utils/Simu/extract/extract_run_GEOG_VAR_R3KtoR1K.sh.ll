#!/bin/bash
###################################################################@
# Nom du travail LoadLeveler
# @ job_name= BENGSAFE_R3KM_EXTRAC_VAR_GEOG_V2C
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 10:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ queue
#-------------
## La variable LOADL_STEP_INITDIR est automatiquement positionnee par
## LoadLeveler au repertoire dans lequel on tape la commande llsubmit
## Execution d'un programme MPI.
#-------------
cd $LOADL_STEP_INITDIR


extract_forR2R=1
extract_forsoda=0


#Extract the run for natalie and alice
# => extract_GRID flag
extract_GRID=1

# => extract_avg flag
extract_AVG=1

if [ $extract_forR2R -eq 1 ]; then
# Extraction of BENG_R3KM for BENG_R1KM_V2 to create r2r bryfiles 
indw=1
inde=410
inds=280
indn=840  
#840 is the max ind in eta dir
#ymin=2010
#ymax=2011
ymin=1990
ymax=2012

DIRIN='BENGSAFE_R3KM_V2c'
DIROUT='BENGSAFE_R3KM_V2c_EXTRACTED_forR2R'

fi

if [ $extract_forsoda -eq 1 ]; then
# Extraction of BENG_R3KM for BENG_R3KM_forsoda to create a reduced climatology
indw=100
inde=410
inds=300
indn=650
#840 is the max ind in eta dir
ymin=1990
ymax=2012

DIRIN='R3KM/BENGSAFE_R3KM_V2c'
DIROUT='R3KM/BENGSAFE_R3KM_V2c_EXTRACTED_forsoda'

fi	

if [ $extract_AVG == 1 ]; then

#for filetype in 'roms_avg' 'roms_rst' ; do
for filetype in 'roms_his' ; do
echo 'Process filetype '$filetype
#####################################################################

for Y in `seq ${ymin} ${ymax}` ;  do 
    for M in `seq 1 12` ;  do 
	i=${filetype}_Y${Y}M${M}.nc 
	echo $i
	#echo "ncks -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn, -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) ${DIRIN}/$i ${DIROUT}/$i" 
	ncks -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_v,$inds,$((indn -1)) ${DIRIN}/$i ${DIROUT}/$i 
    done
done

done
fi

#==========================================================
# GRID EXTRACTION
#==========================================================

if [ ${extract_GRID} == 1 ] ; then
     echo 'extract_GRID is ON'
     i=roms_grd.nc
     echo $i

     CMD="ncks -O -F -d xi_rho,$indw,$inde -d eta_rho,$inds,$indn -d xi_u,$indw,$((inde-1)) -d eta_u,$inds,$indn -d xi_v,$indw,$inde -d eta_v,$inds,$((indn-1)) -d xi_psi,$indw,$((inde-1)) -d eta_psi,$inds,$((indn-1)) ${DIRIN}/$i ${DIROUT}/$i"

     echo $CMD
    $CMD
fi
