#!/bin/bash

set -x
set -e

var=(THETA \
     SALT  \
     SSH   \
     UVEL  \
     VVEL)

# var=(\
#      SALT  \
#      SSH   \
#      UVEL  \
#      VVEL)

dirout=/local/tmp/2/gcambon/ECCO2_cube92_global

yeardeb=2016
yearend=2016
monthdeb=9
monthend=11

clean_flag=1
rename_flag=1
ncrcat_flag=1


# cleaning and removing empty files
if [ $clean_flag = 1 ]; then
    cd $dirout
    for yy in `seq ${yeardeb} ${yearend}` ; do
	#for month in `seq ${monthdeb} ${monthend}` ; do
	for month in ${monthdeb} ${monthend} ; do
	    month2=`printf "%02d" $month`
	    cd $yy/$month2
	    
	    #=======================================================
	    for i in `ls -1 *.nc` ; do
		#ss=`du -ms $i |  tr -s [::] | cut -d' ' -f1-1`
		ss=`du -ks $i | cut -f1-1`
		if [ $ss -lt 100 ] ; then
		    cmd="rm -f $i"
		    $cmd
		fi
	    done
	    #=======================================================
           cd -	    
	done
    done
    cd -
fi
#$$$$$$$$$$$$$$

cd $dirout

for k in ${var[*]} ; do 
    for yy in `seq ${yeardeb} ${yearend}` ; do
	#for month in `seq ${monthdeb} ${monthend}` ; do
	for month in ${monthdeb} ${monthend} ; do
	    month2=`printf "%02d" $month`
	    cd $yy/$month2

	    listf=`ls -1 ${k}*.nc`
	    
	    if [ $rename_flag = 1 ]; then
		# mk_rec_dim
		for i in $listf; do
		    cmd0="ncks -O --mk_rec_dim TIME $i $i"
		    #echo $cmd0
		    $cmd0
		done
	    fi

	    if [ $ncrcat_flag = 1 ]; then
		# ncrcat
		if  [ $k = 'SSH' ]; then
		    ncrcat $listf -o ${k}.1440x720.${yy}${month2}.nc
		else
		    ncrcat $listf -o ${k}.1440x720x50.${yy}${month2}.nc
		fi
	    fi
	   cd -
	done
    done
done
