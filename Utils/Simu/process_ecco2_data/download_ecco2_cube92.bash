#!/bin/bash

set -x

login=gcambon
passwd=aFHryrKMxOcebHfQ8QVY
ecco2_url=https://ecco.jpl.nasa.gov/drive/files/ECCO2/cube92_latlon_quart_90S90N


var=(\
     THETA \
     SALT  \
     SSH   \
     UVEL  \
     VVEL)

# var=(\
#      SALT  \
#      SSH   \
#      UVEL  \
#      VVEL)

#var=(\
#     SSH)

yeardeb=2016
yearend=2016
monthdeb=9
monthend=11

dirout=/local/tmp/2/gcambon/ECCO2_cube92_global

#========
cd $dirout


for k in ${var[*]} ; do 
    for yy in `seq ${yeardeb} ${yearend}` ; do
	[ -d $yy ] && mkdir $yy
	#for month in `seq ${monthdeb} ${monthend}` ; do
	for month in ${monthdeb} ${monthend} ; do
	    month2=`printf "%02d" $month`
	    [ -d $yy/$month2 ] && mkdir -p $yy/$month2

	    cd $yy/$month2

	    for day in `seq $1 31` ; do

		day2=`printf "%02d" $day`
		echo "===> Process var $k"
		if [ $k != 'SSH' ]; then
		    CMD="curl -O -u $login:$passwd $ecco2_url/${k}.nc/${k}.1440x720x50.${yy}${month2}${day2}.nc"
		else
		    CMD="curl -O -u $login:$passwd $ecco2_url/${k}.nc/${k}.1440x720.${yy}${month2}${day2}.nc"
		fi
		    echo $CMD
		$CMD
		echo "..."
		echo " "	

	    done
	    cd -
	done
    done
done

cd -
