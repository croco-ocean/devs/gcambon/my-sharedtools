#!/bin/bash

set -x


var=(\
    THETA \
    SALT  \
    SSH	  \
    UVEL  \
    VVEL)

for i in `ls -1 *.nc` ; do
    #ss=`du -ms $i |  tr -s [::] | cut -d' ' -f1-1`
    ss=`du -ms $i | cut -f1-1`
    if [ $ss -lt 100 ] ; then
	cmd="rm -f $i"
	$cmd
    fi
done
