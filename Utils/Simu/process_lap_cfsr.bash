#!/bin/bash

#
# Script to process the overlapping of the frc
# and blk cfsr file
#
# Feb 2019 : G. Cambon gildas.cambon@ird.fr

######################################################################
set -x
#
suffixNC='.1'

[ ! -d LAB ] && mkdir LAB
cp -Rf No_Overlap/*${suffixNC} LAB/

# for frc file
cd LAB/

for i in 4 5 6 7 8 9 ; do
    echo $i
    # Extract the stress var 
    ncks -v sustr,svstr croco_frc_CFSR_Y200${i}.nc${suffixNC} -o stress_croco_frc_CFSR_Y200${i}.nc${suffixNC}

    # Make sms_time the rec dim
    ncks -O --mk_rec_dmn sms_time stress_croco_frc_CFSR_Y200${i}.nc${suffixNC} stress_croco_frc_CFSR_Y200${i}.nc${suffixNC}

    # Extract the first and last record
    ncks -F -d sms_time,1 stress_croco_frc_CFSR_Y200${i}.nc${suffixNC} -o beg_stress_croco_frc_CFSR_Y200${i}.nc${suffixNC}
    ncks -F -d sms_time,-1 stress_croco_frc_CFSR_Y200${i}.nc${suffixNC} -o end_stress_croco_frc_CFSR_Y200${i}.nc${suffixNC}
done

for i in 5 6 7 8 ; do
    # Do the concatenation
    echo $i 
    ncrcat end_stress_croco_frc_CFSR_Y200$(($i - 1)).nc${suffixNC} stress_croco_frc_CFSR_Y200${i}.nc${suffixNC} beg_stress_croco_frc_CFSR_Y200$((i +1)).nc${suffixNC} -o lap_stress_croco_frc_CFSR_Y200${i}.nc${suffixNC}
    mv lap_stress_croco_frc_CFSR_Y200${i}.nc${suffixNC} ../
done
rm -Rf beg_stress*
rm -Rf end_stress*

cd -

#===================================
## for bulk file
cd LAB/

for i in 4 5 6 7 8 9 ; do
    echo $i
    # Extract the first and last record
    ncks -F -d bulk_time,1 croco_blk_CFSR_Y200${i}.nc${suffixNC} beg_croco_blk_CFSR_Y200${i}.nc${suffixNC}
    ncks -F -d bulk_time,-1 croco_blk_CFSR_Y200${i}.nc${suffixNC} end_croco_blk_CFSR_Y200${i}.nc${suffixNC}
done

for i in 5 6 7 8 ; do
    # Do the concatenation
    echo $i
    ncrcat end_croco_blk_CFSR_Y200$(($i - 1)).nc${suffixNC} croco_blk_CFSR_Y200${i}.nc${suffixNC} beg_croco_blk_CFSR_Y200$((i +1)).nc${suffixNC} -o lap_croco_blk_CFSR_Y200${i}.nc${suffixNC}
    mv lap_croco_blk_CFSR_Y200${i}.nc${suffixNC} ../ 
done
rm -Rf beg_croco_blk*
rm -Rf end_croco_blk*
cd -
#===================================
