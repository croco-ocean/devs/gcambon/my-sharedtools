#!/bin/bash
cp  roms_bengsafeR1K_srtopo_grd.nc roms_grd.nc
##srtopo 1.0 /net/leo/local/tmp/1/herbette/SRTM/srtm30_plus/srtm30/grd roms_grd.nc
gshhs_to_roms_mask roms_grd.nc
copymask mask.nc roms_grd.nc
#rmask 2 roms_grd.nc
single_connect -s 100 100 roms_grd.nc
commit_rmask roms_grd.nc
#srtopo 4 /local/tmp/1/herbette/SRTM/srtm30_plus/srtm30/grd roms_grd.nc
#lsmooth 10 8000 0.25 roms_grd.nc
mv roms_grd.nc roms_bengsafeR1K_srtopo_grd.nc
