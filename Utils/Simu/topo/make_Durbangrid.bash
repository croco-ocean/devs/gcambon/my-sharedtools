#!/bin/bash

cp croco_grd.nc_etopomakegrid croco_grd.nc
srtopo 1.0 /net/leo/local/tmp/1/herbette/SRTM/srtm30_plus/srtm30/grd croco_grd.nc
gshhs_to_roms_mask croco_grd.nc
copymask mask.nc croco_grd.nc
single_connect -s 100 100 croco_grd.nc
commit_rmask croco_grd.nc
srtopo 4 /net/leo/local/tmp/1/herbette/SRTM/srtm30_plus/srtm30/grd croco_grd.nc
lsmooth 10 8000 0.25 croco_grd.nc
