clear all
close all

DIRDATA='/net/centaure/local/tmp/1/gcambon/BUIC/BENGSAFE_R2KM/'
grdname=[DIRDATA,'sbay_grd.nc'];

nc=netcdf(grdname,'r');
Latr=nc{'lat_rho'}(:);
Lonr=nc{'lon_rho'}(:);
Latp=nc{'lat_psi'}(:);
Lonp=nc{'lon_psi'}(:);
close(nc)

[Lonu,Lonv,Lonp]=rho2uvp(Lonr); 
[Latu,Latv,Latp]=rho2uvp(Latr);
[M,L]=size(Latp);
Lp=L+1;
Mp=M+1;

nw = netcdf(grdname, 'write');
nw('xi_u') = L;
nw('eta_u') = Mp;
nw('xi_v') = Lp;
nw('eta_v') = M;
nw('two') = 2;
nw('four') = 4;
nw('bath') = 1;
%
nw{'xl'} = ncdouble('one');
nw{'xl'}.long_name = ncchar('domain length in the XI-direction');
nw{'xl'}.long_name = 'domain length in the XI-direction';
nw{'xl'}.units = ncchar('meter');
nw{'xl'}.units = 'meter';
%
nw{'el'} = ncdouble('one');
nw{'el'}.long_name = ncchar('domain length in the ETA-direction');
nw{'el'}.long_name = 'domain length in the ETA-direction';
nw{'el'}.units = ncchar('meter');
nw{'el'}.units = 'meter';
%
nw{'lon_u'} = ncdouble('eta_u', 'xi_u');
nw{'lon_u'}.long_name = ncchar('longitude of U-points');
nw{'lon_u'}.long_name = 'longitude of U-points';
nw{'lon_u'}.units = ncchar('degree_east');
nw{'lon_u'}.units = 'degree_east';
%
nw{'lon_v'} = ncdouble('eta_v', 'xi_v');
nw{'lon_v'}.long_name = ncchar('longitude of V-points');
nw{'lon_v'}.long_name = 'longitude of V-points';
nw{'lon_v'}.units = ncchar('degree_east');
nw{'lon_v'}.units = 'degree_east';
%
nw{'lat_u'} = ncdouble('eta_u', 'xi_u');
nw{'lat_u'}.long_name = ncchar('latitude of U-points');
nw{'lat_u'}.long_name = 'latitude of U-points';
nw{'lat_u'}.units = ncchar('degree_north');
nw{'lat_u'}.units = 'degree_north';
%
nw{'lat_v'} = ncdouble('eta_v', 'xi_v');
nw{'lat_v'}.long_name = ncchar('latitude of V-points');
nw{'lat_v'}.long_name = 'latitude of V-points';
nw{'lat_v'}.units = ncchar('degree_north');
nw{'lat_v'}.units = 'degree_north';
%
nw{'mask_u'} = ncdouble('eta_u', 'xi_u');
nw{'mask_u'}.long_name = ncchar('mask on U-points');
nw{'mask_u'}.long_name = 'mask on U-points';
nw{'mask_u'}.option_0 = ncchar('land');
nw{'mask_u'}.option_0 = 'land';
nw{'mask_u'}.option_1 = ncchar('water');
nw{'mask_u'}.option_1 = 'water';
%
nw{'mask_v'} = ncdouble('eta_v', 'xi_v');
nw{'mask_v'}.long_name = ncchar('mask on V-points');
nw{'mask_v'}.long_name = 'mask on V-points';
nw{'mask_v'}.option_0 = ncchar('land');
nw{'mask_v'}.option_0 = 'land';
nw{'mask_v'}.option_1 = ncchar('water');
nw{'mask_v'}.option_1 = 'water';
%
nc{'lat_u'}(:)=Latu;
nc{'lon_u'}(:)=Lonu;
nc{'lat_v'}(:)=Latv;
nc{'lon_v'}(:)=Lonv;
close(nw);

%
%--
%
nw = netcdf(grdname, 'write');
nc{'lat_u'}(:)=Latu;
nc{'lon_u'}(:)=Lonu;
nc{'lat_v'}(:)=Latv;
nc{'lon_v'}(:)=Lonv;
close(nw)

