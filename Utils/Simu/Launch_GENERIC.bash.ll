#!/bin/bash
# Nom du travail LoadLeveler
# @ job_name=JOBNAME
# Fichier de sortie standard du travail
# @ output  = $(job_name).$(jobid)
# Fichier de sortie d'erreur du travail
# @ error   = $(job_name).$(jobid)
# Type de travail
# @ job_type= serial
# @ requirements = (Feature == "prepost")
# Temps ELAPSED max. pour l'ensemble du job en hh:mm:ss (10mn ici)
# @ wall_clock_limit = 20:00:00
# @ notify_user = gildas.cambon@ird.fr
# @ notification = always
# @ as_limit = 20.0gb
# @ queue
######################################################################
cd $LOADL_STEP_INITDIR
# =============================
# ./MYSCRIPT.bash or MY COMMAND
clim -t=scrum_time roms_avg_M_*.nc
# =============================
echo '  '
echo ' Great Job done '
echo ' ============= '
