#!/bin/bash

# Script to download simulation from ADA
source ./configure_rapat 

if [ ! -d $DIRLOCAL ]; then 
  echo 'First need to create the local dir'
else
  echo "scp -r ${MACHINE}:${DIRREMOTE}/${filename_roms}.nc $DIRLOCAL"
  scp -r ${MACHINE}:${DIRREMOTE}/${filename_roms} $DIRLOCAL
fi
