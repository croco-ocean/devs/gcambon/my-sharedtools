#!/bin/sh
   
# Rapatriement de donnees depuis Ada IDRIS vers spica 
RapatfromADA()
{
    echo '=================================================================='
    echo 'Local dir. = '$2
    echo 'Ada dir. = /workgpfs/rech/khe/rkhe002/OUTPUT/BENGSAFE_R4/'$1
    echo '=================================================================='
    rsync -avz --progress --partial rkhe002@ada.idris.fr:/workgpfs/rech/khe/rkhe002/OUTPUT/BENGSAFE_R4/$1/\*avg\*.nc $2
}
#===================================
RapatfromADA $1 $2

