#!/bin/bash

#Script to upload datasets on $WORK on jean-zay
OUTDIR='/gpfswork/rech/khe/commun/INPUT/CYCLOPS_R3KM/OCEAN/GLORYS12/'
INDIR='/local/tmp/1/sasdar/CONFIGS/CYCLOPS/INPUT/CROCO_FILES/'
RSYNC_CMD='rsync -avz --progress'
#===================================
set -e

ydeb=2013
yend=2018

cd $INDIR

for y in `seq $ydeb $yend`; do
    echo 'Process year '$y
    listf=`ls -1 *${y}*.nc`
    for i in $listf ; do
	$RSYNC_CMD $INDIR/$i jza_d:$OUTDIR
	#$CMD
    done
done

cd - 
