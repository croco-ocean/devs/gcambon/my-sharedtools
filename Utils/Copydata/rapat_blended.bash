#!/bin/bash

#set -x
#set -e

region=$1

if [[ $region == 'benguela' ]]; then
#  benguela
    ymin=2000
    ymax=2017
    dir_blended="Blended4Benguela/Expl/"
    dir_in="Blended4Benguela"
fi

if [[ $region == 'canaria' ]]; then
    #  canary
    ymin=2000
    ymax=2017
    dir_blended="Blended4Canaria/Expl/"
    dir_in="Blended4Canaria"
fi
#===============================
for yy in `seq $ymin $ymax` ; do 
    for mm0 in `seq 1 12`; do 
	mm=`printf "%02d" $mm0`
	#echo $mm
	for dd0 in `seq 1 31` ; do 
	    dd=`printf "%02d" $dd0`
	    #echo $dd 
	    CMD="rsync -urlpogtv /home/cercache/users/ben/${dir_blended}/${yy}/${mm}/${dd}/*.nc ${dir_in}/${yy}/${mm}/${dd}/"
	    echo ' '
	    echo $CMD
	    $CMD
	done 
    done
done

echo "Good job "
