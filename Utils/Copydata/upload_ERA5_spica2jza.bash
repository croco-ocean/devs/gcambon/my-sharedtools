#!/bin/bash

#Script to upload datasets on $STORE on jean-zay
OUTDIR='/gpfsstore/rech/khe/commun/INPUT/ATMO/ERA5/CYCLOPS_EXT/'
INDIR='/local/tmp/1/sasdar/DATA/ERA5/'
RSYNC_CMD='rsync -avz --progress'
#===================================

cd $INDIR
listf=`ls -1 *.nc`

for i in $listf ; do
    
    $RSYNC_CMD $INDIR/$i jza_d:$OUTDIR
    #$CMD
    
done
cd - 
