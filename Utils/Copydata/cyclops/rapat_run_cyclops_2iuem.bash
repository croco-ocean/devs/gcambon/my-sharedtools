#!/bin/bash

#set -x
set -e
# Scripts to copy outfiles from HOAIN simulations
idris_simu='/gpfswork/rech/khe/rkhe002/forwork_khe/OUTPUT/CYCLOPS_R3KM'
iuem_dir='/data0/project/benguela/CYCLOPS/OUTPUT/CYC3KM'
#
ymin=2003
ymax=2007
#
# create subdir
lengthsimu=${#idris_simu[@]}

for kk in `seq 0 $(( ${lengthsimu} - 1))` ; do
    simu=${idris_simu[$kk]}
    iuemdd=${iuem_dir[$kk]}
    echo "simu is " ${simu}
    echo "iuemdd is " ${iuem_dir}

    [ ! -d $iuem_dir ] && mkdir -p $iuem_dir
    
    # rsync results
    for yy in `seq ${ymin} ${ymax}` ; do 
	for mm in `seq 1 12`; do
	    file=croco_avg_Y${yy}M${mm}.nc
	    echo "Download file $file"
	    CMD="rsync -avz --progress jza_d:$idris_dir/$simu/$file $iuemdd"
	    $CMD
	done
    done
 
done
