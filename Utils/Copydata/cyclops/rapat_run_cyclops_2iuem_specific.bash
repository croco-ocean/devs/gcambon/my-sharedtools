#!/bin/bash

#set -x
set -e
# Scripts to copy outfiles from HOAIN simulations
idris_simu='/gpfswork/rech/khe/rkhe002/forwork_khe/OUTPUT/CYCLOPS_R3KM/AVG/'
iuem_dir='/data0/project/benguela/CYCLOPS/OUTPUT/CYC3KM'
#
#
# create subdir
lengthsimu=${#idris_simu[@]}

for kk in `seq 0 $(( ${lengthsimu} - 1))` ; do
    simu=${idris_simu[$kk]}
    iuemdd=${iuem_dir[$kk]}
    echo "simu is " ${simu}
    echo "iuemdd is " ${iuem_dir}
    
    [ ! -d $iuem_dir ] && mkdir -p $iuem_dir
    
    filelist='croco_avg_Y2015M12.nc croco_avg_Y2016M1.nc
	      croco_avg_Y2016M2.nc croco_avg_Y2015M3.nc
	      croco_avg_Y2016M4.nc croco_avg_Y2016M5.nc croco_avg_Y2016M6.nc'
    
    # rsync results
    for ff in $filelist; do
	echo "Download file $ff"
	CMD="rsync -avz --progress jza_d:${simu}/${ff} $iuemdd"
	$CMD
    done
done
