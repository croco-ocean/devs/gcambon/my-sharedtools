#!/bin/bash

for i in `seq 1 12` ; do 
	ii=`printf "%02d" $i` 
	echo $ii
	file="/mnt/transfert/NC/GLORYS_12V1_SAFE_0${ii}*.nc"
	CMD="rsync -a --progress $file ."
	#echo $CMD
	$CMD
done

