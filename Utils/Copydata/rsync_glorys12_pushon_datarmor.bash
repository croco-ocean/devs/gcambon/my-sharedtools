#!/bin/bash

repin="./"
#repin="/mnt/transfert/NC/"

repdatarmor="dat_d://home/datawork-lops-siam-moawi/DATAREF/GLORYS12/"

for i in `seq 1 10` ; do 
	ii=`printf "%02d" $i` 
	echo $ii
	filein="${repin}GLORYS_12V1_SAFE_0${ii}*.nc"
	echo "Process $filein"
	CMD="rsync -a --progress $filein $repdatarmor"
	#echo $CMD
	$CMD
done

