#!/bin/bash

set -x 

repHERE="."
repLEO="gcambon@leo.univ-brest.fr:/local/tmp/1/gcambon/Ocean_Reanalysis_Glorys12/"

for i in `seq 1 10` ; do 
	ii=`printf "%02d" $i` 
	echo $ii
	filein="GLORYS_12V1_SAFE_0${ii}*.nc"
	echo "Process $filein"
	CMD="rsync -a --progress $repLEO/$filein $repHERE"
	#echo $CMD
	$CMD
done

