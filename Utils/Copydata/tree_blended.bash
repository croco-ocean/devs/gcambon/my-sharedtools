#!/bin/bash

#set -x

for yy in `seq 2000 2017` ; do 
    mkdir $yy
    for mm0 in `seq 1 12`; do 
	mm=`printf "%02d" $mm0`
	echo $mm
	mkdir ${yy}/${mm}
	for dd0 in `seq 1 31` ; do 
	    dd=`printf "%02d" $dd0` ; echo $dd 
	    mkdir ${yy}/${mm}/${dd} 
	done 
    done
done

# remove 31 days months

for yy in `seq 2000 2017` ; do
    for mm in 02 04 06 09 11; do 
	for dd in 31 ; do 
	    rmdir ${yy}/${mm}/${dd}
	done 
    done 
done

# rmove 29 end 30 days months
for yy in 2001 2002 2003 2005 2006 2007 2009 2010 2011 2013 2014 2015 2017; do 
    for mm in 02 ; do 
	for dd in 29 ; do 
	    rmdir ${yy}/${mm}/${dd} 
	done 
    done
done

