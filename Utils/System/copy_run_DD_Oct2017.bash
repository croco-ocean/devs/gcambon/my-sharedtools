#/bin/bash

# Copy des simulations R15, R3KM et R1KM

DD75=/mnt/ddgil1/BENGSAFE_R15
copy75km=0

#DD3KM=/Volumes/DDGIL5
DD3KM=/mnt/ddgil5/BENGSAFE_R3KM
# => DD1 1.8 Tb libre
copy3km=0

#-DD1KM=/Volumes/DDGIL4
DD1KM=/mnt/ddgil4/BENGSAFE_R1KM
# => DD1 2.0 Tb libre
copy1km=0


#===========================================================
DDOCEAN1=/mnt/ddgil2/DATAGIL/
# => DD1 2.0 Tb libre
copyocean1=1

DDOCEAN2=/mnt/ddgil2/DATAGIL/
# => DD1 2.0 Tb libre
copyocean2=1


REP_75='gcambon@spica.univ-brest.fr:/net/centaure/local/tmp/1/gcambon/BUIC/OUTPUT/BENGSAFE_R15/blkperio_R15_CFSR_t2_EXTRACTED/RUN1989-2010/roms_avg_*.nc'

REP_3KM='gcambon@spica.univ-brest.fr:/net/centaure/local/tmp/1/gcambon/BUIC/OUTPUT/BENGSAFE_R3KM/EXTRACTED_GEOG_VAR/AVG/roms_avg_*.nc'

REP_1KM='gcambon@spica.univ-brest.fr:/local/tmp/3/gcambon/BUIC_spica/OUTPUT/BENGSAFE_R1KM_r2r_V3/roms_avg_*.nc'

REP_OCEAN1='gcambon@spica.univ-brest.fr:/local/tmp/1/gcambon/DATA_OBS/DATA_OCEAN'
REP_OCEAN2='gcambon@spica.univ-brest.fr:/local/tmp/1/gcambon/DATA_OBS/DATASETS_ROMS'

#======
if [ ${copy75km} == 1 ] ; then
echo 'Start copy 7.5 km ...'
   scp -r ${REP_75} ${DD75}
echo 'Copy 7.5 km done'
fi
#======	     
if [ ${copy3km} == 1 ] ; then
echo 'Start copy 3 km ...'
scp -r ${REP_3KM} ${DD3KM}
echo 'Copy 3 km done'	
fi
#======
if [ $copy1km == 1 ] ; then
echo 'Start copy 1 km ...'
    scp -r ${REP_1KM} ${DD1KM}
echo 'Copy 1 km done'	      
fi

#======
if [ $copyocean1 == 1 ] ; then
echo 'Start copy DATA_OCEAN ...'
    scp -r ${REP_OCEAN1} ${DDOCEAN1}
echo 'Copy DATA_OCEAN done'	      
fi
#======
if [ $copyocean2 == 1 ] ; then
echo 'Start copy DATASETS_ROMS ...'
    scp -r ${REP_OCEAN2} ${DDOCEAN2}
echo 'Copy DATASETS_ROMS done'	      
fi
