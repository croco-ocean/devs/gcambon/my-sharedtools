#!/usr/bin/env python
# encoding: utf-8

help_string="""
  SYNTAXE:
      search_replace.py  "racine_fichiers" "regexp_fichiers_a_chercher" "look_for" "replace_by"

  EXAMPLE:
     search_replace.py  /home/nino  ".*\.py$"  "python2.2"  "python2.3"

  il fait toujours un backup dans chaque fichier chang� ... .old,
  sauf s'il existe d�j�  (de fa�on � ne pas �craser la sauvegarde initiale
  si le programme est lanc� plusieurs fois).

  Attention, utiliser des guillemets pour les expression reguli�res pour �viter leur
  interpr�tation par le shell.
"""

import sys, os, re

def lister(arg, dirName, filesInDir):              # called at each dir
    print '[' + dirName + ']'
    (fileRegexp, lookFor, replaceBy) = arg
    for fname in filesInDir:                         # includes subdir names
        path = os.path.join(dirName, fname)          # add dir name prefix
        if not os.path.isdir(path):                  # print simple files only
            if fileRegexp.match(fname):
                traiter(path, lookFor, replaceBy)

def compileRe(regexp):
    try:
        comp=re.compile(regexp)
        return comp
    except:
        print "Erreur en compilant %s " % regexp
        sys.exit(1)

def traiter(pathin, lookFor, replaceBy):
    pathbak="%s.srbak" % pathin
    frombak=False
    msg=[]
    if not os.path.exists(pathbak):
        msg.append("    rename %s -> %s " % (pathin, pathbak))
        os.rename(pathin, pathbak)
    else:
        msg.append("    using old %s " % pathbak)
        frombak=True
    input=open(pathbak)
    output=open(pathin,'w')
    same=True
    for s in input.xreadlines():
        snew=lookFor.sub(replaceBy, s)
        if (same and (s != snew)):
            same=False
        output.write(snew)
    output.close()
    input.close()

    if not same:
        print "+++ Fichier %s modifie" % pathin
        for m in msg:
            print m
    else:            # S'il n'a pas chang�, on le laisse � l'�tat initial (sauf si c'est le backup)
        if not frombak:
            os.unlink(pathin)
            os.rename(pathbak, pathin)


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print  "\n  ERREUR: %d arguments trouv�s" % len(sys.argv)
        print help_string
        sys.exit(1)
    else:
        root=sys.argv[1]
        fileRegexp=compileRe(sys.argv[2])
        lookFor=compileRe(sys.argv[3])
        arg=(fileRegexp,lookFor,sys.argv[4])  # regexp, lookFor, replaceBy
        os.path.walk(root, lister, arg)          # dir name in cmdline
