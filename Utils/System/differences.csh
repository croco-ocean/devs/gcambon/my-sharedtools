#!/bin/csh
set mydir=`pwd`
#set dir1=/home/penven/CDROM/Roms_tools/Compile
#set dir2=/mnt/windows/Data/Roms_tools/Compile1
#set dir1=/mnt/windows/Data/Roms_tools/Run_2way/
#set dir2=/home/penven/Roms_tools/Roms_Agrif
#set dir1=/home/ppenven/Roms_tools/Roms_Agrif_2WAY
#set dir2=/home/ppenven/Roms_tools/Roms_Agrif
#set dir2=/home/ppenven/Modeling_general/tmp/Eddy_detection/SOURCES
#set dir1=/home/ppenven/Modeling_general/tmp/Eddy_detect_2012_02_05/SOURCES
set dir2=/home/ppenven/romsagrif/Roms_tools/Diagnostic_tools/Eddy_detect_2013_04_15/SOURCES
set dir1=/home/ppenven/romsagrif/Roms_tools/Diagnostic_tools/eddydetect-code/SOURCES
cd $dir1
foreach f (`ls *.[m] `)
#foreach f (`ls *.f `)
  echo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  echo %
  echo %  testing: $f
  echo %
  echo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if (`diff $f $dir2/$f | wc -l` != 0) then
    echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    echo $f
    echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    diff $f $dir2/$f
  endif
  echo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  echo %
  echo %  end: $f
  echo %
  echo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
cd $mydir
