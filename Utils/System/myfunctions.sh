#!/bin/sh
   
# Rapatriement de donnees depuis Ada IDRIS vers spica 
RapatAda()
{
    dirloc  = $1 
    dirdist = $2
    rsync -avz --progress rkhe002@ada.idris.fr:/workgpfs/rech/khe/rkhe002/OUTPUT/BENGSAFE_R4/Y2000/$1 $2
}
