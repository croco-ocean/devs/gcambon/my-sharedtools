#ifndef KIND
# define KIND 4
c--# define VERBOSE

      subroutine comp_quintic_real( nspc, nrecs, time, f, d1, d2, spv)
#else
      subroutine comp_quintic_double(nspc,nrecs, time, f, d1, d2, spv)
#endif
      implicit none
      integer nspc, nrecs
      real(kind=8) time(nrecs)
      real(kind=KIND), dimension(nspc,nrecs) :: f, d1,d2
      real(kind=KIND) spv
      logical landmask

      real(kind=8), dimension(nrecs) :: c11,c12,c21,c22
      real(kind=8) b11,b12,b21,b22, det, e1,e2, a11,a12,a21,a22
      real(kind=8), parameter :: f16r15=16.D0/15.D0,  f7r15=7.D0/15.D0,
     &         f1r15=1.D0/15.D0, f1r20=0.05D0, f3r10=0.3D0, f2r5=0.4D0,
     &         f2r3=2.D0/3.D0,   f5r3=5.D0/3.D0,   f10r3=10.D0/3.D0,
     &         f7r8=7.D0/8.D0,   f15r8=15.D0/8.D0, f1r3=1.D0/3.D0,
     &                           f20r3=20.D0/3.D0

      integer numthreads,trd, nchunks, chunk_step,chunk_size,
     &              first_ch,last_ch, ichunk, imin,imax, i,j
C$    integer omp_get_num_threads, omp_get_thread_num

      numthreads=1 ; trd=0
C$    numthreads=omp_get_num_threads()
C$    trd=omp_get_thread_num()

      nchunks=max(1,nspc/160)
      i=mod(nchunks,numthreads)
      if (i>0) nchunks=nchunks+numthreads-i
      chunk_step=nchunks/numthreads
      first_ch=trd*chunk_step
      last_ch=first_ch+chunk_step-1

      chunk_size=(nspc + nchunks-1)/nchunks

#ifdef VERBOSE
      write(*,'(1x,A,I6,5(1x,A,I4))')  'comp_quintic :: nspc =', nspc,
     &            'chunk_size =',  chunk_size,    'nchunks =', nchunks,
     &            'trd =', trd, 'first =', first_ch, 'last =', last_ch
#endif


#if KIND == 8
      if (spv == 0.0_8) then
#else
      if (spv == 0.0_4) then
#endif
        landmask=.false.
      else
        landmask=.true.
      endif

      do ichunk=first_ch,last_ch
        imin=ichunk*chunk_size +1
        imax=min(imin+chunk_size -1, nspc)
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

#define THIRD_AND_FOURTH

#ifdef FIRST_AND_SECOND
        c11(1)=0.D0 ; c12(1)=0.D0
        c21(1)=0.D0 ; c22(1)=0.D0
        do i=imin,imax
          d1(i,1)=0.D0
          d2(i,1)=0.D0
        enddo
#elif defined FIRST_AND_THIRD
        c11(1)=0.D0 ; c12(1)=0.D0
        c21(1)=0.D0 ; c22(1)=-f1r3
        do i=imin,imax
          d1(i,1)=0.D0
          d2(i,1)=f20r3*(f(i,2)-f(i,1))
        enddo
#elif defined SECOND_AND_THIRD
        c11(1)=f2r3 ; c12(1)=0.D0
        c21(1)=0.D0 ; c22(1)=0.D0
        do i=imin,imax
          d1(i,1)=f5r3*(f(i,2)-f(i,1))
          d2(i,1)=0.D0
        enddo
#elif defined SECOND_AND_FOURTH
        c11(1)=f7r8 ; c12(1)=0.D0
        c21(1)=0.D0 ; c22(1)=0.D0
        do i=imin,imax
          d1(i,1)=f15r8*(f(i,2)-f(i,1))
          d2(i,1)=0.D0
        enddo
#elif defined THIRD_AND_FOURTH
        c11(1)=1.5D0  ; c12(1)=-0.25D0
        c21(1)=-f10r3 ; c22(1)=f2r3
        do i=imin,imax
          d1(i,1)= 2.5D0*(f(i,2)-f(i,1))
          d2(i,1)=-f10r3*(f(i,2)-f(i,1))
        enddo
#endif

        do j=2,nrecs-1
          b11= f16r15 -f7r15*c11(j-1) -f1r15*c21(j-1)
          b12=        -f7r15*c12(j-1) -f1r15*c22(j-1)
          b21=          f2r5*c11(j-1) +f1r20*c21(j-1)
          b22=  f3r10  +f2r5*c12(j-1) +f1r20*c22(j-1)

          det=1.D0/(b11*b22-b21*b12)

          c11(j)=det*( f7r15*b22 -f2r5 *b12 )
          c12(j)=det*(-f1r15*b22 +f1r20*b12 )
          c21(j)=det*(-f7r15*b21 +f2r5 *b11 )
          c22(j)=det*( f1r15*b21 -f1r20*b11 )

          do i=imin,imax
            e1=f(i,j+1)-f(i,j-1) -f7r15*d1(i,j-1) -f1r15*d2(i,j-1)
            e2=f(i,j+1)+f(i,j-1)-2.D0*f(i,j)
     &                            +f2r5*d1(i,j-1) +f1r20*d2(i,j-1)

            d1(i,j)=det*( b22*e1 -b12*e2 )
            d2(i,j)=det*(-b21*e1 +b11*e2 )
          enddo
        enddo

#ifdef FIRST_AND_SECOND
        a11=0.D0 ; a12=0.D0
        a21=0.D0 ; a22=0.D0
        do i=imin,imax
          d1(i,nrecs)=0.D0
          d2(i,nrecs)=0.D0
        enddo
#elif defined  FIRST_AND_THIRD
        a11=0.D0 ; a12=0.D0
        a21=0.D0 ; a22=-f1r3
        do i=imin,imax
          d1(i,nrecs)=0.D0
          d2(i,nrecs)=-f20r3*(f(i,nrecs)-f(i,nrecs-1))
        enddo
#elif defined SECOND_AND_THIRD
        a11=f2r3 ; a12=0.D0
        a21=0.D0 ; a22=0.D0
        do i=imin,imax
          d1(i,nrecs)=f5r3*(f(i,nrecs)-f(i,nrecs-1))
          d2(i,nrecs)=0.D0
        enddo
#elif defined SECOND_AND_FOURTH
        a11=f7r8 ; a12=0.D0
        a21=0.D0 ; a22=0.D0
        do i=imin,imax
          d1(i,nrecs)=f15r8*(f(i,nrecs)-f(i,nrecs-1))
          d2(i,nrecs)=0.D0
        enddo
#elif defined THIRD_AND_FOURTH
        a11=1.5D0 ; a12=0.25D0
        a21=f10r3 ; a22=f2r3
        do i=imin,imax
          d1(i,nrecs)=2.5D0*(f(i,nrecs)-f(i,nrecs-1))
          d2(i,nrecs)=f10r3*(f(i,nrecs)-f(i,nrecs-1))
        enddo
#endif

        b11=1.D0 -a11*c11(nrecs-1) -a12*c21(nrecs-1)
        b12=     -a11*c12(nrecs-1) -a12*c22(nrecs-1)
        b21=     -a21*c11(nrecs-1) -a22*c21(nrecs-1)
        b22=1.D0 -a21*c12(nrecs-1) -a22*c22(nrecs-1)

        det=1.D0/(b11*b22-b21*b12)

        do i=imin,imax
          e1=d1(i,nrecs) -a11*d1(i,nrecs-1) -a12*d2(i,nrecs-1)
          e2=d2(i,nrecs) -a21*d1(i,nrecs-1) -a22*d2(i,nrecs-1)

          d1(i,nrecs)=det*( b22*e1 -b12*e2 )
          d2(i,nrecs)=det*(-b21*e1 +b11*e2 )
        enddo
        if (landmask) then
          do i=imin,imax
            if (f(i,nrecs) == spv) then
              d1(i,nrecs)=spv
              d2(i,nrecs)=spv
            endif
          enddo
        endif

        do j=nrecs-1,1,-1
          do i=imin,imax
            d1(i,j)=d1(i,j) -c11(j)*d1(i,j+1) -c12(j)*d2(i,j+1)
            d2(i,j)=d2(i,j) -c21(j)*d1(i,j+1) -c22(j)*d2(i,j+1)
          enddo
          if (landmask) then
            do i=imin,imax
              if (f(i,j) == spv) then
                d1(i,j)=spv
                d2(i,j)=spv
              endif
            enddo
          endif
        enddo
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      enddo  !<-- ichunk
      end
#if KIND == 4
# undef KIND
# define KIND 8
# include "comp_quintic.F"
#endif
