/* This is a generic command-line tool to remove undesired attribute for
  a user-specified variable in netCDF file.   It receives three arguments
  -- name of netCDF file, variable name, and name of the attribute to be
  removed.   It attempts to open the file, finds variable with given name,
  and checks whether the specified attribute exists.  If so, it switches
  the file into redefinition mode and deletes the attribute.   Basically
  this is merely a shell around netCDF fiction "nc_del_att" which is
  available only in C-version of netCDF library, but not in Fortran.

  Created and maintained by Alexander Shchepetkin, old_galaxy@yahoo.com
*/

#include <stdio.h>
#include <stdlib.h>
#include <netcdf.h>

int main(int argc, char **argv){
    int iarg, ierr, ncid, varid;
    nc_type xtype;
    size_t lngth ;
/*
  Note that input arguments in C are numbered from 0 to N, where 0-th
  argument is the name of executable command itself, so variable name is
  expected to be argv[1], file name argv[3], and attribute name argv[2].
*/

/*
 *  fprintf(stdout, "argc = %d\n", argc);
 *  for(iarg=0 ; iarg < argc ; iarg++){
 *    fprintf(stdout, "argv[%d] = %s\n", iarg, argv[iarg]);
 *  }
 */

/* Print instruction how to use and quit, if there are not enough arguments. */

  if (argc < 4) {
    fprintf(stdout,"\n Command-line operator nc_del_att is to delete an undesired attribute\n");
    fprintf(stdout," of user-specified variable within netCDF file. Its usage should be:\n");
    fprintf(stdout,"\n             nc_del_att varname attname file.nc [file2.nc file3.nc ...]\n\n");
    fprintf(stdout," use 'global' or 'nc_global' or 'nf_global' instead variable name to delete\n");
    fprintf(stdout," global attribute.\n\n");
    exit(-1);
  } 

  for(iarg=3 ; iarg < argc ; iarg++){
    ierr=nc_open(argv[iarg], NC_WRITE, &ncid);
    if (ierr == NC_NOERR) {
      fprintf(stdout, "open netCDF file %s for writing, ncid = %d\n",
                                                   argv[iarg], ncid);
      if ( strcmp(argv[1],"global") == 0 || strcmp(argv[1],"nc_global") == 0
                                       || strcmp(argv[1],"nf_global") == 0 ) {
        fprintf(stdout, "interpreting '%s' as global attribute.\n",  argv[1]);
        varid=NC_GLOBAL;
      } else {
        ierr=nc_inq_varid(ncid, argv[1], &varid);
        if (ierr == NC_NOERR) {
          fprintf(stdout, "found variable '%s' varid = %d\n", argv[1], varid);
        }
      }

      if (ierr == NC_NOERR) {
        ierr=nc_inq_att(ncid, varid, argv[2], &xtype, &lngth);
        if (ierr == NC_NOERR) {
          fprintf(stdout, "found attribute '%s' type = %d length = %d\n",
                                                  argv[2], xtype, lngth);
          ierr=nc_redef(ncid); 
          if (ierr == NC_NOERR) {
            fprintf(stdout, "switched into redefinition mode\n");
            ierr=nc_del_att(ncid, varid, argv[2]);
            if (ierr == NC_NOERR) {
              fprintf(stdout, "removed attribute '%s'\n", argv[2]);
              ierr=nc_enddef(ncid);    
              if (ierr == NC_NOERR) {
                fprintf(stdout, "left definition mode\n");
                ierr=nc_close(ncid);
                if (ierr == NC_NOERR) {
                  fprintf(stdout, "closed file\n");
                } else {
                  fprintf(stdout, "\n ### ERROR: Cannot close netCDF file: %s.\n\n",
                                                                 nc_strerror(ierr));
                }
              } else {
                fprintf(stdout, "\n ### ERROR: Cannot leave definition: %s.\n\n",
                                                              nc_strerror(ierr));
              }
            } else {
              fprintf(stdout, "\n ### ERROR: Cannot remove attribute %s : %s.\n\n",
                                                       argv[2], nc_strerror(ierr));
            }
          } else {
            fprintf(stdout, "\n ### ERROR: Cannot switch to redefinition: %s.\n\n",
                                                                nc_strerror(ierr));
          }
        } else {
          fprintf(stdout, "\n ### ERROR: Cannot find attribute %s : %s.\n\n",
                                                 argv[2], nc_strerror(ierr));
        }
      } else {
        fprintf(stdout, "\n ### ERROR: Cannot find variable %s : %s.\n\n",
                                              argv[1], nc_strerror(ierr));
      }
    } else {
      fprintf(stdout, "\n ### ERROR: Cannot open netCDF file %s : %s.\n\n",
                                            argv[iarg], nc_strerror(ierr));
    }
  }
  exit(ierr);
}
