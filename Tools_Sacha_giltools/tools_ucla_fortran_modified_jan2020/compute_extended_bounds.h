! Same functionality as similarly-named ROMS code file, but this
! time there is no "cppdefs.h" above to define CPP-macros, so they
! are defined here in a polite way: in principle EDGEs can be defined
! outside to be something else and this file will leave the external
! definitions intact; this also avoids warnings by CPP about redefining
! the same macros if this file is included multiple times. 

#ifndef WESTERN_EDGE
# define WESTERN_EDGE istr==1
#endif
#ifndef EASTERN_EDGE
# define EASTERN_EDGE iend==Lm
#endif
#ifndef SOUTHERN_EDGE
# define SOUTHERN_EDGE jstr==1
#endif
#ifndef NORTHERN_EDGE
# define NORTHERN_EDGE jend==Mm
#endif

      integer istrR,iendR,jstrR,jendR
      if (WESTERN_EDGE) then
        istrR=istr-1
      else
        istrR=istr
      endif
      if (EASTERN_EDGE) then
        iendR=iend+1
      else
        iendR=iend
      endif
      if (SOUTHERN_EDGE) then
        jstrR=jstr-1
      else
        jstrR=jstr
      endif
      if (NORTHERN_EDGE) then
        jendR=jend+1
      else
        jendR=jend
      endif
