      program copymask

! Converts and copies integer land mask from a user-specified source
! file into ROMS-standard grid file specified as target. Typically this
! operation may be needed if the land mask is created from coastline
! data and placed as an integer array into a separate file (see
! "costmask.F". The two-stage procedure is preferred over just writing
! land mask directly into ROMS grid file because

!    (i) it eliminates is the risk of accidental overwrite of
!        hand-edited  mask in ROMS grid file, and

!   (ii) also leaves the freedom for "costmask.F" to generate not
!        just 0-or-1 land mask, but "color" the land areas according
!        to some identifier, like polygon ID, level of the GSHHS data
!        set hierarchy, etc.

! The logical content of integer land mask is always expected to be
! water=0; land=something_else (not necessarily 1). The program below
! merely reads the integer array and converts it into 0-or-1, where
! water=0 becomes ROMS-style mask_rho=1(water), while something_else 
! becomes mask_rho=0(land).

      implicit none
      character(len=80) source, target
      real(kind=8), dimension(:,:), allocatable :: rmask
      integer(kind=2), dimension(:,:), allocatable :: mask
      integer nargs, iargc, ncsrc, nctrg, varid, ierr,
     &                      nx,ny, i,j,k, lsrc,ltrg,lenstr
#include "netcdf.inc"

      nargs=iargc()
      if (nargs == 2) then
        call getarg(1,source) ; lsrc=lenstr(source)
        ierr=nf_open(source, nf_nowrite, ncsrc)
        if (ierr == nf_noerr) then
          call getarg(2,target) ; ltrg=lenstr(target)
          ierr=nf_open(target, nf_write, nctrg)
          if (ierr == nf_noerr) then
            call roms_find_dims(ncsrc,  source, i,j,k)
            call roms_check_dims(nctrg, target, i,j,0)
            nx=i+2 ; ny=j+2
            allocate(mask(nx,ny)) ; allocate(rmask(nx,ny))
            ierr=nf_inq_varid(ncsrc, 'land_rho', varid)
            if (ierr == nf_noerr) then
              ierr=nf_get_var_int2(ncsrc, varid, mask)
              if (ierr == nf_noerr) then
                write(*,'(2x,6A)')     'Converting and transferring ',
     &              'land mask from ''', source(1:lsrc), ''' into ''',
     &                                        target(1:ltrg), '''...'
                do j=1,ny
                  do i=1,nx
                    if (mask(i,j) == 0) then
                      rmask(i,j)=1.D0
                    else
                      rmask(i,j)=0.D0
                    endif
                  enddo
                enddo

                call put_var_by_name_double(nctrg, 'mask_rho', rmask)
                ierr=nf_close(nctrg) ; ierr=nf_close(ncsrc)
              else
                write(*,'(/1x,4A/12x,A)')   '### ERROR: Cannot read ',
     &                    'variable ''mask_rho'' from netCDF file ''',
     &                     source(1:lsrc), '''.',  nf_strerror(ierr)
              endif
            else
              write(*,'(/1x,4A/12x,A)')     '### ERROR: Cannot find ',
     &                  'variable ''mask_rho'' inside netCDF file ''',
     &                      source(1:lsrc), '''.', nf_strerror(ierr)
 
            endif
          else
            write(*,'(/1x,4A/12x,A)')       '### ERROR: Cannot open ',
     &           'netCDF file ''', target(1:ltrg), ''' for writting.',
     &                                             nf_strerror(ierr)
          endif
        else
          write(*,'(/1x,4A/12x,A)')  '### ERROR: Cannot open netCDF ',
     &                   'file ''', source(1:lsrc), ''' for reading.',
     &                                             nf_strerror(ierr)
        endif
      else
        write(*,'(/1x,A/12x,A/)') 'Usage:',
     &          'copymask source.nc target.nc'
      endif
      end
